# OpenGoSim documentation

## Introduction
This file gives a basic overview of how to work with this repo.
However, you will likely want to do some reading on [Sphinx](https://www.sphinx-doc.org/en/master/) and
 [reStructuredText](https://docutils.sourceforge.io/rst.html#user-documentation) alongside this.

## Remote builds
There is a Lambda function [here](https://eu-west-1.console.aws.amazon.com/lambda/home?region=eu-west-1#/functions/build-documentation?tab=configuration).
This is automatically triggered when there is **any** push to the repository.
Note, however, that the function builds using only the latest commit on the 'master' branch.
Therefore, it is possible to work on a separate branch and not accidentally have unfinished material on the website.

The Lambda function builds the docs using the 'dirhtml' setting.
It then puts these files in the S3 bucket: 'ogs-documentation-website'.
This bucket effectively hosts the documentation website.


## Working locally

### Python
This documentation is built using the Python library, [Sphinx](https://www.sphinx-doc.org/en/master/).

If you are using this repo locally,
 I suggest you use Python 3 and use a [virtual environment](https://docs.python.org/3/library/venv.html) for your work.

Activate your venv and install the libraries in the requirements file:

    pip install -r requirements.txt

### Building the docs

Typically, Sphinx requires a command like the follow to build the documentation:

    sphinx-build -b html sourcedir builddir

Here:
* `sphinx-build` calls Sphinx;
* `-b` tells it to build;
* `html` is the type of output;
* `sourcedir` is the source directory;
* `builddir` is the build directory.

However, to avoid having to type the source and build directories each time,
 there is a `Makefile` (for Unix-type) and `make.bat` (for Windows).
Now, instead of the above, you can use:

    make html

#### Build types

`html` is the default builder. It builds each `.rst` into a `.html` file.
However, there are more options available, including:

* `dirhtml` is like the above only the URLs are prettier (there's no `.html`)
* `latexpdf` creates PDF documentation.

See [the docs](https://www.sphinx-doc.org/en/master/man/sphinx-build.html) for more options.

### Hosting the docs
You'll like want to test the docs locally before pushing changes to the repo.
Assuming you are using Python 3 as suggested, this is straightforward.

Build the docs:

    make dirhtml

Navigate to the `dirhtml` build folder:

    cd build/dirhtml

In a separate terminal, run a simple HTTP server using Python

    python3 -m http.server 8000

Now, if you open a web browser and navigate to http://localhost:8000/, you should see your build of the docs.
 l
Each time you update the docs, you need to run `make dirhtml` again.
You don't need to restart the HTTP server; just refresh the page in the browser. 

## Note
Sometimes Sphinx seems to fail to update its document tree properly.
If you think that something isn't right, try deleting `./build/doctrees`.
This forces Sphinx to build from scratch again.
