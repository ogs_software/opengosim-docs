import subprocess
import os

print("Updating download zip files....")

here = os.getcwd()

# location of co2 database that must be put into every zip file:
co2_db_loc = here+'/src/tutorial/tutorial_downloads/co2_storage/co2_dbase.dat'


def workOnDir(n: str, d: str):
  co2_db_target = d+'/co2_dbase.dat'
  subprocess.run(['cp', co2_db_loc, co2_db_target])
  os.chdir(d)
  zip_name = n+'.zip'
  subprocess.run(['rm', zip_name])
  files = os.listdir(d)
  subprocess.run(['zip', zip_name] +files)
  subprocess.run(['rm', co2_db_target])



names = ['tutorial1',
         'tutorial2',
         'tutorial3',
         'tutorial4',
]

dirs = ['CCS_LT',
        'INJ_TH',
        'CCS_3DF',
        'DEP_GAS',
]

base_tut_dir = here+'/src/tutorial/tutorial_downloads/co2_storage/'

dirs = [base_tut_dir+d for d in dirs]

for n,d in zip(names,dirs):
  workOnDir(n,d)


print("Updating download zip files.... done")
