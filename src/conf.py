# Configuration file for the Sphinx documentation builder.

master_doc = 'index'

# this is attempt to prevent autosectionlabel from throwing warnings every time
# a section heading is repeated:
suppress_warnings = ['autosectionlabel']

# -- Project information -----------------------------------------------------
project = 'PFLOTRAN'
copyright = '2024, OpenGoSim Limited'
author = 'OpenGoSim Limited'
release = ''#OGS 1.8

html_theme = 'ogs_theme'
html_theme_path = ['.']
# html_theme = 'sphinx_rtd_theme'

html_js_files = [
    'js/jquery-3.6.0.min.js',
    'language_data.js',
    'js/cookies-banner.js',
    'js/copy-button.js',
]

html_logo = 'manual/images/opengosim-logo.png'

html_theme_options = {
    'canonical_url': '',
    'analytics_id': 'G-Y6RE8VYX90',  # GA4
    'logo_only': False,
#     'display_version': True,
    'prev_next_buttons_location': 'bottom',
    'style_external_links': False,
    'style_nav_header_background': 'white',
    # Toc options
    'collapse_navigation': True,
    'sticky_navigation': True,
    'navigation_depth': 10,
    'includehidden': True,
    'titles_only': False
}

html_context = {
    'content_root': '/',
    'favicon': 'icons/ogs_favicon.ico',
}


# This is added to end of every file, so common substitutions can be included here!
rst_epilog = """
.. |pftogs| replace:: PFLOTRAN-OGS 
.. |vernum| replace:: 1.8
.. |prevvernum| replace:: 1.7
.. |H2O| replace:: H\ :sub:`2`\ O
.. |CO2| replace:: CO\ :sub:`2` \
"""

highlight_language = 'none'

extensions = [
    'sphinx.ext.autosectionlabel',
    'sphinxcontrib.bibtex', 
#     'sphinx_copybutton',
    'sphinx_sitemap',
]

# Newer versions of sphinxcontrib.bibtex require to specify the file
try:
    bibtex_bibfiles = ['refs.bib']
except:
    pass

html_baseurl = 'https://docs.opengosim.com'  # required for sitemap
html_extra_path = ['robots.txt']  # required for sitemap

# sphinx package names for reference:
# sphinxcontrib-bibtex
# sphinx-copybutton

#extensions = [
    ##'sphinx.ext.autosectionlabel',
    #'sphinxcontrib.bibtex'
#]


templates_path = ['_templates']
