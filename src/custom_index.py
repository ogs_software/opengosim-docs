import os
import sys


def isIndexLine(l):
# note not perfect implementation - looks only at start of line instead
# of any appearence of :index:
  comp=":index:"
  lc = len(comp)
  if (len(l) < len(comp)):
    return False 
  else:
    return (comp == l[0:lc])

filename = sys.argv[1]
print(filename)




with open(filename) as txtfile:
  lines = [line for line in txtfile]

for l in lines:
  if (isIndexLine(l)):
    print(l)
