.. OpenGoSim documentation master file.
   You can adapt this file, but it must contain the root `toctree` directive.

##########################
|pftogs|
##########################

Welcome to the |pftogs| manual.


.. toctree::
   :maxdepth: 2

   release
   manual/introduction
   installing/installing
   manual/manual
   theory/theory
   reference/reference
   tutorial/tutorials
   bibliography
   keywords_index
   support

