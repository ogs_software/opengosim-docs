
.. _cloud_install_sec:

Cloud Installation
============================================

`OpenGoSim <https://opengosim.com/>`_ provides a service for running PFLOTRAN-OGS on the cloud.

This services is delivered through a Web Application, the `OGS WebApp <https://opengosim.com/ogs-web-app.php>`_, which  gives a access to flexible and affordable HPC computing.

To know more and get an evaluation account on the OGS-WebApp, `get in touch <https://opengosim.com/contact.php>`_ with OpenGoSim.
