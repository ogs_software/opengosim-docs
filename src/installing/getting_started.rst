
.. _getting_started_sec:

Getting Started With |pftogs|
============================================

Having :ref:`built<ubuntu_install_sec>` |pftogs|, we can start using it. For a detailed introduction to working with some interesting CCS models, see the :ref:`tutorials<tutorial_1_sec>`. 

There is a directory of simple demo cases in the |pftogs| repo. This directory is called ``data`` and contains several small cases.

Running a Demo from the Command Line
--------------------------------------------

Running the ccs demo case from the command line can be done from within the ``data`` directory as: ::
  
  /home/myusername/petsc/ubuntu-opt/bin/mpirun -np 2 /home/myusername/pflotran_ogs_1.8/src/pflotran/pflotran -pflotranin ccs.in

See :ref:`here<ubuntu_command_line_sec>` for a more in depth description of commands like this.

We will then see the simulator running, with reports of timesteps appearing on the screen, while more detailed output is written to files. 

This will also cause a variety of output files to be created in the same directory (the --output_prefix flag could be used to redirect these elsewhere). These files can be used for analysing and :ref:`visualising<visualising_sec>` the results of the simulation. These include:

 * ccs.out: a log file, showing a record of the timesteps and various other important information about the simulation.
 * ccs-mas.dat: the :ref:`mass balance file<mass_file_sec>`.
 * ccs.EGRID, ccs.UNRST, and others: :ref:`eclipse file<eclipse_files_sec>` output, compatible with a variety of industry standard post processors, including Stratus.

.. _run_macro_sec:

:index:`Setting Up and Using the Run-Macro`
--------------------------------------------

In the ``macros`` directory, there is a simple bash script for running |pftogs|, which can allow a more elegant invocation than the command seen in the previous section. With the script correctly set up, that previous command could be replaced by: ::

  pft1.8 ccs 2

More confident Linux users will have their own ideas about how to use a script like this in their own workflows; here we will detail a quick setup process to make use of this script as convinient as possible going forward.

First, note the following lines in the script: ::

  # Please ensure that the following is the correct path of your pflotran_ogs 
  # installaiton and ammend if not:
  PFT_PATH=~/pflotran_ogs_1.8
  # For example:
  #CRS_PATH="/home/myusername/pflotran_ogs_1.8"

Please make sure that the path to your |pftogs| install is correct here, and edit if not (i.e. if you cloned the |pftogs| repo to directory with a name other than pflotran_ogs_1.8).

Next note this part: ::

  # Please ensure the following variables are defined:
  # PETSC_DIR
  # PETSC_ARCH
  # see "Installing and Running on Ubuntu" page for more information

These are the same ``PETSC_DIR`` and ``PETSC_ARCH`` variables that we used when :ref:`building<ubuntu_install_sec>` |pftogs|, and they must be defined when we run the script. In order to ensure they are always defined moving forward, it's convinient to define them in the .bashrc file. Edit the file: ::

  ~/.bashrc

and add at the bottom, the lines, for example: ::

  export PETSC_DIR=/home/myusername/petsc
  export PETSC_ARCH=ubuntu-opt

but change the placeholder myusername. 

We would also like to make the script always be in PATH, so we can invoke it from anywhere in the filesystem. You may have some scripts or macros directory that you use on your system, but otherwise, do something like this: ::


 mkdir ~/macros
 cp ~/pflotran_ogs_1.8/macros/pft1.8 ~/macros 

This copies the pft1.8 script into a new directory called ``macros``. Now to ensure that directory is always in PATH, add the following to .bashrc: ::

  export PATH="~/macros:$PATH"

That should be everything. To ensure that the changes to .bashrc are reflected in your current session, either open a new terminal window or do: ::

  source ~/.bashrc


After this it should always be possible to invoke |pftogs| on your system with commands like: ::

  pft1.8 ccs 2

You can use this to explore running the demos like in the previous section, or the :ref:`tutorials<tutorial_1_sec>`, or your own studies.
