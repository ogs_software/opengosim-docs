
.. _installing_sec:

Installing and Running PFLOTRAN-OGS
=====================================

PFLOTRAN-OGS is developed and supported by `OpenGoSim <https://opengosim.com/>`_, which distributes the simulator in three different ways:

.. toctree::
   :maxdepth: 1


   windows_install
   cloud
   ubuntu_install
   getting_started


Click on each specific section to know more about, or `get in touch <https://opengosim.com/contact.php>`_  with OpenGoSim to know more.
