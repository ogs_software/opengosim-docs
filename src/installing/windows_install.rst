
.. _windows_install_sec:

Windows Install
=========================================


`OpenGoSim <https://opengosim.com/>`_ provides Windows installations that include `Stratus <https://opengosim.com/stratus.php>`_, a GUI to help with pre- and post- processing of PFLOTRAN-OGS simulations.

Stratus is free for Academia, while a fee applies for commercial use.

Please `contact <https://opengosim.com/contact.php>`_ OpenGoSim to obtain an evaluation copy of Stratus.
