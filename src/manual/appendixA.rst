
.. _appA:

:index:`RESERVOIR_DEFAULTS`
===========================================================

Here we list the defaults that the RESERVOIR_DEFAULTS keyword imposes on each input
card, unless overridden. For more information see each respective card and keyword.

SIMULATION
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Card: :ref:`SIMULATION<simulation_card_sec>`.


* :ref:`ANALYTICAL_JACOBIAN<alyt_jac_sec>`: this is automatically selected. Analytical Jacobian increases the accuracy and convergence of the nonlinear solver. The opposite is NUMERICAL_JACOBIAN, whichuses numerical differences to find the Jacobian and is usually less accurate.
* :ref:`MAX_PRESSURE_CHANGE<max_pres_change_sec>`: this is set to 5.5d6 Pa. This value has been observed to provide a good balance of safety and timestep acceleration for reservoir models under test.

TIMESTEPPER FLOW
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Card: :ref:`TIMESTEPPER FLOW<timestepper_sec>`.

* :ref:`TS_ACCELERATION<ts_accel_sec>`: this is set to 100. This value has been observed to provide a good balance of safety and timestep acceleration for reservoir models under test.

LINEAR_SOLVER
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Card: :ref:`LINEAR_SOLVER<linear_solver_card_sec>`.

* :ref:`PC_TYPE<pc_type_sec>`: this is set to CPR, the constrained pressure residual, a multigrid based preconditioner. CPR is generally required for most difficult reservoir models.
* :ref:`KSP_TYPE<ksp_type_sec>`: this is set to FGMRES, the flexible gmres linear solver is a common solver and works well with the CPR preconditioner.
* :ref:`GMRES_MODIFIED_GRAMSCHMIDT<mod_gs_sec>`: this is automatically selected. A reliable setting for FGMRES for our purposes.
* :ref:`GMRES_RESTART<gm_rst_sec>`: this is set to 100. A reliable setting for FGMRES for our purposes.
* :ref:`MAXIT<maxit_ls_sec>`: this is set to 80. Higher than this indicates the linear solver would benefit more from a timestep cut than more iterations.

NEWTON_SOLVER
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Card :ref:`NEWTON_SOLVER<newton_solver_flow_sec>`.

* :ref:`STOL<stol_ns_sec>`: set to 1.0D-16. STOL is not often a reliable convergence measure so in most cases a step should converge by some other criterion.
* :ref:`MAXIT<maxit_ns_sec>`: set to 15. A healthy Newton convergence is usually up to about 10 iterations. More than this and it is often inefficient to force it to converge at the given timestep length.
* :ref:`ITOL<itol_ns_sec>`: set to 1.0D-8. A reliable absolute convergence tolerance. Works well with the RTOL default already in place.
* :ref:`ITOL_S<itol_s_ns_sec>`: set to 1.0D-8. Also the computation of scaled norms is activated. 

