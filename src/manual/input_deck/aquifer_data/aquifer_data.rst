
.. _aquifer_data_sec:

:index:`AQUIFER_DATA`
====================================

Defines an analytical aquifer, which can be modelled by either Carter-Tracy :cite:`carter1960improved`. or Fetkovich :cite:`fetkovich1971simplified`. The Carter-Tracy is the default model, which can be used to either describe a radial or linear aquifer.

``AQUIFER_DATA`` must be followed by a name for the aquifer. For example: ::

  AQUIFER_DATA A1
    BACKFLOW
    DEPTH     2550.0 m
    THICKNESS 100 m
    RADIUS    507.8 m
    PERM      500  mD
    COMPRESSIBILITY 2.0E-9 1/Pa
    POROSITY  0.1
    VISCOSITY 1 cP
    CONN_D 1 1 1 9 1 1 X- 3.0
    CONN_D 1 9 1 1 1 1 Y- 3.0
  END

It is possible to connect the same aquifer to more grid blocks; connections can be added by multiple lines within the definition of one aquifer (see the multiple ``CONN_D`` lines above).

The options supported by AQUIFER_DATA are:

.. contents::
  :backlinks: top
  :depth: 2
  :local:


BACKFLOW
+++++++++++++++++++++++++++

Specifies that fluids are allowed to migrate from the reservoir to the aquifer. 

This option should be used with caution, as it can result in unwanted fluid re-circulations between grid blocks connected to the same aquifer. As a general rule an aquifer should be far away from the main injection/production regions.



DEPTH
+++++++++++++++++++++++++++++++++++++++++++++++++

Specify aquifer depth. Units are required.


ZAQUIFER
++++++++++++++++++++++++++++++

Specify aquifer elevation. For example if the aquifer depth is 2500 m, ZAQUIFER will be -2500 m. Units are required.

THETA_FRACTION, THETA
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Specify the aquifer angle (for a radial aquifer). Defaulted to a ``THETA_FRACTION`` of 1.0.
``THETA`` allows the same data to be added as an angle in degrees - .e.g. ``THETA 145``.

So ``THETA_FRACTION`` = ``THETA``/360.0


THICKNESS
+++++++++++++++++++++++++++++++++++++++++++++++++

Aquifer thickness. Required. Units must be specified.


WIDTH
+++++++++++++++++++++++++++++++++++++++++++++++++

Aquifer width, required for linear aquifer. Units must be specified.


RADIUS
+++++++++++++++++++++++++++++++++++++++++++++++++

Aquifer inner radius. Required for radial aquifer. Units must be specified.


PORO, POROSITY
+++++++++++++++++++++++++++++++++++++++++++++++++

PORO or POROSITY defines the aquifer porosity. Required.


PERM, PERMEABILITY
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


PERM or PERMEABILITY defines the aquifer permeability. Units must be specified. Required.

VISC, VISCOSITY 
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


VISC or VISCOSITY defines the aquifer viscosity. Units must be specified. Required.


CMPR, COMPRESSIBILITY
+++++++++++++++++++++++++++++++++++++++++++++++++

CMPR or COMPRESSIBILITY defines the aquifer compressibility (rock + water): required.


PINIT, PRESSURE
+++++++++++++++++++++++++++++++++++++++++++++++++


PINIT or PRESSURE defines the aquifer pressure. Pressure units must be specified.
Not required, if not entered the aquifer pressure will be set in equilibrium with reservoir.


TINIT, TEMPERATURE
+++++++++++++++++++++++++++++++++++++++++++++++++

TINIT or TEMPERATURE defines the aquifer temperature. Temperature units must be specified.

Required for thermal runs.



GAS_IN_LIQUID_MOLE_FRACTION
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Gas in liquid mole fraction in brine. Defaults to 0.0. Only used in GW mode.


IFT
+++++++++++++++++++++++++++++++++++++++++++++++++

Influence function table. Not required. If omitted, defaults to infinite aquifer. 

Example: ::

  IFT
     0.01 0.112
     0.05 0.229
     0.10 0.315
     0.15 0.376
     0.20 0.424
     0.25 0.469
     0.30 0.503
     0.40 0.564
     0.50 0.616
     0.60 0.659
     0.70 0.702
     0.80 0.735
     0.90 0.772
     1.00 0.802
     1.50 0.927
     2.0  1.020
     2.5  1.101
     3.0  1.169
     4.0  1.275
     5.0  1.362
     6.0  1.436
     7.0  1.500
     8.0  1.556
     9.0  1.604
     10.0 1.651
     15.0 1.829
     20.0 1.960
     25.0 2.067
     30.0 2.147
     40.0 2.282
     50.0 2.388
     60.0 2.476
     70.0 2.550
     80.0 2.615
     90.0 2.672
    100.0 2.723
    150.0 2.921
    200.0 3.064
    250.0 3.173
    300.0 3.263
    400.0 3.406
    500.0 3.516
    600.0 3.608
    700.0 3.684
    800.0 3.750
    900.0 3.809
   1000.0 3.860
  END IFT

.. _conn_D_sec:

CONN_D
++++++++++++++++++++++++++++++++

Specifies blocks of connections to attach aquifer to reservoir cell faces. Follows the ECLIPSE grdecl convention (K index is counted downwards from the top layer).

An example is: ::

  CONN_D 1 9 9 9 1 1 Y+ 3.0

Note that the connection box argument order is: ::

  CONN_D IL IU JL JU KL KU CMF

Where:

    * IL and IU are a range of cell indices in the I-direction
    * JL and JU are a range of cell indices in the J-direction
    * KL and KU are a range of cell indices in the K-direction
    * CMF: connection multiplier factor (optional)             


Connections will be made to cells with I-indices such that :math:`I \geq IL` and :math:`I \leq IU`, etc.

The cell faces to attach the aquifer are specified by one of the following options:

    * X+ or I+, faces connecting cells with I-indices to cells with (I+1)-indices
    * Y+ or J+, faces connecting cells with J-indices to cells with (J+1)-indices
    * Z+ or K+, faces connecting cells with K-indices to cells with (K+1)-indices

The pairs (X- or I-), (Y- or I-) and (Z- or K-) follow the same logic, and are used to connect to cells with (I-1)-indices, (J-1)-indices and (K-1)-indices.

A number of CONN_D sub-keywords may be entered to build up the full connection. If the same face of the same cell is included more than once, only one connection will be generated.

CONN_Z
+++++++++++++++++++++++

Warning: keyword to be used only by proficient PFLOTRAN users. Unless there is some specific reason, consider using :ref:`CONN_D<conn_D_sec>` instead.

Specifies blocks of connections to attach aquifer to reservoir cell faces.

CONN_Z use the convention that the K index is counted upwards from the bottom layer.
For example: ::

  CONN_Z 1 9 9 9 1 1 Y+ 3.0

Note that the connection box argument order is: ::

  CONN_Z IL IU JL JU KL KU CMF


Where:

    * IL and IU are a range of cell indices in the I-direction
    * JL and JU are a range of cell indices in the J-direction
    * KL and KU are a range of cell indices in the K-direction
    * CMF: connection multiplier factor (optional)             

Connections will be made to cells with I-indices such that :math:`I \geq IL` and :math:`I \leq IU`, etc.

The cell faces to attach the aquifer are specified by one of the following options:

    • X+ or I+, faces connecting cells with I-indices to cells with (I+1)-indices (GRDECL grid format).
    • Y+ or J+, faces connecting cells with J-indices to cells with (J+1)-indices (GRDECL grid format).
    • Z+ or K+, faces connecting cells with K-indices to cells with (K+1)-indices (K index counted upwards from the bottom layer).

The pairs (X- or I-), (Y- or I-) and (Z- or K-) follow the same logic, and are used to connect to cells with (I-1)-indices, (J-1)-indices and (K-1)-indices.

When using CONN_Z, the top faces of a reservoir with 100 layers are defined with Z+. See the following example: ::

  CONN_Z  1 50 1 50  100 100  Z+

.. _fet_sec:

FET
++++++++++++++++++++

Selects a Fetkovich analytical aquifer :cite:`fetkovich1971simplified`, instead of the Carter-Tracy, which is the default model
otherwise. A FET aquifer requires the user to specify the aquifer pore volume (:ref:`PV<pv_sec>`), and the aquifer  productivity index (:ref:`PI<pi_sec>`), both referring to surface volumes. See example below: ::

  AQUIFER_DATA AQ1
  BACK
  FET
  PV 10000000 m^3

  PI 1000 m^3/d/bar
  DEPTH 2500 m
  COMPRESSIBILITY 1.0E-4 1/Bar

  CONN_D 1 20 1 10 1 20 X-
  END

Note that the aquifer porosity, permeability, viscosity, internal radius/thickness and angle (theta) are not required.

.. _pv_sec:

PV
++++++++++++++++++++

Specifies the aquifer pore volume at surface conditions, required only by the Fetkovich aquifer (see :ref:`FET<fet_sec>` for an example).



.. _pi_sec:

PI
++++++++++++++++++++

Specifies the aquifer productivity index in terms of surface volumes. This is required only by the
Fetkovich aquifer (see :ref:`FET<fet_sec>` for an example).



