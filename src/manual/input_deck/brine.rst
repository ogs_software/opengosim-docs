
.. _brine_sec:

:index:`BRINE`
---------------------

Enables the brine salinity to be set in GAS_WATER, COMP3 and COMP4 modes. For example: ::

  BRINE 1.092 MOLAL

The brine salinity be given using different units: MOLAL, MASS or MOLE. The default is MOLAL.

  * MOLAL implies salinity is entered as a gm-molality - i.e. mol salt/Kg water
  * MASS implies salinity is entered as a mass fraction - i.e. (mass salt)/(mass brine)
  * MOLE implies salinity is entered as a mole fraction - i.e. (moles salt)/(moles brine)

If the user specifies salinity by SALTVD, the BRINE value is also required for the conversion to brine surface volumes. If not entered the code returns an error.
