
.. _black_oil_cc_sec:

Black Oil Model Characteristic curves
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
The black oil model requires the definition of two capillary pressure curves, for the interface
between the oil and water phases, and the interface of the oil and gas phases. In addition
relative permeabilities for the water, gas and the oil must also be specified.

.. _black_oil_sat_prop_by_table_example:

Black Oil - saturation properties by tables - example
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

In the example below the tables needed to define :math:`P_{cow}`, :math:`K_{rw}`, :math:`K_{rg}`, :math:`P_{cog}`, :math:`K_{row}` and :math:`K_{rog}` have been specified. The oil relative permeability defaults to ECLIPSE model. Keywords such as KRW_TABLE are not needed as each saturation property is defined unequivocally. ::

  CHARACTERISTIC_CURVES bo_sat_prop_table_example
    TABLE swfn_table
      PRESSURE_UNITS Bar
      EXTERNAL_FILE swfn_file.dat
    END
    TABLE sof3_table
      EXTERNAL_FILE sof3_file.dat
    END
    TABLE sgfn_table
      PRESSURE_UNITS Pa
      EXTERNAL_FILE sgfn_file.dat
    END
  END

Where the file “swfn_file.dat” contains the water saturation table: ::

  SWFN
    0.22 0   0.48
    0.3  0.07 0.27
    0.4  0.15 0.2
    0.5  0.24 0.17
    0.6  0.33 0.14
    0.8  0.65 0.07
    0.9  0.83 0.03
    1    1    0
  /

The file “sgfn_file.dat” contains the gas saturation table: ::

  SGFN
    0.00 0.00 0.00
    0.04 0.00 0.014
    0.10 0.02 0.034
    0.20 0.10 0.068
    0.30 0.24 0.10
    0.40 0.34 0.14
    0.50 0.42 0.17
    0.60 0.50 0.20
    0.70 0.81 0.24
    0.78 1.00 0.27
  /

The file sof3_file.dat contains the oil permeability table: ::

  SOF3
    0    0       0
    0.2  0       0
    0.38 0.00432 0
    0.4  0.0048  0.004
    0.48 0.05288 0.02
    0.5  0.0649  0.036
    0.58 0.11298 0.1
    0.6  0.125   0.146
    0.68 0.345   0.33
    0.7  0.4     0.42
    0.74 0.7     0.6
    0.78 1       1
  /
