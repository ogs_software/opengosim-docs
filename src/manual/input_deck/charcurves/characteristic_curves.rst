
.. _char_curves_sec:

:index:`CHARACTERISTIC_CURVES (Saturation functions)`
-----------------------------------------------------------

This section specifies the saturation functions, both the capillary pressures and relative
permeabilities. Multiple characteristic curves can be defined within an input desk. :ref:`Analytical functions<char_curves_alyt_sec>` or :ref:`tables<char_curves_table_sec>` can be used to define the saturation functions, described in their respective sections.


.. toctree::
  :maxdepth: 1
  :caption: Defining The Curves

  charcurves_tables
  charcurves_alyt
  oil_relperm_3phase


.. toctree::
  :maxdepth: 1
  :caption: Examples

  ow_cc
  bo_cc
  gw_cc
  tl_cc
  solv_cc

.. toctree::
  :maxdepth: 1
  :caption: Further Information

  multiple_tables
  levj


Mixed approach
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

PFLOTRAN-OGS can define capillary and relative permeability functions by the means of tables
and analytical expressions. The two approaches can be mixed, as long as the end points are
consistent. The software will perform a check after reading the input, and where different
values for the end points are found in the saturation functions, an error will be thrown.

