
.. _char_curves_alyt_sec:

Characteristic Curves defined by analytic options
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

PFLOTRAN-OGS has a large range of options for entering saturation function data using analytic
functions. These are detailed in :ref:`Analytical Options for Characteristic Curves<alyt_cc_opts_sec>`.

To define characteristic curves with analytical functions the following blocks within the
characteristic curve block may be used. Each of these blocks may select the function which
is required, and provide the required parameters.

+----------------------------+--------+---------------------------------------------------------------------------------------------------------------------------------------+
| Block name                 | Alias  | Defines                                                                                                                               |
+============================+========+=====================+=================================================================================================================+
| CAP_PRESSURE_FUNCTION_OW   | PC_OW  | Oil-water capillary pressure :math:`P_{cow}  =P_o - P_w`                                                                              |
+----------------------------+--------+---------------------------------------------------------------------------------------------------------------------------------------+
| CAP_PRESSURE_FUNCTION_OG   | PC_OG  | Oil-gas capillary pressure :math:`P_{cog}   =P_g  -P_o`                                                                               |
+----------------------------+--------+---------------------------------------------------------------------------------------------------------------------------------------+
| PERMEABILITY_FUNCTION_WAT  | KRW    | Water relative permeability :math:`K_{rw} (S_w  )`                                                                                    |
+----------------------------+--------+---------------------------------------------------------------------------------------------------------------------------------------+
| PERMEABILITY_FUNCTION_GAS  | KRG    | Gas relative permeability :math:`K_{rg}   (S_g  )`                                                                                    |
+----------------------------+--------+---------------------------------------------------------------------------------------------------------------------------------------+
| PERMEABILITY_FUNCTION_OW   | KROW   | Oil-in-water relative permeability :math:`K_{row}  (S_w  )`                                                                           |
+----------------------------+--------+---------------------------------------------------------------------------------------------------------------------------------------+
| PERMEABILITY_FUNCTION_OG   | KROG   | Oil-in-gas relative permeability :math:`K_{rog}   (S_g  )`                                                                            |
+----------------------------+--------+---------------------------------------------------------------------------------------------------------------------------------------+
| PERMEABILITY_FUNCTION_OIL  | KRO    | Oil relative permeability :math:`K_{ro}   (S_w  ,S_g  )`                                                                              |
+----------------------------+--------+---------------------------------------------------------------------------------------------------------------------------------------+
| PERMEABILITY_FUNCTION_HC   | KRH    | Hydrocarbon-in-water relative permeability :math:`K_{rhw} (S_w)`  (similar to KROW, but applies to hydrocarbon :math:`S_h =S_o +S_s`) |
+----------------------------+--------+---------------------------------------------------------------------------------------------------------------------------------------+


For details of these correlations, please consult :ref:`Analytical Options for Characteristic Curves<alyt_cc_opts_sec>`. An example of their use in a two-phase oil case is: ::

  CHARACTERISTIC_CURVES ch1
    CAP_PRESSURE_FUNCTION_OW VAN_GENUCHTEN
      WATER_CONNATE_SATURATION 0.2
      WATER_RESIDUAL_SATURATION 0.2
      M 0.5d0
      ALPHA 5.792e-05
      MAX_CAPILLARY_PRESSURE 0.d0
      !SMOOTH ! When using smoothing option
    /
    PERMEABILITY_FUNCTION_WAT MOD_BROOKS_COREY
      M 2.0d0
      WATER_CONNATE_SATURATION 0.2
      WATER_RESIDUAL_SATURATION 0.2
    END
    PERMEABILITY_FUNCTION_OW MOD_BROOKS_COREY
    M 2.0d0
    OIL_RESIDUAL_SATURATION 0.2d0
    END

    TEST
  /

TEST is an optional keyword that that print the saturation functions on ASCII files, with the following naming:

 * <characteristic_curves_name>_pcxw_sw.dat
 * <characteristic_curves_name>_pcog_sg.dat
 * <characteristic_curves_name>_wat_rel_perm.dat
 * <characteristic_curves_name>_gas_rel_perm.dat
 * <characteristic_curves_name>_ow_rel_perm.dat
 * <characteristic_curves_name>_og_rel_perm.dat
 * <characteristic_curves_name>_oil_rel_perm.dat











