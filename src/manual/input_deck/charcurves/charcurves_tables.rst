

.. _char_curves_table_sec:

Characteristic Curves defined by tables
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Table input is defined within a characteristic curve block, which defines one set of curves in a block. See below example for a set of curves for the GAS_WATER mode: ::

  CHARACTERISTIC_CURVES ch1

   TABLE swfn_table
     PRESSURE_UNITS Bar
     SWFN
       0.0    0    0.4
       0.1    0    0.3
       0.9    1    0.2
       1.0    1    0.1
     /
   END

   TABLE sgfn_table
    PRESSURE_UNITS Bar
      SGFN
       0.0      0        0
       0.10     0.0      0
       0.255    0.15     0
       0.51     0.4      0
       0.765    0.8      0
       1.0      1.0      0
      /
   END

  /

The name that follows CHARACTERISTIC_CURVES (e.g. ‘ch1’),  allows this data to be associated with a material property. If more than one set of characteristic curves is entered, it is also possible to assign these to cells using the SATNUM keyword in a GRDECL file.

To define the set of saturation curves required by a flow model, the CHARACTERISTIC_CURVES block contains multiple TABLE blocks (see example above).

TABLE takes one compulsory argument, which is the name of the table. This can be given a physical name like ‘sgfn_table’, but that is not required - a unique string like XXX would also work. The names must be unique within each CHARACTERISTIC_CURVES block.

PRESSURE_UNITS is required only for tables that define a capillary pressure. If the table contains a capillary pressure this keyword is compulsory.

The following tables can be specified to enter the data of the saturation curves:

.. toctree::
  :maxdepth: 1

  swfn
  sgfn
  swsfn
  sgsfn
  sof2
  sof3

A TABLE block may not contain the data directly - it is possible to include an external file, for example: ::

  TABLE sof2_table
    EXTERNAL_FILE sof2_table.dat ! Contains SOF2 table data
  END

The tables required by each flow models are given below:

+-------------------------------------+------------------------------------------------------------------------------------------------+--------------------------+
| Flow model                          | Saturation Curves                                                                              | Saturation Curve Tables  |
+=====================================+================================================================================================+==========================+
| GAS_WATER, COMP3                    | :math:`P_{\mbox{cgw}},K_{\mbox{rw}},K_{\mbox{rg}}`                                             | SWFN, SGFN               |
+-------------------------------------+------------------------------------------------------------------------------------------------+--------------------------+
| BLACK_OIL, SOLVENT_TL, COMP4, COMP  | :math:`P_{\mbox{cow}},P_{\mbox{cog}}K_{\mbox{rw}},K_{\mbox{rg}},K_{\mbox{row}},K_{\mbox{rog}}` | SWFN, SGFN, SOF3         |
+-------------------------------------+------------------------------------------------------------------------------------------------+--------------------------+
| TODD_LONGSTAFF                      | :math:`P_{\mbox{cow}},K_{\mbox{rw}},K_{\mbox{rhw}}`                                            | SWFN, SOF2               |
+-------------------------------------+------------------------------------------------------------------------------------------------+--------------------------+
| TOIL                                | :math:`P_{\mbox{cow}},K_{\mbox{rw}},K_{\mbox{row}}`                                            | SWFN, SOF2, SOF3         |
+-------------------------------------+------------------------------------------------------------------------------------------------+--------------------------+

In some cases it may be necessary to make clear the association between the information required and where it is obtained, in which case the keywords PC_OW_TABLE, PC_OG_TABLE, KRW_TABLE, KRG_TABLE, KROW_TABLE and KROG_TABLE can be used. Then it would be possible, for example, take the water relative permeability function from one table and the oil-water capillary pressure from another. For more information see :ref:`Defining the data in multiple tables<multiple_tables_sec>`.

