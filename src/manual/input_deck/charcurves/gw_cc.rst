
Gas-water Model Characteristic curves
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The gas-water model requires the definition of a capillary pressure curve for the interface between the gas and the water phases, and relative permeability for the water and the gas in water. An example is: ::

  CHARACTERISTIC_CURVES gas_water_example
    TABLE swfn_table
      PRESSURE_UNITS Bar
      EXTERNAL_FILE swfn_file.dat
    END
    TABLE sgfn_table
      PRESSURE_UNITS Bar
      EXTERNAL_FILE sgfn_file.dat
    END
  END

Where the file “swfn_file.dat” contains the water saturation table: ::

   SWFN
     0.0    0    0.4
     0.1    0    0.3
     0.9    1    0.2
     1.0    1    0.1
   /

The file “sgfn_file.dat” contains the gas saturation table: ::

    SGFN
     0.0      0       0
     0.10    0.0      0
     0.255   0.15     0
     0.51    0.4      0
     0.765   0.8      0
     1.0     1.0      0
    /

Note that the capillary pressure entered with the water function table will be used as the aqueous phase/gas phase capillary pressure. The capillary pressure entered with the gas function table will be ignored.



