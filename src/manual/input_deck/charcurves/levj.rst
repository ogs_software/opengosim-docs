
.. _levj_sec:

:index:`LJEV_GW` (Leverett J-function)       
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The Leverett J-function option in is only supported in the gas-water (GASWAT) solution mode. An error
will be produced if the option is used in any other solution mode. The option is requested by adding the keyword ``JLEV_GW`` in the main input deck for example: ::

  #= saturation functions =============================

  JLEV_GW 20 XY dynes/cm

  CHARACTERISTIC_CURVES ch1

    TABLE swfn_table
      PRESSURE_UNITS None
      SWFN
	0.0 0 0.4
	0.1 0 0.3
	0.9 1 0.2
	1.0 1 0.1
      /
    END

    TABLE sgfn_table
      PRESSURE_UNITS None
      SGFN
	0 0 0
	0.1 0.004 0
	0.2 0.025 0
	0.3 0.075 0
	0.4 0.150 0
	0.5 0.250 0
	0.6 0.400 0
	1.0 1.000 0
      /
    END

  END


The first argument is the gas-water surface tension, in this case in units of dynes/cm. The only allowed  alternative unit option is N/m (Newtons/meter). The XY field indicates that the permeability to be used in the capillary pressure calculation is :math:`(K_x+K_y)/2`. Alternatives are X, Y and Z to request the use of :math:`K_x`, :math:`K_y` and  :math:`K_z` respectively. If ``JLEV_GW`` is used, the input capillary pressure curves are interpreted as dimensionless :math:`J(S_w)`-functions of saturation. These must be entered with SWFN tables using a ``PRESSURE_UNITS`` value of ‘NONE’ (see example above).

The porosity used is the porosity value entered by the user, under the ``PORO`` keyword. This is the porosity value at the reference pressure. This is conventional, but note that actual porosity values in the simulation will be slightly different, and time-dependent, due to rock compressibility effects.


The actual capillary pressure used in the simulation when JLEV_GW is used is:

.. math::
  P_c(S_w) = \sigma \sqrt{\phi/K} \cdot J(S_w)

Where :math:`\sigma` is the surface tension in :math:`N/m`, :math:`\phi` is the porosity, :math:`K` is the permeability value in :math:`m^2` and :math:`P_c` is the capillary pressure in Pa. :math:`J(S_w)` is the supplied dimensionless J-function as a function of :math:`S_w`.
To avoid numerical problems in the case in which :math:`K \geq 0`, a minimum permeability value of 1 nanodarcy is imposed ( 9.869233E-22 :math:`m^2`).
