
.. _multiple_tables_sec:

Defining the data in multiple tables
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
There are six basic types of data that can be input in a ``CHARACTERISTIC_CURVES`` block, and each has a keyword which is followed by the name of the table block from which the data is to be taken.

The following example shows the use of keywords like KRW_TABLE to identify which TABLE block is to be used. ::

  CHARACTERISTIC_CURVES ch1

    KRW_TABLE swfn_table
    KRG_TABLE sgfn_table
    KRO ECLIPSE
      KRO_TABLE sof3_table
    END

   TABLE swfn_table
      PRESSURE_UNITS Bar
      EXTERNAL_FILE swfn_file.dat
    END

    TABLE sgfn_table
      PRESSURE_UNITS Pa
      EXTERNAL_FILE sgfn_file.dat
    END

    TABLE sof3_table
      EXTERNAL_FILE sof3_file.dat
    END

  /

The following keywords can be used within the CHARACTERISTIC_CURVES block to specify which table a saturation curve must be taken from:

.. contents::
  :backlinks: top
  :depth: 2
  :local:



PC_OW_TABLE
++++++++++++++++++++++++++++++++

Selects the table block to be used for the capillary pressure curve between oil and water,
:math:`P_{cow} =P_o -P_w`. ::

  PC_OW_TABLE <water_sat_property_table_name>

PC_OG_TABLE
++++++++++++++++++++++++++++++++

Selects the table block to be used for the capillary pressure curve between oil and gas, :math:`P_{cog} =P_g -P_o`. ::

  PC_OG_TABLE <gas_sat_property_table_name>

KRW_TABLE
++++++++++++++++++++++++++++++++
Selects the table block to be used for the water relative permeability curve, :math:`K_{rw}(S_w)`. ::

  KRW_TABLE <water_sat_property_table_name>

KRG_TABLE
++++++++++++++++++++++++++++++++
Selects the table block to be used for the gas relative permeability curve, :math:`K_{rg}(S_g)`.::

  KRG_TABLE <gas_sat_property_table_name>

KROW_TABLE
++++++++++++++++++++++++++++++++
Selects the table block to be used for the oil in water relative permeability curve at zero gas
saturation, :math:`K_{row}(S_w,S_o=1-S_w,S_g=0)`. ::

  KROW_TABLE <oil_in_wat_rel_perm_table_name>

KROG_TABLE
++++++++++++++++++++++++++++++++
Selects the table to be used for the oil in gas relative permeability curve at connate water
saturation, :math:`K_{row}(S_g,S_o=1-S_g-S_{wco},S_w=S_{wco})`. ::

  KROG_TABLE <oil_in_gas_rel_perm_table_name>

For Todd-Longstaff mode runs, KRH_TABLE should be used rather than KROW_TABLE, as it
applies to the hydrocarbon saturation :math:`(S_h =S_o +S_s )` rather than just the oil saturation.
