
.. _rpm-3ph-section:

Oil Relative permeability in three phase cases
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
In the three phase case (oil-water-gas) or four phase case (oil-water-gas-solvent), an oil-wet
relative permeability model is assumed - the water and gas relative permeabilities are
functions of the water and gas saturations respectively, but the intermediate-wet phase
relative permeability, the oil, is a function of both the water and the gas saturations,
through a three phase relative permeability model. The one currently available is the
Eclipse model for :math:`K_{ro}(S_w ,S_g )`, which is described by the following equation:

.. math::
  k_{ro} = \frac{
  S_g k_{rog} +( S_w −S_{wco} ) k_{row}  }
  {S_g + S_w −S_{wco} }

where:

.. math::
  \begin{split}
  &S_{w} &=  \mbox{water saturation} \\
  &S_{g} &=  \mbox{gas saturation} \\
  &S_{wco}  &= \mbox{water connate saturation} \\
  &k_{row} &= \mbox{relative permeability of oil in water} \\
  &k_{rog} &=\mbox{relative permeability of oil in gas} 
  \end{split}


This model assumes that the oil can only see either the gas or the water, and may be regarded as assuming segregation between the phases.


This is the default model for the oil relative permeability in three-phase flow models, such has BLACK_OIL and SOLVENT_TL. When one of these flow options are selected, the code look for the functions or tables that define :math:`K_{row}` and :math:`K_{rog}` within the CHARACTERISTIC_CURVES block. if no found, or multple options are available for the saturation curves, the software returns an error.


If there is the need to specify the tables from which :math:`K_{row}` and :math:`K_{rog}` will be loaded, the KRO block can be used. Two options are possible, one specifies a table to define both :math:`K_{row}` and :math:`K_{rog}`, see example below: ::

  KRO ECLIPSE
    KRO_TABLE sof3_table
  END

The second options allow to read the two saturation curves from two different tables: ::

  KRO ECLIPSE
    KROW_TABLE krow_table
    KROG_TABLE krog_table
  END

KRO_TABLE must be followed by the name of the :ref:`TABLE<char_curves_table_sec>` defining both :math:`K_{row}` and :math:`K_{rog}`.

KROW_TABLE  must be followed by the name of the :ref:`TABLE<char_curves_table_sec>` defining :math:`K_{row}`.

KROG_TABLE must be followed by the name of the :ref:`TABLE<char_curves_table_sec>` defining :math:`K_{rog}`.

When using the two-table approach, the software will run a check to ensure that the end points of the two tables are consistent: the oil in water and oil in gas relative permeability must have the same value for the oil saturation maximum value, if not it will return an error.

