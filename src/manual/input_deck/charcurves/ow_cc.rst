
Oil-Water Model Characteristic curves
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
The oil water flow model (TOIL) requires the capillary pressure between the oil and water,
and relative permeability for the water and the oil in water.

Oil-Water saturation properties defined by tables - example
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

In the example below the tables needed to define :math:`P_{cow}`, :math:`K_{rw}`, and :math:`K_{row}` have been specified.
Each of the saturation property is defined unambiguously, so the keywords PC_OW_TABLE,
KRW_TABLE, and KROW_TABLE are not needed. ::

  CHARACTERISTIC_CURVES toil_sat_prop_table_example
    TABLE swfn_table
      PRESSURE_UNITS Pa
      EXTERNAL_FILE swfn_file.dat !contain SWFN
    END
    TABLE sof2_table
      EXTERNAL_FILE sof2_table.dat !contain SOF2
    END
  /

Oil-Water saturation properties by analytical functions - example
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
::

  CHARACTERISTIC_CURVES toil_sat_prop_func_example
    CAP_PRESSURE_FUNCTION_OW BROOKS_COREY
      WATER_CONNATE_SATURATION 0.0
      WATER_RESIDUAL_SATURATION 0.45
      LAMBDA 0.4d0
      ALPHA 0.5e-03
      MAX_CAPILLARY_PRESSURE 5.d5
    /
    PERMEABILITY_FUNCTION_WAT BURDINE_VG
      M 3.0
      WATER_CONNATE_SATURATION 0.0
      WATER_RESIDUAL_SATURATION 0.45
    END
    PERMEABILITY_FUNCTION_OW MOD_BROOKS_COREY
      M 1.0d0
      OIL_RESIDUAL_SATURATION 0.3d0
      MAX_REL_PERM 0.28d0 !if not included defaults to 1
      SMOOTH
    END
  /


