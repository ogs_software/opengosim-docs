
.. _sgfn_sec:

:index:`SGFN`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


SGFN is followed by a table with three columns, in which the first is the gas saturation (:math:`S_g`), the second is the gas relative permeability (:math:`K_{rg}`), and the third is the capillary pressure between the gas and the oil phase (:math:`P_{cog}`). The third column (:math:`P_{cog}`) is ignored when SGFN is used with the GAS_WATER or COMP3 flow option, as there is no oil. The table must be terminated by a ``/`` or ``END``. An example is shown below: ::

  SGFN
    0.00 0.00 0.00
    0.04 0.00 0.014
    0.10 0.02 0.034
    0.20 0.10 0.068
    0.30 0.24 0.10
    0.40 0.34 0.14
    0.50 0.42 0.17
    0.60 0.50 0.20
    0.70 0.81 0.24
    0.78 1.00 0.27
  /

The first gas saturation entry is the gas connate saturation, :math:`S_{gco}` (usually zero, as in the example), the largest value of the gas saturation for which the gas relative permeability is zero is the gas residual saturation :math:`S_{gcr}` (in the example above 0.04). The largest gas saturation value in the table is the maximum gas saturation, :math:`S_{g,\mbox{max}}`, usually :math:`1-S_{wcon}`, (0.78 in the example). :math:`S_{g}` must increase monotonically. :math:`K_{rg}` and :math:`P_{cog}` can be constant or increase as :math:`S_{g}` increases.

