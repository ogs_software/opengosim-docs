
.. _sgsfn_sec:

:index:`SGSFN`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``SGSFN`` is followed by a table with two columns, in which the first is the gas phase saturation
(:math:`S_g`), the second is the pure-solvent relative permeability in water (:math:`K_{rsw}`). The table must be terminated by a ``/`` or ``END``. The ``SGSFN`` table is used only for the COMP3 mode and if the composition-dependent saturation functions option (``COMP_DEP_SFNS``) has been activated. If
``COMP_DEP_SFNS`` is active and ``SGSFN`` is not found in the input, the code will take the relative
permeability for pure solvent in water from SGFN, treating it like gas. An example of the table is
shown below: ::

  SGSFN
    0.0 0
    0.05 0
    0.8 1.0
  /

The first gas saturation entry is the gas connate saturation, :math:`S_{gco}` (usually zero, as in the example), the largest value of the gas saturation for which the gas relative permeability is zero
is the gas residual saturation :math:`S_{gcr}` (in the example above :math:`0.05`). The largest gas saturation value
in the table is the maximum gas saturation, :math:`S_{g,\mbox{max}}`, usually :math:`1−S_{wcon}`, (:math:`0.8` in the example). :math:`S_g` must increase monotonically. :math:`K_{rsw}` can be constant or increase as :math:`S_g` increases.
