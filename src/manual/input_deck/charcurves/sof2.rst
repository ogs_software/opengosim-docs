
.. _sof2_sec:

:index:`SOF2`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The SOF2 is followed by a table with two columns, in which the first is the oil or
hydrocarbon saturation (:math:`S_o`  or :math:`S_h` ), and the second is the oil or hydrocarbon relative permeability (:math:`K_{ro}` or :math:`K_{rh}` ). The table must be terminated by a ``/`` or ``END``. An example is reported below: ::

  SOF2
  0    0
  0.18 0
  0.28 0.0001
  0.38 0.001
  0.43 0.01
  0.48 0.021
  0.58 0.09
  0.63 0.2
  0.68 0.35 
  0.76 0.7 
  0.83 0.98 
  0.86 0.997 
  0.88 1
  /

The first saturation value in the table is the oil or hydrocarbon connate saturation (:math:`S_{oco}`  or :math:`S_{hco}` ), which must be zero or the program will return an error. The largest oil or hydrocarbon saturation for which the oil or hydrocarbon relative permeability is zero is the oil or hydrocarbon critical saturation :math:`S_{ocr}` or :math:`S_{hcr}` (0.18 in the example above). The last saturation entry in the table is the maximum oil or hydrocarbon saturation (:math:`S_{o,\mbox{max}}` or :math:`S_{h,\mbox{max}}`), usually :math:`1-S_{wcon}`. :math:`S_o` or :math:`S_h` must increase monotonically. :math:`K_{ro}` or :math:`K_{rh}` can be constant or increase as :math:`S_o` increases.

