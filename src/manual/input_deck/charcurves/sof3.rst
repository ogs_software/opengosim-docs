
.. _sof3_sec:

:index:`SOF3`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The SOF3 is followed by a table with three columns, in which the first is the oil saturation
(:math:`S_o`), the second is the oil in water relative permeability (:math:`K_{row}`), the third is oil in gas relative permeability (:math:`K_{rog}`). The table must be terminated by a ``/`` or ``END``. An example is reported below: ::

  SOF3
  0    0       0
  0.2  0       0
  0.38 0.00432 0
  0.4  0.0048  0.004
  0.48 0.05288 0.02
  0.5  0.0649  0.036
  0.58 0.11298 0.1
  0.6  0.125   0.146
  0.68 0.345   0.33
  0.7  0.4     0.42
  0.74 0.7     0.6
  0.78 1       1
  /

The largest oil saturation for which the oil in water relative permeability is zero is the oil in water critical saturation :math:`S_{owcr}` (0.2 in the example), and the largest oil saturation for which the oil in gas relative permeability is zero is the oil in gas critical saturation :math:`S_{ogcr}` (0.38 in the example). :math:`S_o` must increase monotonically. :math:`K_{row}` and :math:`K _{og}` can be constant or increase as :math:`S_o` increases. At the maximum oil saturation, :math:`K_{row}` must be equal to :math:`K_{rog}` or the software will return an error.

