
Solvent Model Characteristic curves
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The solvent model requires the definition of two capillary pressure curves, for the interface
between the oil and water phases, and the interface of the oil and gas phases. In addition
relative permeabilities for the water, gas, and oil in water and in gas must be specified.

In this model the relative permeability of oil in water is re-used to describe the relative
permeability of the hydrocarbon phase (oil + gas + solvent) in the miscible limit.

For examples see :ref:`black oil characteristic curves<black_oil_cc_sec>`, as the saturation properties function are
defined in the same way.
