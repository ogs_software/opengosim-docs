
.. _swfn_sec:

:index:`SWFN`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


The SWFN is followed by a table with three columns, in which the first is the water saturation (:math:`S_w`), the second is the water relative permeability (:math:`K_{rw}`), and the third is the capillary pressure between the gas and water (:math:`P_{\mbox{cgw}}`), or between oil and the water phase (:math:`P_{\mbox{cow}}`). The table must be terminated by a ``/`` or ``END``. The third column is :math:`P_{\mbox{cgw}}` when the GAS_WATER or COMP3 modes are used, and is :math:`P_{\mbox{cow}}` for all other modes. An example is shown below: ::

  SWFN
    0.05 0    0.7
    0.22 0    0.48
    0.3  0.07 0.27
    0.4  0.15 0.2
    0.5  0.24 0.17
    0.6  0.33 0.14
    0.8  0.65 0.07
    0.9  0.83 0.03
    1    1    0
  /

The first saturation entry in the table is the water connate saturation, :math:`S_{wco}` (0.05 in this example). The largest water saturation value for which the water relative permeability is zero is the water residual saturation, :math:`S_{wcr}` (0.22 in this example). The last water saturation entry is the maximum water saturation, :math:`S_{w,\mbox{max}}` (usually 1 as in the example above). :math:`S_{w}` must increase monotonically. :math:`K_{rw}` can be constant or increase as :math:`S_{w}` increases. :math:`P_{cgw}` or :math:`P_{cow}` can be constant or decrease as :math:`S_{w}` increases.

