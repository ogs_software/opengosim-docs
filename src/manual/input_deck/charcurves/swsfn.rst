

.. _swsfn_sec:

:index:`SWSFN`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


The ``SWSFN`` keyword is followed by a table with three columns, in which the first is the water saturation
(:math:`S_w`), the second is the water relative permeability with respect to the solvent (:math:`K_{rws}`), and the
third is the capillary pressure between solvent and water (:math:`P_{csw}`). The table must be terminated by a ``/`` or ``END``. The ``SWSFN`` table is used only for the COMP3 mode and if the composition-dependent saturation functions option ``COMP_DEP_SFNS`` has been switched on. If ``COMP_DEP_SFNS`` is active and ``SWSFN`` is not found in the input, the code will assume that
the relative permeability and the capillary pressure of the water/solvent system are identical to
those for the gas/water system, and the values of ``SWFN`` will be used. An example of the table
is shown below: ::

  SWSFN
    0.2 0 3.1026
    0.25 0.0 1.3120
    1.0 1.0 0.0
  /


The first saturation entry in the table is the water connate saturation, :math:`S_{wco}` (:math:`0.2` in this example).

The largest water saturation value for which the water relative permeability is zero is the water
residual saturation, :math:`S_{wcr}` (:math:`0.25` in this example). The last water saturation entry is the maximum
water saturation, :math:`S_{w,\mbox{max}}` (usually :math:`1` as in the example above). :math:`S_w` must increase monotonically.
:math:`K_{rws}` can be constant or increase as :math:`S_w` increases. :math:`P_{csw}` can be constant or decrease as :math:`S_w` increases. Note that the end points of ``SWSFN`` and ``SWFN`` must be the same for the same ``SATNUM`` value, that is the same connate and maximum water saturations, for which the relative permeability and capillary pressure values must coincide.
