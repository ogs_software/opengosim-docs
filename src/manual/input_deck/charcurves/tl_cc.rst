
Todd-Longstaff Model Characteristic curves
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The Todd-Longstaff model requires the definition of the capillary pressure between the
hydrocarbon phase (oil + solvent) and the water. Relative permeabilities are needed for the
water and the hydrocarbon phases.

Todd-Longstaff saturation properties by tables - example
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:: 

  CHARACTERISTIC_CURVES tl_sat_prop_table_example
    KRW_TABLE swfn_table
    KRH_TABLE sof2_table
    TABLE swfn_table
      PRESSURE_UNITS Pa
      EXTERNAL_FILE swfn_file.dat
    END
    TABLE sof2_table
      EXTERNAL_FILE sof2_file.dat
    END
  /

the files swfn_file.dat and sof2_file.dat contain an SFWN and SOF2 table respectively.

Todd-Longstaff saturation properties by analytical function - example
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

  CHARACTERISTIC_CURVES tl_sat_prop_func_example
    CAP_PRESSURE_FUNCTION_OW CONSTANT
      CONSTANT_PRESSURE 0.0
    /
    PERMEABILITY_FUNCTION_WAT BURDINE_VG
      M 3.0
      WATER_CONNATE_SATURATION 0.0
      WATER_RESIDUAL_SATURATION 0.0
    END
    PERMEABILITY_FUNCTION_HC MOD_BROOKS_COREY
      M 2.0d0
      HYDROCARBON_RESIDUAL_SATURATION 0.0d0
    /
  /
