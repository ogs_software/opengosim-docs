.. _checkpoint_sec:

:index:`CHECKPOINT`
-------------------

CHECKPOINT is a sub-block and must be defined within SIMULATION.

CHECKPOINT saves one or more simulation states at specified times on binary files. These
files, often known as restart or checkpoint files, can be used at later stage to restart a
simulation from a state at a given time. When using GRDECL, only the HDF5 format is
supported for the restart files, and must be specified when defining the output of restart
files. HDF5 is a format that allows for efficient parallel reading and writing of restart files.::

  CHECKPOINT
    FORMAT <file_format>
    TIMES <time_unit> <t1>, <t2>, ....., <tn>
    PERIODIC TIMESTEP <number_of_time_steps>
  /

<file_format> = format type, only HDF5 allowed when using GRDECL

<t1>, <t2>, ....., <tn> = times for which restart file must be saved

<number_of_time_steps> = number time steps between two consecutive restart files
The times for which the restart files must be saved can be specified with two options:

* specifying the times using the TIMES keyword.
* specifying the number of time steps between two consecutive restart files, using the PERIODIC TIMESTEP keyword.

See examples below: ::

  CHECKPOINT
    FORMAT HDF5
    TIMES d 500.0
  /
  CHECKPOINT
    FORMAT HDF5
    PERIODIC TIMESTEP 20
  /

The simulator names the restart files in different ways, depending on the option used to
specify the physical times for which the files must be saved:

* for TIMES: input_deck_file_name-XXX.h5, where XXX is the physical time specified in TIMES,
* for PERIODIC TIMESTEP: input_deck_file_name-tsXXX.h5, where XXX is the time step number.

When including the CHECKPOINT keyword the simulator always saves the simulation last
state (final step), naming it input_deck_file_name-restart.h5.

