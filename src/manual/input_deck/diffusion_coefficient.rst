
.. _diff_coeff_sec:

:index:`DIFFUSION_COEFFICIENT`
-----------------------------------------------



The phase molecular diffusion coefficients may be entered in the GAS_WATER, COMP3 and COMP4 modes. These are entered in FLUID_PROPERTY blocks. See example: ::

  FLUID_PROPERTY
    PHASE LIQUID
    DIFFUSION_COEFFICIENT 2.0d-9
  /
  FLUID_PROPERTY
    PHASE GAS
    GAS_DIFFUSION_COEFFICIENT 2.0d-5
  /

The diffusion coefficients are entered in m2/sec.

If not entered, the diffusion coefficients default to the following values:
    * liq_diff_coeff = :math:`1.0 \times 10^{-9} m^2 /\mbox{sec}`
    * gas_diff_coeff = :math:`2.13 \times 10^{-5} m^2 / \mbox{sec}`

Internally the simulator uses the liquid molecular diffusion as entered by the user, while it modifies the gas molecular diffusion with corrections that accounts for pressure and temperature:


.. math::
  D_{\mbox{gas}} = \mbox{gas_diff_coeff}  \frac{101235}{P} \left[   \frac{T+273.15}{273.15} \right]^{1.8}


Note that the pressure and temperature in the equation above are in Pascal and degree Celsius.







