
EOS
---------------------------------------------------

To input :ref:`thermodynamic properties of reservoir fluids<thermodynamic_props_sec>`, |pftogs| uses cards of this naming scheme, described in more detail in that section. 

The full list is:

* :ref:`EOS WATER<eos_water_sec>`
* :ref:`EOS OIL<eos_oil_sec>`
* :ref:`EOS GAS<eos_gas_sec>`
* :ref:`EOS SOLVENT<eos_solv_sec>`
* :ref:`EOS COMP<eos_comp_sec>`

