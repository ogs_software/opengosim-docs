
.. _equil_sec:

:index:`EQUILIBRATION`
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Defines a hydrostatic equilibration that computes the distribution for pressure and saturation versus depth, and it is used to initialise a study. See example below: ::


  EQUILIBRATION initial_solution_1
    DATUM_D 1000.0 m
    PRESSURE 105.0 Bar
    RTEMP 50 C
    SALT_TABLE
      D_UNITS m
      CONCENTRATION_UNITS MOLAL
      SALTVD
       1000 0.95
       2500 3.0
      /
    END
  /

The set of EQUILIBRATION instructions can optionally be associated with a name (‘initial_solution_1’ in the example above). The input sub-cards supported by the EQUILIBRATION are:

  * :ref:`DATUM_D<equil_datum_d_sec>`
  * :ref:`PRESSURE<equil_pres_sec>`
  * :ref:`WGC_D<equil_wgc_d_sec>`
  * :ref:`PCWG_WGC<equil_pcwg_wgc_sec>`
  * :ref:`OWC_D<equil_owc_d_sec>`
  * :ref:`PCOW_OWC<equil_pcow_owc_sec>`
  * :ref:`OGC_D<equil_ogc_d_sec>`
  * :ref:`PCOG_OGC<equil_pcog_ogc_sec>`
  * :ref:`BUBBLE_POINT_TABLE<equil_bubble_point_table_sec>`
  * :ref:`RTEMP<equil_rtemp_sec>`
  * :ref:`TEMPERATURE_TABLE<temp_table_bo_fc_sec>`
  * :ref:`SALT_TABLE<equil_salt_table_sec>`
  * :ref:`GAS_IN_LIQUID_MOLE_FRACTION<equil_gilmf_sec>`
  * :ref:`SOLVENT_IN_LIQUID_MOLE_FRACTION<equil_silmf_sec>`
  * :ref:`ZMFVD<equil_zmfvd_sec>`
  * :ref:`NUM_EQUIL_NODES<equil_num_equil_nodes_sec>`


Additional considerations on how the study is initialised:

  * :ref:`Saturation Initialisation<sat_init_sec>`
  * :ref:`Equilibration aliases used before version OGS-1.6<eql_changes15_sec>`
  * :ref:`Multiple Equilibration Regions<eql_mult_sec>`

Examples:

  * :ref:`Gas Water Equilibration example<gw_eq_sec>`
  * :ref:`Black Oil/COMP4 Equilibration example<bo_eq_sec>`
  * :ref:`COMP Equilibration example<comp_eq_sec>`
  * :ref:`COMP3 Equilibration Example<comp3_eq_sec>`

.. _equil_datum_d_sec:

DATUM_D
++++++++++++++++++++

Depth of the reference datum. Require units. Defaults to 0.


.. _equil_pres_sec:

PRESSURE
++++++++++++++++++++

Reference pressure at datum. Must be entered or the code will return an error. Pressure units must be specified.

.. _equil_wgc_d_sec:

WGC_D
++++++++++++++++++++++++++++++++

Defines the depth of the water-gas phase contact. Requires units. If not set defaults to 0. It is used only by the :ref:`COMP3<multigas_comp3_intro_sec>` and :ref:`GAS_WATER<gw_intro_sec>` modes, and ignored by all other modelling options. Cannot be used in conjunction with OWC_D and OGC_D.

Note that for the GAS_WATER mode, if WGC_D is entered, this must be equal to DATUM_D, as the simulator will use the reference pressure to compute the water composition in the gas-water transition zone.

.. _equil_pcwg_wgc_sec:

PCWG_WGC
++++++++++++++++++++++++++++++++++++

Defines the capillary pressure between the water and the gas phases at the depth of the water-gas phase contact. It requires pressure units. If not set defaults to 0. It is used only by the :ref:`COMP3<multigas_comp3_intro_sec>` and :ref:`GAS_WATER<gw_intro_sec>` modes, and ignored by all other modelling options.


.. _equil_owc_d_sec:

OWC_D
++++++++++++++++++++++++++++++++++++++

Defines the depth of the oil-water phase contact. Requires units. If not set defaults to 0. It is ignored by the :ref:`GAS_WATER<gw_intro_sec>` and the :ref:`COMP3<multigas_comp3_intro_sec>` modes, which do not model the oil phase. Cannot be used in conjunction with WGC_D.

.. _equil_pcow_owc_sec:


PCOW_OWC
+++++++++++++++++++++++++++++++++++++++++

Defines the capillary pressure between the water and the oil phases at the depth of the oil-water phase contact. It requires pressure units. If not set defaults to 0. It is ignored by the :ref:`GAS_WATER<gw_intro_sec>` and the :ref:`COMP3<multigas_comp3_intro_sec>` modes, which do not model the oil phase.


.. _equil_ogc_d_sec:

OGC_D
+++++++++++++++++++++++++

Defines the depth of the oil-gas phase contact. It requires units. If not set defaults to 0. It is ignored by the :ref:`GAS_WATER<gw_intro_sec>`, :ref:`COMP3<multigas_comp3_intro_sec>` and :ref:`TOIL_IMS<toil_intro_sec>` modes. Cannot be used in conjunction with WGC_D.

.. _equil_pcog_ogc_sec:

PCOG_OGC
++++++++++++++++++++++++++++

Capillary pressure between the oil and the gas phases at the depth of the oil-gas phase contact. Requires pressure units. If not set default to 0. It is ignored by the :ref:`GAS_WATER<gw_intro_sec>`, :ref:`COMP3<multigas_comp3_intro_sec>` and :ref:`TOIL_IMS<toil_intro_sec>` modes.

.. _equil_bubble_point_table_sec:

BUBBLE_POINT_TABLE
++++++++++++++++++++++++++++++++

The BUBBLE_POINT_TABLE defines the bubble point pressure (:math:`P_b`) versus depth for all grid blocks associated with the hydrostatic equilibration. The table is used by the :ref:`BLACK_OIL<bo_intro_sec>`, :ref:`COMP4<multigas_intro_sec>` and :ref:`SOLVENT_TL<tl4p_intro_sec>` modes, and it is ignored by the other modelling options. See example below: ::

  BUBBLE_POINT_TABLE
     D_UNITS m
     PRESSURE_UNITS Bar
     PBVD
       2430.0 333.0
       2470.0 335.0
    /
  END

Units must be defined using D_UNITS and PRESSURE_UNITS. The data depth vs bubble point pressure are entered within the PBVD block.

The entries must be ordered for monotonically growing depths, and at least two entries are required for the internal interpolation to work. In the hydrostatic equilibration calculation, if for a given depth a bubble point pressure is found to be larger than the oil hydrostatic pressure, its value is reset to the hydrostatic oil pressure value to ensure :math:`P_b \leq P_o`.

To remove the PBVD table, the entire ``BUBBLE_POINT_TABLE`` block must be removed. If only the PBVD table is removed leaving the ``BUBBLE_POINT_TABLE`` block, the code returns an error as a PBVD is expected.

If the PBVD table is not defined, the datum and the oil-gas phase contact must have the same location ( DATUM_D = OGC_D), and the bubble point pressure below the OGC (i.e. undersaturated oil) is taken constant and equal to the pressure at datum. See example: ::

  EQUILIBRATION initial_condition
    PRESSURE 327.0 Bar
    RTEMP 50 C
    DATUM_D 2430.0 m
    OGC_D     2430.0 m
    PCOG_OGC 0.5 Bar
    OWC_D 2470.0 m
    PCOW_OWC 0.5 Bar
  /

.. _equil_rtemp_sec:

RTEMP
++++++++++++++++++++

Constant temperature value to assign to the reservoir. Note that currently only degrees Celsius are accepted, for any other temperature unit the software returns an error. RTEMP is an alternative to :ref:`TEMPERATURE_TABLE<temp_table_bo_fc_sec>`, either one or the other can be supplied.



.. _temp_table_bo_fc_sec:

TEMPERATURE_TABLE
++++++++++++++++++++++++++++++++

Define the temperature versus depth for all grid blocks associated with the hydrostatic equilibration. See example: ::

  TEMPERATURE_TABLE
    D_UNITS m
    TEMPERATURE_UNITS C
    RTEMPVD
      2300.0 126.0
      2400.0 128.0
    /
  END

Units must be defined using D_UNITS and TEMPERATURE_UNITS. The data depth vs temperature are entered within the RTEMPVD block.
The entries must be ordered for monotonically growing depths, and at least two entries are required for the internal interpolation to work.

If the TEMPERATURE_TABLE is defined RTEMP cannot be supplied at the same time in the same equilibration, otherwise the code returns an error, as the temperature is ambiguously defined. To remove the ``RTEMPVD`` table, the entire ``TEMPERATURE_TABLE`` block must be removed. If only the ``RTEMPVD`` table is removed leaving the ``TEMPERATURE_TABLE`` block, the code returns an error as a ``RTEMPVD`` table is expected.

.. _equil_salt_table_sec:

SALT_TABLE
++++++++++++++++++++

Defines the brine salinity versus depth for all grid blocks associated with the hydrostatic equilibration. It is used by the :ref:`COMP<comp_intro_sec>`, :ref:`COMP3<multigas_comp3_intro_sec>` and :ref:`COMP4<multigas_intro_sec>`, and :ref:`GAS_WATER<gw_intro_sec>` modes, while it is ignored by the other flow models. See example below: ::

  SALT_TABLE
      D_UNITS m
      CONCENTRATION_UNITS MOLAL
      SALTVD
       1000 0.95
       2500 3.0
      /
  END

Units must be defined using D_UNITS and CONCENTRATION_UNITS. CONCENTRATION_UNITS can take three different values, depending on which units the brine salinity values are entered:

    - MOLAL, for salt concentrations in molality (m).
    - MASS or MASS_FRACTION, for salt concentrations given as mass fractions (kg-salt/kg-brine).
    - MOLE or MOLE_FRACTION,  for salt concentrations given as mole fractions, :math:`\frac{n_{NaCl}}{(n_{NaCl} + n_{H2O})}`

In all three cases the values entered do not account for the dissolved |CO2| in brine.

The data depth vs salinity are entered within the SALTVD block. The entries must be ordered for monotonically growing depths, and at least two entries are required for the internal interpolation to work.

If BRINE has already been used in the input deck to define a constant salinity, SALT_TABLE will overwrite it. If the user specifies salinity by SALTVD, the BRINE value is also required for the conversion to brine surface volumes. If not entered the code returns an error.


.. _equil_gilmf_sec:

GAS_IN_LIQUID_MOLE_FRACTION
++++++++++++++++++++++++++++++++++++

Defines the initial mole fraction of gas in water, dimensionless. If not set, defaults to 0. It is used only by the :ref:`GAS_WATER<gw_intro_sec>` mode. If the NOGASSOL option is active, i.e. dissolved gas is not modelled, this card cannot be entered or the simulator throws an error.

Note that if WGC_D is entered, this is overwritten by the gas saturated mole fraction computed in the transition zone, which is extrapolated at constant value in the aquifer below.

.. _equil_silmf_sec:

SOLVENT_IN_LIQUID_MOLE_FRACTION
++++++++++++++++++++++++++++++++++++

Defines the initial mole fraction of solvent in brine, dimensionless. If not set, defaults to 0. It is used only by the  :ref:`COMP3<multigas_comp3_intro_sec>` and :ref:`COMP4<multigas_intro_sec>` modes. If the NOSLVSOL option is active, i.e. dissolved solvent is not modelled, this card cannot be entered or the simulator throws an error


.. _equil_zmfvd_sec:

ZMFVD
++++++++++++++++++++++++++++++++++++++++++

Defines the reservoir non-aqueous phase composition versus depth for all grid blocks associated with the hydrostatic equilibration. It is used only by :ref:`COMP<comp_intro_sec>` mode. See example below: ::

  ZMFVD m frac
     1000.0 0.0315  0.6599 0.0869 0.0591 0.0967 0.0472 0.0153 0.0034
     3000.0 0.0315  0.6599 0.0869 0.0591 0.0967 0.0472 0.0153 0.0034
  END

The ZMFVD keyword is followed by the length and composition units, (m, frac) in the example above. The first can take any of the :ref:`length units<unit-conventions>` accepted by the simulator, for the composition units two values are accepted [frac, percent]: frac requires the composition to be entered as mole fraction, percent requires the composition to be entered by percentage. 

The non-aqueous phase composition can be defined by either ZMFVD in EQUILIBRATION or ZMF in :ref:`EOS COMP<eos_comp_sec>`, if the code finds both definitions in the input throws an error.


.. _equil_num_equil_nodes_sec:

NUM_EQUIL_NODES
++++++++++++++++++++++++++++++++++++

Defines the number of nodes used in the internal hydrostatic equilibration calculation. Below is an example:  ::


  EQUILIBRATION initial_sol
    DATUM_D 1000.0 m
    PRESSURE 105.0 Bar
    RTEMP 50 C
    GAS_IN_LIQUID_MOLE_FRACTION 1.D-4
    NUM_EQUIL_NODES 2001
  /

The reservoir vertical extension is divided into (NUM_EQUIL_NODES – 1) equispaced depth intervals, and the one-dimensional grid obtained is used to carry out the hydrostatic equilibration calculation. If not entered,  NUM_EQUIL_NODES defaults to 501. In some cases it might be beneficial to test higher resolutions than the default one to get a more accurate initial state.


.. _sat_init_sec: 

Saturation Initialisation
++++++++++++++++++++++++++++++++

Away from the transition zones, in the gas-saturated region, the gas maximum saturation is computed as :math:`S_{g,max}=1–S_{wco}`, where :math:`S_{wco}` is the connate water, which is the smallest saturation value entered in the SWFN table, or explicitly entered when using analytical water saturation functions.

Away from the transition zones, in the water-saturated region, the maximum water saturation is computed as :math:`S_{w,max}=1–S_{gco}`, where :math:`S_{gco}` is gas connate saturation, which is the smallest saturation value in the SGFN table or explicitly entered when using analytical gas saturation functions. Note that usually :math:`S_{gco}=0`.

The minimum and maximum values entered in SWFN and SGFN table or in the analytical saturation functions should be consistent, i.e :math:`S_{g,max} =1 – S_{w,min}` and :math:`S_{w,max} =1 – S_{g,min}`. If these maximum values are not respected, they are reset automatically by software.

In the oil region, the oil saturation is computed as :math:`S_o=1–S_w–S_g`. In case of overlap between the oil-water and oil-gas transition zones, a negative oil saturation may result from the calculation. In this region the software assumes no presence of oil, i.e. :math:`S_o=0` and :math:`S_w=1−S_g`, and solves a non-linear pressure equilibration in Sw using the water and gas capillary pressure functions:


.. math::

  P_{cow}(S_w)+P_{cog}(S_g=1−S_w)=P_g–P_w.

The overall equilibration calculation assumes that the oil is lighter than the water, and the gas is lighter than the oil. If the user input do not reflects such assumption, an error will be thrown.

.. _eql_changes15_sec:

.. _equil_15_changes_sec:

EQUILIBRATION aliases used before version OGS-1.6
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

The EQUILIBRATION keyword has two aliases: EQUIL and FLOW_CONDITION. Versions older than OGS-1.6 support only :ref:`FLOW_CONDITION<flow_cond_sec>`, which has to be used in combination with :ref:`INITIAL_CONDITION<init_cond_sec>` and :ref:`REGION<region_sec>`. From OGS-1.6, EQUILIBRATION is the only keyword needed to initialise a study by hydrostatic equilibration.

.. _eql_mult_sec:

Multiple Equilibration Regions
++++++++++++++++++++++++++++++++++++++++

It is possible to define multiple equilibration regions, inserting a series of EQUILIBRATION cards, see example below: ::

  EQUILIBRATION equil1
    PRESSURE 260.0 Bar
    DATUM_D 2600.0 m
    OWC_D 2500.0 m
    RTEMP 120 C
  /

  EQUILIBRATION equil2
    PRESSURE 260.0 Bar
    DATUM_D 2600.0 m
    OWC_D 2000.0 m
    RTEMP 120 C
  /

For example this can be useful to define different phase contact depths, a situation that can be modelled by the simulator, as long as the two phase transition zones are not connected (e.g. two distinct oil traps). The equilibration data to be used by each grid block in the initialisation of the study is defined by the EQLNUM keyword found in the grdecl include file. EQLNUM associates each cell to an EQUILIBRATION data set, with the EQLNUM integer indices referring to the order in which EQUILIBRATION data sets are found in the input. That is, an EQLNUM value of 1 associates the cell with the first EQUILIBRATION block, 2 with the second and so on.



.. _gw_eq_sec:

Gas-Water Equilibration example
++++++++++++++++++++++++++++++++++++++++++++++++++
Below is an example of a hydrostatic equilibration for a Gas-Water model: ::

  EQUILIBRATION initial_sol
    DATUM_D 1000.0 m
    PRESSURE 105.0 Bar
    RTEMP 50 C
    GAS_IN_LIQUID_MOLE_FRACTION 1.D-4
    SALT_TABLE
      D_UNITS m
      CONCENTRATION_UNITS MOLAL
      SALTVD
       1000 0.95
       2500 3.0
      /
    END
  /

.. _bo_eq_sec:

Black Oil/COMP4 Equilibration example
++++++++++++++++++++++++++++++++++++++++++++++++

Below is an example of a hydrostatic equilibration for a black oil model, with variable :math:`P_b` and temperature: ::

  EQUILIBRATION initial_sol
    PRESSURE 330.0 Bar
    DATUM_D 2480.0 m
    OGC_D 2430.0 m
    PCOG_OGC 0.5 Bar
    OWC_D 2470.0 m
    PCOW_OWC 0.5 Bar
    BUBBLE_POINT_TABLE
      D_UNITS m
      PRESSURE_UNITS Bar
      PBVD
        2430.0 333.0
        2470.0 335.0
      /
    END
    TEMPERATURE_TABLE
      D_UNITS m
      TEMPERATURE_UNITS C
      RTEMPVD
        2300.0 126.0
        2400.0 128.0
      /
    END
  /

.. _comp_eq_sec:

COMP Equilibration Example
++++++++++++++++++++++++++++++++++++++++++++++++

Below is an example of a hydrostatic equilibration for :ref:`COMP<comp_intro_sec>` mode, with non-aqueous composition defined with ZMFVD: ::
 
  EQUILIBRATION equil_1

    OIL_PRESSURE 3550 psi
    DATUM_D  7500 ft
    OGC_D    7499.0 ft
    OWC_D    7500.0 ft
    PCOG_OGC 0.0    Bar
    PCOW_OWC 0.0    Bar

    ZMFVD m frac
       1000.0 0.0315  0.6599 0.0869 0.0591 0.0967 0.0472 0.0153 0.0034
       3000.0 0.0315  0.6599 0.0869 0.0591 0.0967 0.0472 0.0153 0.0034
    END
    
    RTEMP 93.4 C
  /

.. _comp3_eq_sec:

COMP3 Equilibration Example
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Below is an example of a hydrostatic equilibration for a :ref:`COMP3<multigas_comp3_intro_sec>` model, which uses a gas-water phase contact to define the gas cap location: ::

  EQUILIBRATION

    PRESSURE 330.94 Bar 
    DATUM_D  2550.0 m
    WGC_D    2550.0 m
    PCWG_WGC 0.0    Bar

    RTEMP 75.0 C
  /
