.. _erestart_sec:

:index:`ERESTART`
---------------------------

If present, ERESTART must be defined within :ref:`SIMULATION<simulation_card_sec>`.

ERESTART is used to restart a simulation from a given time. However, in the
case of ERESTART the solution is loaded from an Eclipse restart file, which may have been
written out by a Pflotran base run using the :ref:`eclipse output keywords <eclipse_files_sec>` keywords.

To reload from a unified Eclipse output file such as base.UNRST at 1000 days, use: ::

  ERESTART base 1000 d

To reload from an Eclipse multiple file called base.X0014, use: ::

  ERESTART base.X0014 1000 d
  
If you are not certain which multiple restart file contains the required time, specify: ::

  ERESTART base.X0000 1000 d

when the required time is not found on the nominated file, Pflotran will sweep through the files looking for the required time.

When using ERESTART it is possible to define or remove local grid refinements (:ref:`LGR<carfin_sec>`) on a restart run, for more information see:


.. toctree::
  :maxdepth: 1

  restart_lgrs
