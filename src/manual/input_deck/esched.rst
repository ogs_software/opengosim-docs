
.. role:: red

.. _esched_sec:

:index:`ESCHED`
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The ESCHED keyword expects the name of an include file with extension .esched, where a basic subset
of the Eclipse ``SCHEDULE`` keywords can be defined using the Eclipse syntax (``WELSPECS``, ``COMPDAT``,
etc.). For example: ::

  #====== wells ===========
  esched injection_model.esched

Note that the same well may be entered in both ``ESCHED`` and :ref:`WELL_DATA<well_data_sec>` input – the simulator will
process internally both inputs into an event table for that well. For the conventions and the keywords supported within the .esched include file, see list below



.. contents::
  :backlinks: top
  :depth: 2
  :local:


.. _esced_grdecl_sec:

Conventions within esched input files
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Within a .esched file the data syntax and conventions are those of Eclipse rather than PFLOTRAN-OGS, in the following respects:

*  The data is placed on a new line, after the keyword. The values are normally delimited by spaces (although tabs are accepted), and may be split across a number of lines. Data will be read until either the correct number of items have been processed, or a / terminating character is encountered. If all the expected values are present, a / character may still be added and will do no harm.


* Defaults may be used – for example, 3* implies 3 default values.


* Depths are measured downwards.

* The basic ordering scheme is natural ordering – with the I-index changing fastest, then the J-index, and with the K-index changing slowest. The K-order is down the columns of cells.


Within each keyword, arguments that are not supported - marked in :red:`red` in this wiki - are mainly ignored
by the simulator.


COMPDAT
^^^^^^^^^^^^^^

Defines data for the completions of one or more wells, example: ::

  COMPDAT
  INJE1 1 1 1 1 OPEN 6* Z /
  PROD1 10 10 1 1 OPEN 6* Z /
  /

Each record specifies the data of a completion, or a set completions spanning a given K-range, and can take up to 14 arguments: ::

  COMPDAT
  arg1 arg2 arg3 arg4 arg5 arg6 arg7 arg8 arg9 arg10 arg11 arg12 arg13 arg14 /

Some of these arguments are not supported and ignored by the simulator, highlighted in :red:`red` below:

  arg1 arg2 arg3 arg4 arg5 arg6 :red:`arg7` arg8 arg9 arg10 arg11 :red:`arg12` arg13 arg14 /

The arguments are as follows:

.. list-table::

  * - **Argument**
    - **Meaning**
  * - arg1 
    - Well name.
  * - arg1 
    - name of the well the completion belongs to.
  * - arg2 
    - I-location of completed grid block.
  * - arg3 
    - J-location of completed grid block.
  * - arg4 
    - K-location of the shallower completed grid block, for the set of completions associated with record.
  * - arg5 
    - K-location of the deeper completed grid block, for the set of completions associated with record.
  * - arg6 
    - OPEN/SHUT, indicates if a completion is open or shut. AUTO and HEAT are read but ignored. Defaults to OPEN.
  * - :red:`arg7`
    - :red:`Saturation table number of the pseudo relative permeability used for the well connection. This is not supported, and a non-default value will result in the software throwing an error.`
  * - arg8 
    - Well transmissibility factor, also known as completion connection factor (CCF). If defaulted or set to zero, the CCF is computed internally, using the radius (arg9), the connection’s effective Kh (arg10) and the skin factor (arg11). If a CCF > 0 is found, its value is used directly by the simulator, skipping the internal calculation. The CCF units are:

	* cP.rm3/day/bar, if |linkMETRIC| units are specified for the .esched include file
	* cP.rb/day/psi, if |linkFIELD| units are specified for the .esched include file

  * - arg9 
    - Well diameter, used to compute the CCF if arg8 is defaulted, ignored otherwise. Defaults to 0.3048 m. Its units are:

	* m, if |linkMETRIC| units are specified for the .esched include file 
	* ft, if |linkFIELD| units are specified for the .esched include file 
  * - arg10 
    - The connection’s effective :math:`K_h` (permeability x thickness). If defaulted is computed internally using the grid block data. If a value > 0 is found, and the CCF value in arg8 is defaulted, this quantity is used to compute the CCF, ignored otherwise. The :math:`K_h` units are:

	* mD.m, if |linkMETRIC| units are specified for the .esched include file.
	* mD.ft, if |linkFIELD| units are specified for the .esched include file.

  * - arg11 
    - Skin factor, used to compute the CCF if arg8 is defaulted, ignored otherwise. It is dimensionless. Defaults to zero.
  * - :red:`arg12`
    - :red:`D-factor to model non-Darcy flow effects. Not supported, and a non-zero or non-default value will result in the software throwing an error.`
  * - arg13 
    - Well drilling direction, can only take three values: X, Y and Z. Defaults to Z.
  * - arg14 
    - Pressure equivalent radius :math:`r_0` , which is used to compute the CCF if arg8 is defaulted, ignored otherwise. If a value > 0 is not found, it is computed internally using the grid block data. Its units are:

	- m, if |linkMETRIC|  units are specified for the .esched include file
	- ft, if |linkFIELD| units are specified for the .esched include file


The computation of CCF, :math:`K_h` and :math:`r_0` are done accordingly to the Peaceman formulae (see Equation :ref:`(2)<well_mod_peacman_eq>` in the :ref:`well model<well_model>`).

.. _esced_dates_sec:

DATES
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Indicates dates for which the simulator is asked to output restart file and print report information. See example: ::

  DATES
  1 Mar 2000 /
  1 Feb 2001 /
  /

Each record expect a maximum of 4 arguments: ::

  DATES
  arg1 arg2 arg3 arg4 /
  /


Some of these arguments are not supported and ignored by the simulator, highlighted in :red:`red` below:

  arg1 arg2 arg3 :red:`arg4` /

The arguments are as follows:

.. list-table::

  * - **Argument**
    - **Meaning**
  * - arg1 
    - Well name.
  * - arg1 
    - Day of the month, from 1 to 31.
  * - arg2 
    - Short name for the month in three characters, one of the following; 

        * JAN, FEB, MAR, APR, MAY, JUN, JUL/JLY, AUG, SEP, OCT, NOV, DEC.
  * - arg3 
    - 4 digits specifying a year (e.g. 1989). 
  * - :red:`arg4`
    - :red:`Time in the format HH:MM:SS. Defaults to 00:00:00. Values different than the defaults are ignored.`


.. _esched_field_sec:

FIELD
^^^^^^^^^^^^^^^^
If this keyword is found, field units are assumed for data entered in the .esched include file. By default units are assumed to be metric.


.. _esched_metric_sec:

METRIC
^^^^^^^^^^^^^^^^^^
If this keyword is found, metric units are assumed for data entered in the .esched include file. By default units are already assumed to be metric.

WCONINJE
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Used to enter data to control injection wells. Example below: ::

  WCONINJE
  INJE1 GAS OPEN RATE 1000000 1* 500 /
  /

Up to 15 arguments are expected for each well. Each record corresponds to a well: ::

  WCONINJE
  arg1 arg2 arg3 arg4 arg5 arg6 arg7 arg8 arg9 arg10 arg11 arg12 arg13 arg14 arg15 /
  /

Some of these arguments are not supported and ignored by the simulator, highlighted in :red:`red` below:

  arg1 arg2 arg3 arg4 arg5 :red:`arg6` arg7 :red:`arg8` :red:`arg9` :red:`arg10` :red:`arg11` :red:`arg12` :red:`arg13` :red:`arg14` :red:`arg15` /


The arguments are as follows:

.. list-table::

  * - **Argument**
    - **Meaning**
  * - arg1 
    - Well name.
  * - arg2 
    - Injector type. The options supported are: GAS, WATER or SOLVENT. For any other injector type the simulator will throw an error.
  * - arg3 
    - Status of the well. OPEN and SHUT supported, everything else is ignored (:red:`STOP and AUTO not supported`).
  * - arg4 
    - Control mode. Only two options are supported, RATE and BHP. If any other option is found, this is ignored, and RATE is assumed.
  * - arg5 
    - Surface flow rate target or upper limit. The units are:

       * sm3 /day, if |linkMETRIC| units are specified for the .esched include file
       * stb/day, if |linkFIELD| units are specified for the .esched include file
  * - :red:`arg6`
    - :red:`Reservoir fluid volume rate target or upper limit. Not supported, if a value is found, the simulator ignores it, considering no target or upper limit.`
  * - arg 7 
    - BHP target or upper limit. The units are:

	  * barsa, if |linkMETRIC| units are specified for the .esched include file
	  * psi, if |linkFIELD| units are specified for the .esched include file
  * - :red:`arg8`
    - :red:`THP target or upper limit. Not supported; if a value is entered, the simulator ignores it, considering no THP target or upper limit.`
  * - :red:`arg9` 
    - :red:`Injection well VFP number. Not supported – value ignored.`
  * - :red:`arg10-15` 
    - :red:`Not supported, values ignored.`

WCONPROD
^^^^^^^^^^^^^^^^^^^^^^^^

Enters data to control production wells. Example below: ::

  WCONPROD
  PROD1 OPEN WRAT 1* 10000 3* 100 /
  /

Up to 20 arguments are expected for each well. Each record corresponds to a well: ::

  WCONPROD
  arg1 arg2 arg3 arg4 arg5 arg6 arg7 arg8 arg9 arg10 arg11 ... arg20 /
  /

Some of these arguments are not supported and ignored by the simulator, highlighted in :red:`red` below:

  arg1 arg2 arg3 arg4 arg5 arg6 arg7 :red:`arg8` arg9 :red:`arg10 arg11 ... arg20` /


The arguments are as follows:

.. list-table::

  * - **Argument**
    - **Meaning**
  * - arg1 
    - Well name.
  * - arg2 
    - Status of the well. OPEN and SHUT supported, everything else is ignored (:red:`STOP and AUTO not supported`).
  * - arg3 
    - Control mode. Any other option than those supported is ignored, and the well produces at the BHP limit without any rate target or upper limits. The following options are supported: 

        * ORAT, WRAT, GRAT, LRAT, BHP. 

  * - arg4 
    - Oil rate target or upper limit. Defaults to no target or upper limit. The units are:

	* sm3 /day, if |linkMETRIC| units are specified for the .esched include file
	* stb/day, if |linkFIELD| units are specified for the .esched include file
  * - arg5 
    - Water rate target or upper limit. Defaults to no target or upper limit. The units are:

	* sm3 /day, if |linkMETRIC| units are specified for the .esched include file
	* stb/day, if |linkFIELD| units are specified for the .esched include file
  * - arg6 
    - Gas rate target or upper limit. Defaults to no target or upper limit. The units are:

	* sm3/day, if |linkMETRIC| units are specified for the .esched include file
	* Mscf/day, if |linkFIELD| units are specified for the .esched include file
  * - arg7 
    - Liquid rate target or upper limit. Defaults to no target or upper limit. The units are:

	* sm3/day, if |linkMETRIC| units are specified for the .esched include file
	* stb/day, if |linkFIELD| units are specified for the .esched include file
  * - :red:`arg8`
    - :red:`Reservoir fluid volume rate target or upper limit. No supported – the simulator ignores any value entered and assumes no target or upper limit.`
  * - arg9 
    - BHP target or lower limit. Defaults to 10.133 barsa. The units are:

	* barsa, if |linkMETRIC| units are specified for the .esched include file
	* psi, if |linkFIELD| units are specified for the .esched include file
  * - :red:`arg10`
    - :red:`THP target or lower limit. No supported – the simulator ignores any value entered and assumes no target or lower limit.`
  * - :red:`arg11-20`
    - :red:`Not supported, values ignored.`


WELLOPEN
^^^^^^^^^^^^^^^^^^^^^^^^^^

Reopens one or more wells previously defined and shut. Example:  ::

  WELLOPEN
  INJE1 /
  INJE2 /
  /

Each record corresponds to a well, and has only one argument, which is the well name. ::

  WELLOPEN
  arg1 /
  /

.. list-table::

  * - **Argument**
    - **Meaning**
  * - arg1 
    - Well name.

WELLSHUT
^^^^^^^^^^^^^^^^

Shuts one or more wells previously defined and open. Example:  ::

  WELLSHUT
  PROD1 /
  INJE1 /
  /

Each record corresponds to a well, and has only one argument, which is the well name. ::

  WELLSHUT
  arg1 /
  /

.. list-table::

  * - **Argument**
    - **Meaning**
  * - arg1 
    - Well name.


WELLSPEC
^^^^^^^^^^^^^^^^^^^^^^^^^^^

Introduces one or more wells, specifying name, location of the well head and the reference depth of the BHP. Example below: ::

  WELLSPEC
  INJE1 Field 1 1 -1.0 /
  PROD1 'GG' 10 10 2000 /
  /

Each record corresponds to a well, and can have up to 7 arguments: ::

  WELLSPEC
  arg1 arg2 arg3 arg4 arg5 arg6 arg7 /
  /

Some of these arguments are not supported and ignored by the simulator, highlighted in :red:`red` below:

  arg1 arg2 arg3 arg4 arg5 :red:`arg6` :red:`arg7` /

The arguments are as follows:

.. list-table::

  * - **Argument**
    - **Meaning**
  * - arg1 
    - Well name.
  * - arg2 
    - Name of the group the well belongs to. Defaults to Field.
  * - arg3 
    - I-location of the well head.
  * - arg4 
    - J-location of the well head.
  * - arg5 
    - BHP reference depth. Defaults to the depth of the centre of the grid block that contains the shallowest well connection. Has the following units:

	* sm3/day, if |linkMETRIC| units are specified for the .esched include file
	* stb/day, if |linkFIELD| units are specified for the .esched include file
  * - :red:`arg6-7`
    - :red:`Not supported – the simulator ignores these arguments.`


WELSPECS
^^^^^^^^^^^^^^^^^^^^^^^

Introduces one or more wells, specifying name, location of the well head and the reference depth of the BHP. Example below: ::

  WELSPECS
  INJE1 Field 1 1 -1.0 /
  PROD1 'GG' 10 10 2000 /
  /

Each record corresponds to a well, and can have up to 17 arguments: ::

  WELSPECS
  arg1 arg2 arg3 arg4 arg5 arg6 ... arg17 /
  /

Some of these arguments are not supported and ignored by the simulator, highlighted in :red:`red` below:

  arg1 arg2 arg3 arg4 arg5 :red:`arg6` ... :red:`arg17` /

The arguments are as follows:

.. list-table::

  * - **Argument**
    - **Meaning**
  * - arg1 
    - Well name.
  * - arg2 
    - Name of the group the well belongs to. Defaults to Field.
  * - arg3 
    - I-location of the well head.
  * - arg4 
    - J-location of the well head.
  * - arg5 
    - BHP reference depth. Defaults to the depth of the centre of the grid block that contains the shallowest well connection. Has the following units:

	* sm3 /day, if |linkMETRIC| units are specified for the .esched include file
	* stb/day, if |linkFIELD| units are specified for the .esched include file

  * - :red:`Arg6-17`
    - :red:`Not supported - the simulator ignores these arguments.`


TIME
^^^^^^^^^^^^^^^^^^^^^^^^^^^

Introduces one or more report times. An example is: ::

  TIME
  200 300 365 /

When multiple values are included, their value must grow with the order in which are entered. The time units is day for both |linkMETRIC| and |linkFIELD| units.

If one of the dates specified is beyond the final simulation time found in the main input deck, this will be ignored.


WCONINJH
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Controls an injection well using observed historical data (instead of :ref:`WCONINJE`). Used for history matching analysis (see :ref:`history_data_sec` for the native PFLOTRAN-OGS format). 
Historical data such as rates and BHP can be introduced, although only one can be used to control the well. An injector well using history matching is treated differently to a normal injection well in the following way:

    - Observed injection rates and BHP are written to the summary file. (The mnemonic has a *h* added at the end).
    - Wells can only have one control model (other observed data is just reported). Available control modes are RATE and BHP.
    - For rate controls the BHP limit is set to a very high value automatically to avoid it switching to BHP control mode.
    
Example below: ::

    WCONINJH
        inj1 GAS   OPEN 1000000 330 6* BHP /
    /

Up to 12 arguments are expected for each well. Each record corresponds to a well: ::

  WCONINJH
  arg1 arg2 arg3 arg4 arg5 arg6 arg7 arg8 arg9 arg10 arg11 arg12 /
  /

Some of these arguments are not supported and are ignored by the simulator, highlighted in :red:`red` below:

  arg1 arg2 arg3 arg4 arg5 :red:`arg6` :red:`arg7` :red:`arg8` :red:`arg9` :red:`arg10` :red:`arg11` arg12 /


The arguments are as follows:

.. list-table::

  * - **Argument**
    - **Meaning**
  * - arg1 
    - Well name.
  * - arg2 
    - Injector type. The options supported are: GAS, WATER or SOLVENT. For any other injector type the simulator will throw an error.
  * - arg3 
    - Status of the well. OPEN and SHUT supported, everything else is ignored (:red:`STOP and AUTO not supported`).
  * - arg4 
    - Observed Surface flow rate. The units are:

       * sm3 /day, if |linkMETRIC| units are specified for the .esched include file
       * stb/day for water, Mcf/day for gas, If |linkFIELD| units are specified for the .esched include file.
       
  * - arg5 
    - Observed bottom hole pressure BHP. The units are:
    
	    * barsa, if |linkMETRIC| units are specified for the .esched include file
	    * psi, if |linkFIELD| units are specified for the .esched include file
  * - :red:`arg6-11` 
    - :red:`Not supported, values ignored.`
  * - arg12 
    - Control mode. Only two options are supported, RATE and BHP.

WCONHIST
^^^^^^^^^^^^^^^^^^^^^^^^
Controls a production well using observed historical data (instead of :ref:`WCONPROD`). Used for history matching analysis (see :ref:`history_data_sec` for the native PFLOTRAN-OGS format). 
Historical data such as rates and BHP can be introduced, although only one can be used to control the well. A producer well using history matching is treated differently to a normal production well in the following way:

    - Observed production rates and BHP are written to the summary file. (The mnemonic has a *h* added at the end).
    - Wells can only have one control mode (other observed data is just reported). 
    - For rate controls the BHP limit is set to a low level automatically to avoid it switching to BHP control mode.

Example below: ::

    WCONHIST
    prod1  OPEN BHP  38 49 5550  3* 275 /
    /

Up to 12 arguments are expected for each well. Each record corresponds to a well: ::

  WCONHIST
  arg1 arg2 arg3 arg4 arg5 arg6 arg7 arg8 arg9 arg10 arg11 arg12 /
  /

Some of these arguments are not supported and are ignored by the simulator, highlighted in :red:`red` below:

  arg1 arg2 arg3 arg4 arg5 arg6 :red:`arg7` :red:`arg8` :red:`arg9` arg10 :red:`arg11` :red:`arg12` /


The arguments are as follows:

.. list-table::

  * - **Argument**
    - **Meaning**
  * - arg1 
    - Well name.
  * - arg2 
    - Status of the well. OPEN and SHUT supported, everything else is ignored (:red:`STOP and AUTO not supported`).
  * - arg3 
    - Control mode. The following options are supported: 

        * ORAT, WRAT, GRAT, LRAT, BHP. 

  * - arg4 
    - Observed oil rate. The units are:

	* sm3 /day, if |linkMETRIC| units are specified for the .esched include file
	* stb/day, if |linkFIELD| units are specified for the .esched include file
  * - arg5 
    - Observed water rate. The units are:

	* sm3 /day, if |linkMETRIC| units are specified for the .esched include file
	* stb/day, if |linkFIELD| units are specified for the .esched include file
  * - arg6 
    - Observed gas rate. The units are:

	* sm3/day, if |linkMETRIC| units are specified for the .esched include file
	* Mscf/day, if |linkFIELD| units are specified for the .esched include file
  * - :red:`arg7-9`  
    - :red:`Not supported, values ignored.`

  * - arg10 
    - Observed BHP value. The units are:

	* barsa, if |linkMETRIC| units are specified for the .esched include file
	* psi, if |linkFIELD| units are specified for the .esched include file
  * - :red:`arg11-12`
    - :red:`Not supported, values ignored.`



.. |linkMETRIC| replace:: :ref:`METRIC<esched_metric_sec>`
.. |linkFIELD| replace:: :ref:`FIELD<esched_field_sec>`
