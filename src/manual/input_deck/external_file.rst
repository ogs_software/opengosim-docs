
:index:`EXTERNAL_FILE`
----------------------------------------------------------
Any part of the input dataset can be placed into an external (i.e. include) file, using the keyword ``EXTERNAL_FILE``. 
External files can be nested up to 9 files deep (8 files beyond the main input file). In the following example a file of well data is read. ::

  external_file mwbo_wells.inc

An illustration of how a water saturation property table may be included in this way may be found in :ref:`this example<black_oil_sat_prop_by_table_example>`.

``EXTERNAL_FILE`` can be used from within a :ref:`GRDECL<GRDECL-grids>` file to include datasets of pre-processed grid data.
