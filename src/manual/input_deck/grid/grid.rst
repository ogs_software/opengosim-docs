
:index:`GRID`
-------------

The grid of the computational model is defined by the GRID block, which specifies the type of discretisation to be used. Grid may define a regular grid directly, or select an external file where the grid data is stored.

Various types of meshes can be defined using the TYPE keyword. A common grid type for reservoir simulation is GRDECL, in which the grid is defined using a number of Eclipse keywords in a named file. The GRDECL format is recommended for reservoir simulations in |pftogs|, as it is the only one that supports modeling of wells using the keyword WELL_DATA.

An example of use of the GRDECL grid format is given below: ::

  GRID
    TYPE grdecl spe10.grdecl
  END



More details on the eclipse-style grids are given in:

.. toctree::
   :maxdepth: 1

   grdecl



Other grid formats are available for advanced |pftogs| users:

* :ref:`Structured and Structured Cylindrical<cylin_grid_sec>`
* :ref:`Unstructured Explicit<unstruc_exp_grid_sec>`





