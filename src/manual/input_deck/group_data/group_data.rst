
.. _group_data_sec:

:index:`GROUP_DATA`
==========================

Defines a group of :ref:`wells<well_data_sec>`.

Example: ::

  GROUP_DATA XXX
    WELL proda prodb
    TARG_RVP  15 m^3/day
    TIME 15 d
    TARG_RVP  18 m^3/day
  END

The options supported by GROUP_DATA are:

.. contents::
  :backlinks: top
  :depth: 2
  :local:


Group Rate Options
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The following is a list of keywords to define rates in a ``GROUP_DATA`` block:

+-----------+-----------------------------------------------------------------+
| Keyword   | Meaning                                                         |
+===========+=================================================================+
| TARG_OSVP | Group oil surface volume production rate                        |
+-----------+-----------------------------------------------------------------+
| TARG_GSVP | Group gas surface volume production rate                        |
+-----------+-----------------------------------------------------------------+
| TARG_WSVP | Group water surface volume production rate                      |
+-----------+-----------------------------------------------------------------+
| TARG_SSVP | Group solvent surface volume production rate                    |
+-----------+-----------------------------------------------------------------+
| TARG_LSVP | Group liquid (oil+water) surface volume production rate         |
+-----------+-----------------------------------------------------------------+
| TARG_GSVI | Group gas surface volume injection rate                         |
+-----------+-----------------------------------------------------------------+
| TARG_WSVI | Group water surface volume injection rate                       |
+-----------+-----------------------------------------------------------------+
| TARG_OSVI | Group oil surface volume injection rate                         |
+-----------+-----------------------------------------------------------------+
| TARG_SSVI | Group solvent surface volume injection rate                     |
+-----------+-----------------------------------------------------------------+
| TARG_OMP  | Group oil mass production rate                                  |
+-----------+-----------------------------------------------------------------+
| TARG_GMP  | Group gas mass production rate                                  |
+-----------+-----------------------------------------------------------------+
| TARG_WMP  | Group water mass production rate                                |
+-----------+-----------------------------------------------------------------+
| TARG_SMP  | Group solvent mass production rate                              |
+-----------+-----------------------------------------------------------------+
| TARG_GMI  | Group gas mass injection rate                                   |
+-----------+-----------------------------------------------------------------+
| TARG_WMI  | Group water mass injection rate                                 |
+-----------+-----------------------------------------------------------------+
| TARG_OMI  | Group oil mass injection rate                                   |
+-----------+-----------------------------------------------------------------+
| TARG_RVP  | Group reservoir volume production rate                          |
+-----------+-----------------------------------------------------------------+
| TARG_RVGI | Group gas reservoir volume injection rate                       |
+-----------+-----------------------------------------------------------------+
| TARG_RVWI | Group water reservoir volume injection rate                     |
+-----------+-----------------------------------------------------------------+
| TARG_RVOI | Group oil reservoir volume injection rate                       |
+-----------+-----------------------------------------------------------------+
| TARG_RVSI | Group solvent reservoir volume injection rate                   |
+-----------+-----------------------------------------------------------------+                       

Note that group production targets are shared between all producers in the group.
Group injection  targets are shared between all injectors of that type in the group (so water target shared between water injctors).



WELL
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

List of well names in group.

May be more than one line of ``WELL`` data, see also :ref:`the WELL_DATA keyword <well_data_sec>`.





TIME
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Time at which next command acts, in a ``GROUP_DATA`` block.

 
DATE
^^^^^^^^^^^^^^^^^^^^^^^^^

Date at which next command acts (with respect to run ``START_DATE``).

Example: ::

  GROUP_DATA GPRO
    WELL AA-1 AA-2 AA-3 AA-4 AA-5 AA-6 AA-7 AA-8 AA-9 AA-10
    TARG_RVP  10000 m^3/day
    DATE 1 JAN 2031
    TARG_RVP 11000  m^3/day
  END

