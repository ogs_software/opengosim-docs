
.. _history_data_sec:

:index:`HISTORY_DATA`
-----------------------

HISTORY_DATA is used to do history matching analysis. History matching is the process of adjusting a reservoir model to closely match the behaviour of the reservoir with the observed historical data. Pressure and  production need to be reproduced, as well as possible, by the reservoir model.
 
During the sensitivity analysis, it is important to be able to easily compare historical data against model predictions. For example, the user may control the injection rate of a well and check if its pressure profile matches the historical data.

The **HISTORY_DATA** block is used to set up historical values of production or injection wells in the model. It is the native set of keywords to do **history matching** in PFLOTRAN-OGS (Note that it can also be done through :ref:`ESCHED<esched_sec>` -WCONINJH and WCONHIST- , although only one of the two history matching approaches can be used per simulation).
 
In HISTORY_DATA, the data is introduced using tables with two headers, a mnemonic and the corresponding unit. In this way, a prior ordering of the table is not required by the user and the introduced data is automatically converted to internal units. HISTORY_DATA can be used to impose a well target (one target only). In the output, new fields ending with “H” will be added accounting for the historical values (for example, from WBHP we would now have also WBHPH). All the input data in the tables is output in the summary file so the user can cross-compare with the results obtained by the current simulation and the historical data introduced. Below there is an example of a HISTORY_DATA block and setting up a historical target for a well: ::

    WELL_DATA well1
      WELL_TYPE PRODUCER
      CIJK 10 10 1 1 
      USE_HIST_TARG OSV
    END

This could reference a well history table of the form: ::

    HISTORY_DATA 
      WELL_NAME well1
      STEP_MATCH
        DATE		BHP     WOPR  	WWPR	WGPT   
        dd/mmm/yyyy	bar     m^3/d	bbl/d	Mcf    
        01/Jan/2000	68.9476 3179.7 	100     0      
        01/Feb/2000	100     5000.5 	120     3100 
        01/Mar/2000	100     5000.5  120     6290   
      END
    END

or equally using TIME: ::

    HISTORY_DATA 
      WELL_NAME well1
      STEP_MATCH
        TIME		BHP     WOPR  	WWPR	WGPT   
        day       	bar     m^3/d	bbl/d	Mcf    
        0       	68.9476 3179.7 	100     0      
        31      	100     5000.5 	120     3100
        60      	100     5000.5 	120     6290 
      END
    END

The input data describes the following:

* The well *well1* is of type producer, with a target defined by the historical data of the oil column. 

* :ref:`step_match`: This keyword, ensures that the simulator has to honour the time-periods defined in the table.

* **DATE**: Times in the format of days/month/year (using abbreviation of the month), when historical data change value.
* **TIME**: Times in days, from the start of the simulation time, when historical data change value.

Regarding the table data, WOPR is the well historical oil production rate imposed as a target with USE_HIST_TARG OSV, while WWPR, WGPT and BHP are the water rate, total gas production and the bottom hole pressure historical data to match.
Also, since oil and water rates are provided, the *Liquid* rate will be computed internally and can be used as a target. Finally, note that different unit systems have been used for the different rates/totals.


The options and characteristics supported by HISTORY_DATA are listed below:

.. contents::
  :backlinks: top
  :depth: 2
  :local:

.. _well_name:

WELL_NAME
~~~~~~~~~~~~~~~~~~~~~~~~~~~

This keyword has to be included before any tabulated data and must be followed by the name of the well the historical data refers to. 
Example: ::

    WELL_NAME prod1

.. _step_match:

:index:`STEP_MATCH`
~~~~~~~~~~~~~~~~~~~~~~~~~~

By default the simulator time steps will not necessarily match the times defined in the history data. If the user wants an exact match with the times provided in the table,  the keyword STEP_MATCH should be used.


.. _mnemonics_header_sec:

Mnemonics header
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Each mnemonic defines the data that its column is going to have. 
Accepted mnemonics and structure is as follows:

 * TIME or DATE: times can be described either using time from the start of the simulation using the keyword TIME or using a date with the keyword DATE.
 
    * TIME (time units): times can be introduced as elapsed time from the beginning of the simulation. 
    * DATE: The format of the date has to be consistent with units defined in the header.
 
         * dd/mm/yyyy: days, months and years are introduced using numerical values.
         * dd/mmm/yyyy: days and years are introduced using numerical values, while the month is introduced using an abbreviation of the name of the month, e.g. JAN, FEB, MAR, ...
         
    The separators need to be consistent but can be anything that is one single character width (excluding spaces). For example “/”, “.” or “-”. A row is active until the next DATE or TIME is reached by the simulator time.
    
 * BHP (pressure units): history data for the bottom hole pressure.
 
 
 * Rates ([volume or mass]/time) and Total (volume or mass): production and injection history data can be introduced using the following mnemonics: 
 
+-------+-----------------------------------------------------+-------+------------------------------------------------------+
| WOPR  | Oil production volume rate at surface conditions    | WOPT  | Oil production volume total at surface conditions    |       
+-------+-----------------------------------------------------+-------+------------------------------------------------------+
| WGPR  | Gas production volume rate at surface conditions    | WGPT  | Gas production volume total at surface conditions    |       
+-------+-----------------------------------------------------+-------+------------------------------------------------------+
| WWPR  | Water production volume rate at surface conditions  | WWPT  | Water production volume total at surface conditions  |       
+-------+-----------------------------------------------------+-------+------------------------------------------------------+
| WLPR  | Liquid production volume rate at surface conditions | WLPT  | Liquid production volume total at surface conditions |       
+-------+-----------------------------------------------------+-------+------------------------------------------------------+
| WSPR  | Solvent production volume rate at surface conditions| WSPT  | Solvent production volume total at surface conditions|       
+-------+-----------------------------------------------------+-------+------------------------------------------------------+
| WOPRM | Oil production mass rate                            | WOPTM | Oil production mass total                            |       
+-------+-----------------------------------------------------+-------+------------------------------------------------------+
| WWPRM | Water production mass rate                          | WWPTM |Water production mass total                           |       
+-------+-----------------------------------------------------+-------+------------------------------------------------------+
| WGPRM | Gas production mass rate                            | WGPTM |Gas production mass total                             |       
+-------+-----------------------------------------------------+-------+------------------------------------------------------+
| WSPRM | Solvent production mass rate                        | WSPTM | lvent production mass total                          |       
+-------+-----------------------------------------------------+-------+------------------------------------------------------+
| WWIR  | Water injection volume rate at surface conditions   | WWIT  |Water injection volume total at surface conditions    |       
+-------+-----------------------------------------------------+-------+------------------------------------------------------+
| WGIR  | Gas injection  volume rate at surface conditions    | WGIT  | Gas injection  volume total at surface conditions    |       
+-------+-----------------------------------------------------+-------+------------------------------------------------------+
| WSIR  | Solvent injection volume rate at surface conditions | WSIT  | Solvent injection volume total at surface conditions |       
+-------+-----------------------------------------------------+-------+------------------------------------------------------+
| WWIRM | Water injection mass rate                           | WWITM | Water injection mass total                           |       
+-------+-----------------------------------------------------+-------+------------------------------------------------------+
| WGIRM | Gas injection mass rate                             | WGITM | Gas injection mass total                             |       
+-------+-----------------------------------------------------+-------+------------------------------------------------------+
| WSIRM | Solvent injection mass rate                         | WSITM | Solvent injection mass total                         |       
+-------+-----------------------------------------------------+-------+------------------------------------------------------+
  
**Rates and totals differences**

For a given entry, either a total or a rate can be introduced. If both are introduced, although consistent, the simulator will throw an error.
The main differences between using rates and totals are: 

    - Rates are active from the start of the time/date provided.
    - If rates of oil and water are provided the liquid rate is calculated internally and can be used as a target.
    - Totals are considered until the time/date provided. 
    
For example, the input below is equivalent to the example provided at the top of this section (final time is 01/Mar/2000 and 2000 is a leap year): ::

    HISTORY_DATA 
      WELL_NAME well1
      STEP_MATCH
        DATE		BHP     WOPR  	WWPR	WGPR   
        dd/mmm/yyyy	bar     m^3/d	bbl/d	Mcf/day  
        01/Jan/2000	68.9476 3179.7 	100     100      
        01/Feb/2000	100     5000.5 	120     110  
      END
    END
    
In this case we have provided historical gas production using WGPR instead of totals, and therefore, we do not need to provide the final total.

Input units
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Both metric and field unit systems can be used to introduce data into HISTORY_DATA (See :ref:`unit-conventions` for accepted units). The units need to be defined per column, so defining in the same table a rate of gas using m^3/d and a rate of water using bbl/y is accepted.

.. _historic_targetsHD:

:ref:`USE_HIST_TARG<use_hist_targ>`
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Historical data can be used to specify the target for a well. When using historical data as well target, only one target can be specified for the well. The target is selected in the section :ref:`well_data_sec` using the keyword USE_HIST_TARG. Below an example of using a historical BHP as target for an injector well: ::

    WELL_DATA well1
      WELL_TYPE INJECTOR
      CIJK 1 1 1 1 
      USE_HIST_TARG BHP
    END

