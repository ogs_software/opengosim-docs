
:index:`INCLUDE`
----------------------

Allows the inclusion of part of the input deck from an external file. Eclipse-style multi-line format is required. For example: ::

  include 
    water_data.inc /

although the terminating ``/`` is not necessary. The file ``water_data.inc`` contains the data for the :ref:`water equation of state<eos_water_sec>`: ::

  EOS WATER                                                                       
    SURFACE_DENSITY 996.95710                                                     
    DENSITY CONSTANT 996.95710                                                    
    VISCOSITY CONSTANT 0.31d-3 ! 1 cp                                             
  END  

