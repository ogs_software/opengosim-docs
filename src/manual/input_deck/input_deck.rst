.. _input_dec_sec:

|pftogs| Input Deck
===================

A |pftogs| input data is organized into blocks, for example the computational grid, the properties of water, etc.
The contents of a block can be entered over a single or multiple lines.
Each block starts with a keyword, and in case of a multiple-line blocks, is terminated by the keyword ``END`` or ``/``.
Although |pftogs| input is generally case-independent in this case an upper-case END is required.
An input block can include one or more sub-blocks.

The following example is a block which defines the simulation time and time stepping::

  TIME
    FINAL_TIME 1000 d
    INITIAL_TIMESTEP_SIZE 0.01 d
    MINIMUM_TIMESTEP_SIZE 1.0D-10 d
    MAXIMUM_TIMESTEP_SIZE 50.d0 d
  END

The exclamation mark ``!``, and the hash sign ``#`` introduce a comment:
when one of them is encountered at the beginning or somewhere else in a line, anything that comes after it will be ignored.

It is a good practice to use indentation when including sub-blocks, as in the TIME example above.
Although this makes the input dataset more readable, it is not compulsory.

There is also a :ref:`keyword index <genindex>` available for direct reference.

The following pages concern the input deck:

.. toctree::
   :maxdepth: 1
   :caption: General

   units_conventions
   thermodynamic_props/thermodynamic_properties



.. toctree::
   :maxdepth: 1
   :caption: Input Cards
   :glob:

   aquifer_data/aquifer_data
   brine
   checkpoint
   diffusion_coefficient
   external_file
   erestart
   esched
   equilibration
   skip_noskip
   include
   simulation
   restart
   grid/grid
   material_property
   charcurves/characteristic_curves
   misc
   pmisc
   eos_xyz
   separator_data
   time
   output
   well_data
   history_data
   group_data/group_data
   tbc_data/tbc_data
