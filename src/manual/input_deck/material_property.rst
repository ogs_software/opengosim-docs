
.. _material_prop_sec:

:index:`MATERIAL_PROPERTY`
--------------------------------

This section specifies material properties to be associated with the entire reservoir domain or with a region of it. See example below: ::

  MATERIAL_PROPERTY formation
    ID 1
    CHARACTERISTIC_CURVES    ch1
    ROCK_COMPRESSIBILITY     4.35d-5  1/Bar
    ROCK_REFERENCE_PRESSURE  1.0      Bar    
    ROCK_DENSITY             2.350d3  kg/m^3
    SPECIFIC_HEAT            1.0d3    J/kg-C
    THERMAL_CONDUCTIVITY_DRY 1.6      W/m-C
    THERMAL_CONDUCTIVITY_WET 4.3      W/m-C
  /

The options supported within the MATERIAL_PROPERTY blocks are:

.. contents::
  :local:



ID
^^^^^^^^^^^^^^^
Goes under the :ref:`MATERIAL_PROPERTY<material_prop_sec>` card. 

Takes only one integer parameter, which defines the material id by which the material property object may be identified/linked. Most commonly, only one material is defined and associated with the entire reservoir domain, in this case this id must be assign to 1.

For PFLOTRAN advanced users, material ids can be assigned on a cell-by-cell basis to define material regions. Note that ID= 0 is reserved for inactive grid cells and cannot be used in this block.  This material id must be entered or the code returns an error.

CHARACTERISTIC_CURVES
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Goes under the :ref:`MATERIAL_PROPERTY<material_prop_sec>` card. 

Takes only one argument, which is the name of the :ref:`characteristic curves<char_curves_sec>` block to be associated with material. This defines a the set of saturation curves, and must be defined in the same input deck, directly or via an external file. Note that if SATNUM is found in GRDECL, CHARACTERISTIC_CURVES is not required. In this case, the characteristic curves are assigned on a cell-by-cell basis, where the SATNUM cell indexes refers to the order of the characteristic curves, as entered in the input deck.



.. _rock_compr_sec:

:index:`ROCK_COMPRESSIBILITY`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Goes under the :ref:`MATERIAL_PROPERTY<material_prop_sec>` card. 

Defines the compressibility coefficient of pore space, see compressibility models :ref:`in the theory guide<soil_comp_model>`. As is conventional in reservoir simulation, this coefficient reflects the expansivity of the pore space, rather than the compressibility of the rock grains. The user must specify units as one over unit pressure (inverse pressure), e.g. 1/Bar, 1/psi, 1/Pa, etc. For all pressure units supported see :ref:`Units and Conversion<unit-conventions>`. 

If not set, the rock compressibility defaults to zero. See example below: ::

  ROCK_COMPRESSIBILITY 4.35d-5 1/Bar

.. _rock_refp_sec:

:index:`ROCK_REFERENCE_PRESSURE`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Goes under the :ref:`MATERIAL_PROPERTY<material_prop_sec>` card. 

Defines the reference pressure for the computation of rock compressibility effects, which can be assigned a constant value. Units must be specified (see :ref:`Units and Conversion<unit-conventions>` for pressure unit options). An example is given below: ::

  ROCK_REFERENCE_PRESSURE 1.0 Bar

The rock reference pressure will be computed internally as the maximum pressure resulting from the initialisation when selecting the INITIAL_PRESSURE option, e.g., ::

  ROCK_REFERENCE_PRESSURE  INITIAL_PRESSURE


:index:`ROCK_COMPRESSIBILITY_FUNCTION`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Goes under the :ref:`MATERIAL_PROPERTY<material_prop_sec>` card. 

This is an optional card. Takes one argument that specifies the type of pore space compressibility function for which the options available are [QUADRATIC, LEIJNSE, BRAGFLO]. It defaults to the QUADRATIC model. See all compressibility models available :ref:`in the theory guide<soil_comp_model>`. An example is given below: ::

  ROCK_COMPRESSIBILITY_FUNCTION   LEIJNSE

  
:index:`ROCK_DENSITY`
^^^^^^^^^^^^^^^^^^^^^^^^^^
Goes under the :ref:`MATERIAL_PROPERTY<material_prop_sec>` card. 

The ROCK_DENSITY keyword is required only for non-isothermal problems. It takes one argument which defines the rock density. Units can be specified after the value, as those of mass per unit volume (see :ref:`Units and Conversion<unit-conventions>` for all units options); if not entered the software assumes [kg/m3]. See example below: ::

  ROCK_DENSITY 2350.0 kg/m^3

:index:`SPECIFIC_HEAT`
^^^^^^^^^^^^^^^^^^^^^^
Goes under the :ref:`MATERIAL_PROPERTY<material_prop_sec>` card. 


The SPECIFIC_HEAT keyword is required only for non-isothermal problems. It takes one argument which defines the specific heat capacity of the rock. Units can be specified after the value, as those of energy per unit mass per unit temperature (see :ref:`Units and Conversion<unit-conventions>` for all units options), if not entered the software assumes [J/(Kg-K)]. See example below: ::

  SPECIFIC_HEAT 1000.0 J/kg-C


:index:`THERMAL_CONDUCTIVITY_DRY`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Goes under the :ref:`MATERIAL_PROPERTY<material_prop_sec>` card. 


The THERMAL_CONDUCTIVITY_DRY (:math:`\lambda_{\mbox{dry}}`)  keyword is required only for non-isothermal problems. It takes one argument which is the dry thermal conductivity of the rock. This is the thermal conductivity of the rock considered fully saturated with gas. Units can be specified after the value, as those of power per unit length per unit temperature (see :ref:`Units and Conversion<unit-conventions>` for all units options), if not entered the software assumes [W/(K-m)]. The thermal conductivity of the rock partially saturated with liquid and gas is computed as follows:


.. math::
  \lambda = \lambda_{\mbox{dry}} + S_{\mbox{liq}}^{1/2}\left( \lambda_{\mbox{wet}}-  \lambda_{\mbox{dry}} \right)

where the liquid saturation is equal to the sum of the oil and water saturation (:math:`S_{\mbox{liq}} =S_w + S_o` ).


:index:`THERMAL_CONDUCTIVITY_WET`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Goes under the :ref:`MATERIAL_PROPERTY<material_prop_sec>` card. 


The THERMAL_CONDUCTIVITY_WET (:math:`\lambda_{\mbox{wet}}`) keyword is required only for non-isothermal problems. It takes one argument which is the wet thermal conductivity of the rock. This is the thermal conductivity of the rock considered fully saturated with liquid. Note that liquid implies water and oil together when both phases are present. Units can be specified after the value, as those of power per unit length per unit temperature (see :ref:`Units and Conversion<unit-conventions>` for all units options), if not entered the software assumes [W/(K-m)]. The thermal conductivity of the rock partially saturated with liquid and gas is computed as follows:

.. math::
  \lambda = \lambda_{\mbox{dry}} + S_{\mbox{liq}}^{1/2}\left( \lambda_{\mbox{wet}}-  \lambda_{\mbox{dry}} \right)

where the liquid saturation is equal to the sum of the oil and water saturation (:math:`S_{\mbox{liq}} =S_w + S_o` ).


:index:`SOIL_COMPRESSIBIITY`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Goes under the :ref:`MATERIAL_PROPERTY<material_prop_sec>` card. See :ref:`ROCK_COMPRESSIBILITY<rock_compr_sec>`, which is a more practical keyworkd to specify the same quantity.

Defines the compressibility coefficient of pore space to be entered in [1/Pa]; see components models in the :ref:`in the theory guide<soil_comp_model>`. As is conventional in reservoir simulation, this coefficient reflects the expansivity of the pore space, rather than the compressibility of the rock grains. 

Note the rock compressibility coefficients are specified in 1/Pa, not the 1/Bar units which may be assumed in some other simulator metric unit systems.

If not set, the rock compressibility defaults to zero.

:index:`SOIL_REFERENCE_PRESSURE`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Goes under the :ref:`MATERIAL_PROPERTY<material_prop_sec>` card. See :ref:`ROCK_REFERENCE_PRESSURE<rock_refp_sec>`, which is a more practical keyword to specify the same quantity

Defines the reference pressure for the computation of rock compressibility effects, which can be assigned a constant value to be entered in [Pa]. An example is given below: ::

  SOIL_REFERENCE_PRESSURE 1.0

The rock reference pressure will be computed internally as the maximum pressure resulting from the initialisation when selecting the INITIAL_PRESSURE option, e.g., ::

  SOIL_REFERENCE_PRESSURE  INITIAL_PRESSURE


TORTUOSITY
^^^^^^^^^^^^^^^^^^^^^^^^
Goes under the :ref:`MATERIAL_PROPERTY<material_prop_sec>` card. 

Takes only one argument, the tortuosity coefficient that corrects the molecular diffusion of the components in the fluid phases. Currently molecular diffusion of the components is accounted only in the :ref:`GAS_WATER<formulation-of-gw>` mode. If not entered the tortuosity defaults to 1, i.e. no correction of the molecular diffusion coefficients.


Advanced Options
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

For advanced PFLOTRAN users who want to use a mesh format other than GRDECL, the porosity and permeability must also be included, see :ref:`MATERIAL_PROPERTY with permeability and porosity<mat_prop_perm_poro_sec>`.

