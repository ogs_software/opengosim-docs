
.. _MISC-sec:

:index:`MISC`
---------------------------------------------------

The MISC table defines the solvent-saturation-dependent miscibility function :math:`f_m^s(f_s)` used to interpolate between the immiscible and the miscible sets of relative permeability functions in the :ref:`Solvent Todd-Longstaff model<formulation-of-solvent_todd_longstaff>`, see also related :ref:`theory page<tl4p-interpolation>`. Below is an example of the table: ::

  MISC
    0.0  0.0
    0.2  0.0
    0.5  1.0
  /

The first column is the solvent saturation fraction  in the vapour phase:

.. math::
  f_s = \frac{S_s}{S_v}  =  \frac{S_s}{S_s+S_q}


:math:`f_s` must grow monotonically moving down the table, and its values must be between 0 and 1. The second column reports the corresponding values of :math:`f_m^s`, which must have values between 0 (immiscible) and 1 (miscible). The table must have at least two entries.

If the MISC table is not entered, :math:`f_m^s` defaults to a linear function with null value for zero solvent saturation, and unity when the vapour is fully saturated with solvent (:math:`f_s=1`).
