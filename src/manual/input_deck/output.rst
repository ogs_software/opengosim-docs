
.. _output_sec:

:index:`OUTPUT`
----------------

This input block selects the type of output, which variables to output, how frequently, and in which format. See the following example: ::

  OUTPUT
    MASS_BALANCE_FILE
      PERIODIC TIMESTEP 1
    END
    ECLIPSE_FILE
      PERIOD_RST TIME 200 d
    END
    LINEREPT
    SOLUTION_MONITOR
  END

The instructions above require to output an ASCII mass file report (rates, total, fluid in place, etc.) at every time step, and to save the 3D solution every 200 days in a unified Eclipse file format. This is a standard Reservoir Engineering format which is accepted by many post-processors such as `ResInSight <http://www.resinsight.org/>`_.

For more information about the mass and Eclipse output files, consult the following pages:

  * :ref:`MASS_BALANCE_FILE<mass_file_sec>`
  * :ref:`ECLIPSE_FILE<eclipse_files_sec>`

Other instructions that may be specified in the OUTPUT block are:
    • LINEREPT. When present prints out a standard reservoir Engineering console log.
    • TIME_UNITS. It takes one argument that specifies the time units printed in the output file (.out) and mass file. Example: “TIME_UNITS d”
    • SOLUTION_MONITOR. When present prints out detailed information about the slowest converging cells to the .out file. Advanced users/debugging only. See the corresponding :ref:`page<solution_monitor_sec>` for more information if necessary.

For advanced PFLOTRAN users, other output formats are also possible:
    • :ref:`SNAPSHOT_FILE<snapshot_sec>`      (for ParaView)
    • :ref:`OBSERVATION_FILE<variable_output_sec>` (ASCII fromat)


