
.. _PMISC-sec:

:index:`PMISC`
---------------------------------------------------

The PMISC table defines the pressure-dependent miscibility function :math:`f_m^p(P)` used to interpolate between the immiscible and the miscible sets of relative permeability functions, in the :ref:`Solvent Todd-Longstaff model<formulation-of-solvent_todd_longstaff>`, see also related :ref:`theory page<tl4p-interpolation>`.



Below is an example of the table: ::

  PMISC Bar
    50.0  0.0
   100.0  0.3
   200.0  1.0
  /


The first column is the pressure :math:`P`, which must growth monotonically moving down the table. The second column is the function :math:`f_m^s`, whose values must be between 0 (immiscible) and 1 (miscible). The table must have at least two entries.

The table is introduced by the keyword ``PMISC``, followed by the units of the pressure values in the first column. The pressure can be entered in Bar, psi, Pa, KPa and MPa (see :ref:`units<unit-conventions>` for more details), and cannot be defaulted.

The PMISC table is used to model how the pressure affects the miscibility of the solvent with the reservoir hydrocarbon. If PMISC is not entered the pressure-dependent miscibility function is defaulted to 1 (:math:`f_m^p=1`), i.e. full miscibility is assumed.
