
.. _restart_sec:

:index:`RESTART`
-----------------------------------
RESTART is a sub-item, and must be defined within :ref:`SIMULATION<simulation_card_sec>`.

RESTART is used to restart a simulation from a given state, at a given time. The restart file
to load must have been saved by the :ref:`CHECKPOINT<checkpoint_sec>` block, and it is specified by the RESTART keyword, see examples below. ::

  restart test-23.0000y.h5

If required, a reset time, i.e. a new initial time and its units can be specified after the name of the restart file. See example below: ::

  restart test-23.0000y.h5 10 y


In the case above, the simulation state loaded from the restart file is associated to a physical time of 10 years.


Note that if the final simulation physical time specified in the :ref:`TIME<time_sec>` block of the restart run,
is equal or less than the physical time for which the restart file was saved, the code will
return an error.
