
.. _erestart_lgr_sec:

Restart runs when adding/removing LGRs
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


The user can define or remove local grid refinements (:ref:`LGRs<carfin_sec>`) on a restart run. If a new LGR is defined, then the solution values for the local cells will be obtained by sampling the solution values in the global cells. If a LGR is dropped on a restart, the new global cells will take the average values of the local grid cells that have been replaced. 

There is a restriction on the way in which local grids may be added and removed. You can increase or decrease the number of LGRs, but the common set of LGRs which appear in the base run restart file and in the restarted run must have the same names in the same order – you are not allowed to shuffle them. This means that if you add LGRs in steps and then take them off again in steps, you must do so in the reverse order. However, you can add or take out several at a time.


Material balance warning
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


When a run is restarted, the original initial fluid in place (the amount of each component present in the reservoir) and the production, injection and aquifer influx totals are written in the summary files loaded with the restart solution. This enables the totals to be continued smoothly through the restart, but also enables the material balance error to be reported. The material balance (reported as MATBAL in the .out file) compares the fluid in the reservoir at a given time to the initial fluid plus the net injection. 

However, when a LGR is added or removed, the sampling or averaging of the solution variables will generally cause a slight material balance error to appear - the material is not a simple linear function of the solution variables, and a non-linear function of the average is not the average of the non-linear functions. This effect should not be large - order 1% is typical – and may be regarded as a measure of the solution inaccuracy caused by the required sampling and averaging.
