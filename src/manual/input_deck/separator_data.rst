

:index:`SEPARATOR_DATA`
---------------------------------------------------

This data block, used with :ref:`COMP mode<comp_intro_sec>` defines the separator conditions used to convert the components in the reservoir into surface volumes. This is assumed to be a multi-stage separator chain, the oil output from each stage being the input to the next stage. The total gas volume is the sum of the gas outputs from each stage, converted to an ideal gas volume at standard conditions. 

Within the SEPARATOR_DATA block each FIELDSEP keyword taking the form: ::

  FIELDSEP [stage] [pressure] [pressure_units] [temperature] [temperature_units]

For example: ::

  SEPARATOR_DATA
   FIELDSEP 1 815  psi 26.67 C
   FIELDSEP 2 315  psi 26.67 C 
   FIELDSEP 3 14.7 psi 15.56 C 
  END



The only temperature units currently available are Centigrade.
