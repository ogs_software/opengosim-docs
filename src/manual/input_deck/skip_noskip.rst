
:index:`SKIP` and :index:`NOSKIP`
-------------------------------------------------------------------
If a number of lines need to be commented-out (i.e. skipped), the keywords ``SKIP``/``NOSKIP`` can be used:
SKIP starts the block of lines to be skipped, and nothing will be read until a ``NOSKIP`` keyword is found. ::

  TIME
    FINAL_TIME 1000 d
    ! Initial step
    INITIAL_TIMESTEP_SIZE 0.01 d
    SKIP ! Remove the following two lines
    MAXIMUM_TIMESTEP_SIZE 10 d at 20. d
    MAXIMUM_TIMESTEP_SIZE 50 d at 200 d
    NOSKIP
  END
