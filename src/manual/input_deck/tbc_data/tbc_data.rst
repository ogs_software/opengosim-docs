
.. _tbc_data_sec:

:index:`TBC_DATA`
============================

Describes a thermal boundary condition.

Examples: ::

  TBC_DATA tbctop
    TEMPVD m C
       800  29
       1000 35
       1200 41
    END TEMPVD
    CONN_D 1 50 1 50 1 1 Z- 
  END

  TBC_DATA tbctop
    TEMPERATURE 29 C
    CONN_D 1 50 1 50 1 1 Z- 
  END

The options supported by TBC_DATA are:

.. contents::
  :backlinks: top
  :depth: 2
  :local:



TINIT, TEMPERATURE
+++++++++++++++++++++++++++++++++++++++

TINIT or TEMPERATURE define the temperature associated with a TBC_DATA thermal boundary condition. Temperature can be provided by a constant value, or using a table of temperature versus depth: ::

  TEMPVD m C
     800  29
     1000 35
     1200 41
  END TEMPVD

TEMPVD requires to specify the depth unit, meter in the example above, while temperature are assumed to be in degree Celsius.  
A temperature table is a preferred option when the faces of a boundary are located at different depths, and the user want to assign values that follow the geothermal gradient. 


.. _conn_D_tbc_sec:

CONN_D
++++++++++++++++++++++++++++++++

Specifies blocks of connections to attach aquifer to reservoir cell faces. Follows the ECLIPSE grdecl convention (K index is counted downwards from the top layer).

An example is: ::

  CONN_D 1 9 9 9 1 1 Y+

Note that the connection box argument order is: ::

  CONN_D IL IU JL JU KL KU

Where:

    * IL and IU are a range of cell indices in the I-direction
    * JL and JU are a range of cell indices in the J-direction
    * KL and KU are a range of cell indices in the K-direction


Connections will be made to cells with I-indices such that :math:`I \geq IL` and :math:`I \leq IU`, etc.

The cell faces to attach the aquifer are specified by one of the following options:

    * X+ or I+, faces connecting cells with I-indices to cells with (I+1)-indices
    * Y+ or J+, faces connecting cells with J-indices to cells with (J+1)-indices
    * Z+ or K+, faces connecting cells with K-indices to cells with (K+1)-indices

The pairs (X- or I-), (Y- or I-) and (Z- or K-) follow the same logic, and are used to connect to cells with (I-1)-indices, (J-1)-indices and (K-1)-indices.

A number of CONN_D sub-keywords may be entered to build up the full connection. If the same face of the same cell is included more than once, only one connection will be generated.

CONN_Z
+++++++++++++++++++++++

Warning: keyword to be used only by proficient PFLOTRAN users. Unless there is some specific reason, consider using :ref:`CONN_D<conn_D_tbc_sec>` instead.

Specifies blocks of connections to attach aquifer to reservoir cell faces.

CONN_Z use the convention that the K index is counted upwards from the bottom layer.
For example: ::

  CONN_Z 1 9 9 9 1 1 Y+

Note that the connection box argument order is: ::

  CONN_Z IL IU JL JU KL KU


Where:

    * IL and IU are a range of cell indices in the I-direction
    * JL and JU are a range of cell indices in the J-direction
    * KL and KU are a range of cell indices in the K-direction

Connections will be made to cells with I-indices such that :math:`I \geq IL` and :math:`I \leq IU`, etc.

The cell faces to attach the aquifer are specified by one of the following options:

    • X+ or I+, faces connecting cells with I-indices to cells with (I+1)-indices (GRDECL grid format).
    • Y+ or J+, faces connecting cells with J-indices to cells with (J+1)-indices (GRDECL grid format).
    • Z+ or K+, faces connecting cells with K-indices to cells with (K+1)-indices (K index counted upwards from the bottom layer).

The pairs (X- or I-), (Y- or I-) and (Z- or K-) follow the same logic, and are used to connect to cells with (I-1)-indices, (J-1)-indices and (K-1)-indices.

When using CONN_Z, the top faces of a reservoir with 100 layers are defined with Z+. See the following example: ::

  CONN_Z  1 50 1 50  100 100  Z+

