
.. _real_co2_db_sec:

Database for real |CO2|
^^^^^^^^^^^^^^^^^^^^^^^^^

Real pure |CO2| can be characterised by a thermodynamic database to define a Gas or a Solvent by the :ref:`EOS GAS<eos_gas_sec>` or :ref:`EOS SOLVENT<eos_solv_sec>` data block.

The |CO2| database can be included with two different keywords. The first option requires to state explicitly the |CO2| formula weight of 44 g/mol. An example is given below: ::

  EOS GAS
    SURFACE_DENSITY 1.87 kg/m^3
    FORMULA_WEIGHT 44 g/mol
    DATABASE co2_db.dat
  END

The alternative option uses the CO2_DATABASE keyword, and does not require to state the |CO2| formula weight, which is assigned to 44.00098, internally, by the simulator. An example is given below: ::

  EOS GAS
    SURFACE_DENSITY 1.87 kg/m^3
    CO2_DATABASE co2_db.dat
  END

In both cases described above, the external database must have the format of an :ref:`EOS properties database<eos_props_db_sec>`, and contains all the properties needed do define the |CO2| (density, viscosity, enthalpy, fugacity).

A database for pure |CO2| is provided with the software, and it is made available in the database folder found in the installation directory. The data file is called ‘co2_db.dat’ and it is valid in the pressure and temperature ranges reported below:


.. math::
  \begin{split}
  &P &= [0.1 – 2500.1] \mbox{ Bar} \\
  &T &= [ 0 – 375 ] \mbox{ °C}
  \end{split}


The data is sampled using a discretisation step of dP = 5 Bars for the pressure and dT = 2.5 °C for the temperature.

The reference state used for the enthalpy is 

.. math::

  H(P=3.4851\mbox{ MPa}, T=0 °C) = 200 \mbox{ kj/kg},  

where :math:`P=3.4851 \mbox{MPa}` and :math:`T = 0 °C` correspond to a state of saturated liquid at :math:`0 °C`.

The |CO2| database can be generated using the CO2_SPAN_WAGNER_DB data block. See example below: ::


  EOS GAS
    CO2_SPAN_WAGNER_DB
      PRESSURE_MIN 5 Bar
      PRESSURE_MAX 100 Bar
      PRESSURE_DELTA 1 Bar
      TEMPERATURE_MIN 38 C
      TEMPERATURE_MAX 150 C
      TEMPERATURE_DELTA 2 C
      DATABASE_FILE_NAME co2_db.dat
    END
  END

Where:

PRESSURE_MIN and PRESSURE_MAX are the minimum and maximum pressure values defining the database pressure range. For the validity of the model available, the pressure range must fall within the following limits: :math:`0.1 \mbox{ Bar} < P < 2500 \mbox{ Bar}`. The minimum and maximum pressures are optional, if not entered they are defaulted to 0.1 and 2500 Bar respectively.

TEMPERATURE_MIN and TEMPERATURE_MAX are the minimum and maximum temperature values defining the database temperature range. For the validity of the model available, the temperature range must fall within the following limits: :math:`−55°C <T<375°C`. The minimum and maximum temperatures are optional, if not entered they are defaulted to :math:`0` and :math:`375 °C` respectively.

TEMPERATURE_DELTA is the temperature step used to discretise the temperature. It is an optional parameter, if not entered is defaulted to :math:`2.5 °C`.

Temperatures can be entered only in degree Celsius.

DATABASE_FILE_NAME is used to specify the name of file where the |CO2| database will be printed. This is an optional paramter, if not entered will be defaulted to “co2datbase_eos.dat”.

The properties in the |CO2| database are computed using the Span-Wagner model, see: :cite:`span1996new`.

As none of the flow models implemented can handle multi-thermodynamic |CO2| states in the
same simulation, care must be taken so that pressure and temperature conditions do not
span the |CO2| phase transition region, even if the range of validity of the |CO2| database can
cover these cases.

