
.. _eos_comp_sec:

:index:`EOS COMP`
---------------------------------------------------

When in :ref:`COMP mode<comp_intro_sec>`, the ``EOS COMP`` table is available to specify EOS properties for the various components. The equation of state used is always cubic. This replaces ``EOS OIL`` and ``EOS GAS``.

An example of the table is as follows: ::

  EOS COMP
    PREOS
    GCOND
    CNAME      CO2N2   C1     C2     C3     C46    C7P1   C7P2   C7P3
    TCRIT K    215.2   186.6  305.4  369.9  396.2  572.5  630.2  862.6
    PCRIT Bar  53.903  45.6   46.20  42.55  34.35  25.93  16.92  8.61
    ZCRIT      0.283   0.224  0.285  0.281  0.274  0.260  0.260  0.260
    ZMF   Frac 0.0315  0.6599 0.0869 0.0591 0.0967 0.0472 0.0153 0.0034
    ACF        0.1325  0.013  0.098  0.152  0.234  0.332  0.495  0.833
    MW         36.01   16.04  30.07  44.10  67.28  110.9  170.9  282.1
    BIC 0.036
    BIC 0.05   0
    BIC 0.08   0       0
    BIC 0.1002 0.09281 0       0
    BIC 0.1    0       0.00385 0.00385 0
    BIC 0.1    0       0.00630 0.00630 0 0 
    BIC 0.1    0.1392  0.00600 0.00600 0 0 0
  END


The sub-keywords in ``EOS COMP`` are described below:


.. contents::
  :backlinks: top
  :depth: 2
  :local:


PREOS
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Specify to use the Peng-Robinson equation of state :cite:`Peng_Robinson`.


SRKEOS
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Specify to use the Soave-Redlich-Kwong equation of state.

CNAME 
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Followed by a set of component names. If not supplied, internal names like C1, C2 will be generated.


TCRIT 
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Component critical temperatures. Followed by unit (K is only option at present). Then Nch values, where Nch = Nc-1 is the number of non water components.

PCRIT 
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Component critical pressures. Followed by pressure unit (e.g., Bar), then Nch values.

ZCRIT 
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Component critical Z-factors. No unit; followed by Nch values.

ZMF   
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Reservoir non-aqueous phase composition. Followed by unit (Frac or Percentage), then Nch values. The composition can be over-ridden on a cell by cell basis using ZMFVD.

ACF   
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Component acentric factors. No unit; followed by Nch values.

PRCORR
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This keyword activates a modification of the acentric factor polynomial in the Peng-Robinson equation of state :cite:`Peng_Robinson`.
For acentric factor w > 0.49 a cubic expression is used instead of the quadratic default.

MW    
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Component molecular weights. No unit; followed by Nch values.

SHIFT 
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Component dimensionless shift factors for the Peneloux volume shift.

OMEGAA
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Non-default Omega A values for the equation of state.

OMEGAB
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Non-default Omega B values for the equation of state.

BIC     
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Binary interaction coefficients. As series of keywords specifying the binary coefficients as a lower triangular matrix. So the first BIC will specify the binary interaction coefficient between component 2 and component 1; the next BIC will specify the two binary interaction coefficients between component 3 and components 1 and 2, and so on – a total of Nch.(Nch-1)/2 values are required.


GCOND   
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Specifies a gas condensate reservoir (all one phase hydrocarbon states are gas).


PARACHOR  
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Parachors are values which can be used in the McLeod-Sugden expression for surface tensions :cite:`CT9242501167`. Parachors have units of :math:`(dynes/cm)^{1/4}cm^3/mol` or :math:`(mN/m)^{1/4} cm^3/mol)`, as 1 mN/m = 1 dyne/cm. Typical values are of the order of 100. This is a conventional unit for the parachor, and PARACHOR values will always be regarded as being in these units. 

Parachors are entered as part of the equation of state description in COMP EOS mode, and take the form, for example, of: ::

    EOS COMP
      PREOS
      CNAME         C1      C3     C6     C10    C15    C20
      ...
      PARACHOR      71      151    271    400    600    800
    END

PARACHOR values are only required with the MISCIBLE option activated.
