.. _eos_gas_sec:

:index:`EOS GAS`
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Defines the properties of the gas phase. Below an example that model CO2 as an ideal gas: ::

  EOS GAS
    SURFACE_DENSITY 1.9 kg/m^3 
    DENSITY IDEAL
    ENTHALPY IDEAL_CO2
    VISCOSITY CONSTANT 0.03 cP
  END



.. toctree::
   :maxdepth: 1

   PVDG : Dry gas PVT table <gas_pvdg>
   Constant value <gas_const>
   Ideal gas law <gas_ideal>
   External database <gas_db>
   CO2 database <gas_co2_db>


The other two properties to set are:

  * :ref:`SURFACE_DENSITY <gas_surface_density_sec>`
  * :ref:`FORMULA_WEIGHT <gas_formula_weight_sec>`



Enthalpy must be defined only for thermal problems, in which case it cannot be defaulted. If the gas is defined by a database option (|CO2| database or external), the Enthalpy model is not required, as enthalpy must be defined in the database, if not found the software will return an error. In any other case, the user must specify an Enthalpy model.


If no specifications are given, the gas will default to air, with density and energy
computations using the ideal gas law. 


.. _gas_surface_density_sec:

SURFACE_DENSITY specification of gas surface density
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
This keyword specifies the density of the gas phase at surface conditions. The surface
density value is used to convert fluid flows in the reservoir into surface volume flows.
Surface volumes are used to specify required well rates with targets like :ref:`TARG_GSV<well_target_options_sec>`, and surface volume rates and totals are reported in the :ref:`Mass file<mass_file_sec>` and in the :ref:`Eclipse files<eclipse_files_sec>`. The surface density must be entered or the software will return an error. The density units are optional, if not entered are defaulted to :math:`kg/m^3.`

An example is: ::

  SURFACE_DENSITY 0.656 kg/m^3

.. _gas_formula_weight_sec:

FORMULA_WEIGHT specification of gas molecular weight
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The gas molecular weight can be entered by the user, otherwise will be defaulted to air,
28.96 g/mol.

An example in setting the gas molecular weight to that of methane: ::

  FORMULA_WEIGHT 16.04

If not entered the units will default g/mol.

The molecular weight will be used with some gas density calculation options, such as
DENSITY IDEAL. In some cases, such as specification of the reservoir density using a table
of formation volume factor with respect to the specified surface density, the density may
not depend on the FORMULA_WEIGHT value.




