
.. _eos_oil_sec:

:index:`EOS OIL`
~~~~~~~~~~~~~~~~~~~~~ 


Defines the properties of the oil phase, and no default model exists for the oil. If no specifications are given for the computation of density, viscosity and enthalpy the program will return an error.

An example that describes the oil with a constant density and viscosity, and uses a linear model for enthalpy is given below: ::

  EOS OIL
    SURFACE_DENSITY  740.75 kg/m^3
    DENSITY CONSTANT 734.29d0 !kg/m3
    ENTHALPY LINEAR_TEMP 1.d3 !c oil [J/kg/°C]
    VISCOSITY CONSTANT 0.987d-3 !Pa.s
  END

The following modelling options are available for the oil properties:




.. toctree::
   :maxdepth: 1

   PVDO: Dead Oil PVT table <oil_pvdo>
   PVCO: Live Oil PVT table (only option for the Black Oil and Solvent models) <oil_pvco>
   PVCOS table for COMP4 mode <pvcos>
   Constant value oil properties <oil_const>
   Density linear model with respect to pressure and temperature <oil_den_lin> 
   Density inverse linear model <oil_den_inv_lin>
   Viscosity quadratic model with respect to pressure and temperature <oil_visc_quad>
   Enthalpy linear with respect to temperature <oil_enth_lin> 
   Enthalpy quadratic with respect to temperature <oil_enth_quad>
   Oil properties from external database <oil_db>
   All oil properties defined by the same external database <oil_all_db>

The other two properties to set are:

  * :ref:`SURFACE_DENSITY <oil_surface_density_sec>`
  * :ref:`FORMULA_WEIGHT <oil_formula_weight_sec>`
  

.. _oil_surface_density_sec:

SURFACE_DENSITY specification of oil surface density
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This keyword specifies the density of the oil phase at surface conditions. The surface
density value is used to convert fluid flows in the reservoir into surface volume flows.
Surface volumes are used to specify required well rates with targets like :ref:`TARG_OSV<well_target_options_sec>`, and
surface volume rates and totals are reported in the :ref:`Mass file<mass_file_sec>` and in the :ref:`Eclipse files<eclipse_files_sec>`. The surface density must be entered or the software will return an error. The density units are optional, if not entered are defaulted to kg/m^3.

An example is: ::

  SURFACE_DENSITY 740.75 kg/m^3

.. _oil_formula_weight_sec:

FORMULA_WEIGHT specification of oil molecular weight
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The oil molecular weight can be entered by the user, otherwise it will be defaulted to
decane (C\ :sub:`10`\ H\ :sub:`22`\ , 142 g/mol). Units are optional, if not entered it defaults to g/mol.

An example is: ::

  FORMULA_WEIGHT 800.0d0

In the case in which the oil properties are specified using the PVDO or PVCO tables, the oil
fluid properties will be independent of the specified molecular weight.





