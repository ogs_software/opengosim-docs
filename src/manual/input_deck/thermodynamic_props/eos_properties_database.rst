
.. _eos_props_db_sec:

EOS properties database
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


The EOS database can be used to specify the properties of a component, e.g. pure |CO2|, pure hydrogen, reservoir gas, etc.

The database file must contain an header that states the number of nodes used to discretise the pressure (NUM_DP) and temperature (NUM_DT), the block DATA_LIST_ORDER that specifies the properties provided and their column order, and the DATA section where the properties values are reported for each pressure and temperature pair. See example below: ::


  NUM_P    4
  NUM_T    4
  DATA_LIST_ORDER
    PRESSURE  MPa
    TEMPERATURE  C
    DENSITY  kg/m^3
    INTERNAL_ENERGY  MJ/kg
    ENTHALPY  MJ/kg
    VISCOSITY  Pa-s
    FUGACITY  MPa
  END
  DATA
    1.00E-02  0.00E+00  1.93E-01  4.34E-01  4.85E-01  1.37E-05  9.99E-03
    1.00E-02  2.50E+00  1.92E-01  4.35E-01  4.87E-01  1.38E-05  9.99E-03
    1.00E-02  5.00E+00 1.90E-01  4.37E-01  4.90E-01  1.39E-05  9.99E-03
    [...]


In the example above the pressure and temperature ranges are both discretised by four nodes, thus the data section must contain 16 entries, otherwise the code will return an error. 

DATA_LIST_ORDER specifies that the first column is pressure, the second is temperature, the third is density and so on. The first column must be pressure and the second temperature.

The pressure and temperature intervals, dP and dT, must be constant throughout the database. If variable intervals are required, consider to use PVT tables instead. 

The data must be reported for growing pressure and temperature, with the temperature looping faster.
The EOS database can be used to define the properties of one of the following components:

* the gas in the GAS_WATER, COMP3, COMP4 and BLACK_OIL modes.
* the solvent in the COMP3, COMP4, TODD_LONGSTAFF and SOLVENT_TL modes.
* the dead oil in the TOIL_IMS mode.

When used to define the properties of the gas and solvent components, in the GAS_WATER, COMP3 and COMP4 modes, the fugacity must be provided. This is not needed when the database is used to characterise the properties of a component in the TODD_LONGSTAFF,  SOLVENT_TL, BLACK_OIL and TOIL_IMS modes.

