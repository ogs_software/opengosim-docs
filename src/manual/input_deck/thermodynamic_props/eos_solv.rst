
.. _eos_solv_sec:

:index:`EOS SOLVENT`
~~~~~~~~~~~~~~~~~~~~~~~~~~


Defines the solvent properties. Below an example where solvent is CO2 modelled as an ideal gas: ::

  EOS SOLVENT
    DENSITY IDEAL
    VISCOSITY CONSTANT 0.03 cP
    ENTHALPY IDEAL_CO2
  END

The following modelling options are available for the solvent properties:



.. toctree::
   :maxdepth: 1

   PVDS <solv_pvds>
   Ideal gas law <solv_ideal>
   External database <solv_db>
   CO2 database <solv_co2_db>

The other two properties to set are:

  * :ref:`SURFACE_DENSITY <slv_surface_density_sec>`
  * :ref:`FORMULA_WEIGHT <slv_formula_weight_sec>`

Enthalpy must be defined only for thermal problems, in which case it cannot be
defaulted. If the solvent is defined by a database option (|CO2| database or external), the
Enthalpy model is not required, as enthalpy must be defined in the database, if not found
the software will return an error. In any other case, the user must specify an Enthalpy
model.

.. _slv_surface_density_sec:


SURFACE_DENSITY specification of solvent surface density
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This keyword specifies the density of the solvent phase at surface conditions. The surface
density value is used to convert fluid flows in the reservoir into surface volume flows.
Surface volumes are used to specify required well rates with targets like :ref:`TARG_SSV<well_target_options_sec>`, and surface volume rates and totals are reported in the :ref:`Mass file<mass_file_sec>` and in the :ref:`Eclipse files<eclipse_files_sec>`. The surface density must be entered or the software will return an error. The density units are optional, if not entered are defaulted to :math:`kg/m^3`.

An example is: ::

  SURFACE_DENSITY 1.98 kg/m^3 

.. _slv_formula_weight_sec:

FORMULA_WEIGHT specification of solvent molecular weight
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The solvent molecular weight can be entered by the user, otherwise will be defaulted to |CO2|,
44.00098 g/mol.

An example in setting the molecular weight to that of methane: ::

  FORMULA_WEIGHT 16.04


The molecular weight will be used with some solvent density calculation options, such as DENSITY IDEAL. In some cases, such as specification of the reservoir density using a table of formation volume factor with respect to the specified surface density, the density may not depend on the FORMULA_WEIGHT value.
