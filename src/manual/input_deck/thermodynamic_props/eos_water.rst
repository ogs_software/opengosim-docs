
.. _eos_water_sec: 

:index:`EOS WATER`
~~~~~~~~~~~~~~~~~~~~~ 

Defines the water properties such as density, viscosity and enthalpy, and the surface density. For example: ::

  EOS WATER
   SURFACE_DENSITY 1000.0 kg/m^3
  END


The above is the minimum required input, where the surface density is specified, while the other properties are defaulted to use standard water tables, :ref:`IFC67<wat_ifc67_sec>`.

The following modelling options are available for the water properties:





.. toctree::
   :maxdepth: 1

   IFC67 water table <wat_ifc67>
   WATERTAB table <wat_watertab>
   Constant value properties <wat_const>
   Density linear model <wat_den_lin>
   Density quadratic model <wat_den_quad>
   Density exponential model <wat_den_exp>
   Density - Trangenstein <wat_TG>
   Density - Rowe and Chou <wat_RC>
   Density - Ezrokhi <wat_Ezrokhi>
   Viscosity - Grabowsky <wat_TG>
   Viscosity - Kestin <wat_Kestin>
   Batzle and Wang <wat_BW>


Different type of models can be selected for different properties, however mixed selections should be assessed carefully. Properties for which no models have been selected will default to the IFC67 water tables.

Other modelling aspects of the water properties:

    * :ref:`SURFACE_DENSITY<surf_den_eos_water_sec>`
    * :ref:`Density and viscosity dependence on salinity<den_salinity_eos_water_sec>`
    * :ref:`Density dependence on dissolved CO2<den_co2_eos_water_sec>`

.. _surf_den_eos_water_sec:

SURFACE_DENSITY specification of water surface density
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
This keyword specifies the density of the aqueous phase at surface conditions. This may
include any dissolved salts and the effect of the surface temperature. The surface density
value is used to convert fluid flows in the reservoir into surface volume flows. Surface
volumes are used to specify required well rates with targets like :ref:`TARG_WSV<well_target_options_sec>`, and surface volume rates and totals are reported in the :ref:`Mass file<mass_file_sec>` and in the :ref:`Eclipse files<eclipse_files_sec>`. The surface
density must be entered or the software will return an error. The density units are optional,
if not entered are defaulted to kg/m^3.

An example is: ::

  SURFACE_DENSITY 1004.2 kg/m^3


.. _den_salinity_eos_water_sec:

Density and viscosity dependence on salinity
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

For the GAS_WATER, COMP, COMP3 and COMP4 modes, the simulator might apply a correction to water density and viscosity using the salinity entered by the user, depending on how the water properties have been specified.

If the water density is defined by correlations for pure water, such as IFC67 water tables or Trangenstein, by default the aqueous phase density and viscosity will depend on the salinity value specified in BRINE or SALTVD. In this case, by default, the :ref:`Batzle and Wang<wat_BW_sec>` correlations are used to model the brine density and viscosity accounting for salinity. However, the :ref:`Kestin <wat_Kestin_sec>` model is also available for the viscosity and :ref:`Rowe and Chow<wat_Rowe_and_Chou_sec>` and :ref:`Ezrokhi<wat_Ezrokhi_sec>` can also be used for the brine density. 

If the user specifies directly the brine density and viscosity, by either entering WATERTAB, or by setting constant values, or by defining user-defined correlations (linear, quadratic or exponential), the values given are honoured and the salinity values found in BRINE or SALTVD are ignored. If the brine density is specified but not the viscosity, the viscosity is computed as the one of pure water using the IFC67 tables.

.. _den_co2_eos_water_sec:

Density dependence on dissolved CO2
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

For the GAS_WATER, COMP3 and COMP4 modes, the simulator will apply by default a correction of the brine density, to account for the presence of dissolved CO2, by using Duan et al. :cite:`duan2008densities`. This model accounts for the salinity entered by BRINE or SALTVD.
Currently CO2 is the only component which is allowed to dissolve in brine with default settings, by using the internal Duan and Sun correlation :cite:`duan2003improved`.


