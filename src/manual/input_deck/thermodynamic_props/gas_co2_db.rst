
.. _gas_co2_db_sec:

Gas defined as CO2
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
An example is: ::


  EOS GAS
    CO2_DATABASE co2_dbase.dat
  END

The file specified contains the database, see the :ref:`real CO2 database section<real_co2_db_sec>`.


