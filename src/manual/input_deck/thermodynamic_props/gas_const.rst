
.. _gas_const_sec:

Gas constant value properties
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


An example to define a gas with constant properties is given below: ::

  EOS GAS
    SURFACE_DENSITY 1.87 kg/m^3
    FORMULA_WEIGHT 44.0 g/mol
    DENSITY CONSTANT 11.00 !defaults to kmol/m3, unit cannot be specified.
    VISCOSITY CONSTANT 0.0395 cP
    ENTHALPY CONSTANT 1.8890d0 J/kmol
  END


Except for Density, the units are optional. If not entered they will take the following defaults:

* enthalpy units : :math:`J/k\mbox{mol}`
* viscosity units : :math:`Pa.s`
* density units are :math:`\mbox{kmol}/m^3`, and units cannot be specified.


