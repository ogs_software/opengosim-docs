
.. _gas_db_sec:

Gas properties defined by a database
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

An example is: ::

  EOS GAS
    DATABASE gas_eos_database.dat
  END

The file containing the database must have the format specified in :ref:`EOS properties database<eos_props_db_sec>` and contains all the properties needed for the gas (density, viscosity, enthalpy). For the :ref:`GAS_WATER<formulation-of-gw>` mode that model solubility effects, the fugacity is also required.

