
.. _gas_ideal_sec:

Ideal Gas Law
^^^^^^^^^^^^^^^^^^^^^


Defines an ideal gas, for example: ::

  EOS GAS
    DENSITY IDEAL
    ENTHALPY IDEAL_CO2
    VISCOSITY CONSTANT 0.03 cP
  END 


The enthalpy of the ideal gas can defined specified as IDEAL_METHANE, or  IDEAL_CO2. This value cannot be defaulted for thermal problems or the code will return an error.

Viscosity units are optional, if not entered defaults to :math:`Pa.s`

The density is modeled with the gas ideal law.


.. math::
  \rho( P , T )= \frac{P}{RT},


where:

.. math::
  R = 8.31446, 
  
the gas ideal constant, units :math:`J/(\mbox{mol} K)`.

The Enthalpy will be computed differently based on the type of ideal gas selected above.

For IDEAL_METHANE the Enthalpy will be computed as:

.. math::
  h(T )=c_p T,

where

.. math::
  c_p = 35.69,
  
the constant pressure heat capacity of methane, units J/(mol K), at T = 288.15 K, P=1 bar.


For IDEAL_CO2 the Enthalpy will be computed as:

.. math::
  h(T )=c_v T +RT,

where:

.. math::
  c_v = 28.075,
  
the constant volume heat capacity of |CO2|, units :math:`J/(\mbox{mol} K)`, at T = 15 C and P= 1 atm.
