
All Oil properties defined by the same external database
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

If all oil properties (density, viscosity and enthalpy) are defined by the same :ref:`external database<eos_props_db_sec>`, only one entry is required to define the oil EOS.

See example below: ::

  EOS OIL
    DATABASE eos_oil_database.dat
  END
