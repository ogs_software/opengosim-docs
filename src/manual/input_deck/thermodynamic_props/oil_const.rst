
.. _oil_const_sec:

Constant Value Oil Properties
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


This option assigns constant values to the density and viscosity that do not change with pressure and temperature. See example below: ::

  EOS OIL
    DENSITY CONSTANT 734.29d0 !kg/m3
    ENTHALPY LINEAR_TEMP 1.d3 !c oil [J/kg/°C]
    VISCOSITY CONSTANT 0.987d-3 !Pa.s
  END

In the example above a :ref:`linear model for the enthalpy<oil_enth_lin_sec>` is used.

The units are optional, if not entered will default to :math:`kg/m^3` for the density and to :math:`Pa.s`. for the viscosity.
