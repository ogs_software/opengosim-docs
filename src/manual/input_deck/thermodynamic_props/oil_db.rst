
Oil Properties from external database
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The oil density, viscosity and enthalpy can be defined by an external database that tabulates the dependency with respect to temperature and pressure.

See example below: ::

  EOS OIL
    DENSITY DATABASE density_database.dat
    ENTHALPY DATABASE enthalpy_database_file_name.dat
    VISCOSITY DATABASE viscosity_database.dat
  END

For the format of the database, see the :ref:`eos properties database<eos_props_db_sec>`. 
