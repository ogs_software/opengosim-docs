
.. _oil_den_inv_lin_sec:

Oil Inverse Linear Density model
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


The inverse linear density model requires the input of a reference density, pressure and temperature, and compressibility and expansion coefficients. See example below: ::

  EOS OIL
    FORMULA_WEIGHT 508.0d0
      DENSITY INVERSE_LINEAR
      REFERENCE_VALUE 995.98d0          !den0 [kg/m3]
      PRES_REF_VALUE 1.13d5             !p0 [Pa]
      TEMP_REF_VALUE 21.d0              !T0 [°C]
      COMPRESS_COEFF 5.63d-10           !cp [1/Pa]
      THERMAL_EXPANSION_COEFF 8.480d-4  !ct [1/°C]
    END
    ENTHALPY LINEAR_TEMP 1.d3           !c oil [J/kg/°C]
    VISCOSITY CONSTANT 3.0d-3           ![Pa.s]
  END



The mathematical expression of the model is reported below:

.. math::
  \rho( p ,T )= \frac{\rho_0}{ [1−c_p ( p−p_0 )] [1+c_T (T −T_0 )]}

where:

.. math::
  \begin{split}
  & p &=  \mbox{pressure [Pa]} \\
  &T &=  \mbox{temperature [°C]} \\
  &\rho_0 &= \mbox{reference density [kg/m3]} \\
  &p_0 &= \mbox{reference pressure [Pa]} \\
  &T_0 &=\mbox{reference temperatures [°C]}  \\
  &C &=\mbox{compressibility coefficient [kg/m3/Pa]} \\
  &E &=\mbox{thermal expansion coefficient [kg/m3/°C]} 
  \end{split}

Units cannot be specified, therefore values must be entered in the units prescribed above.

