
.. _oil_den_lin_sec:

Oil Linear Density model
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


The linear density model requires the input of a reference density, pressure and temperature, and compressibility and expansion coefficients. See example below: ::

  EOS OIL
    DENSITY LINEAR
      REFERENCE_VALUE 833.5d0       !den0 [kg/m3]
      PRES_REF_VAULE 5.516d6        ![Pa]
      TEMP_REF_VALUE 94.8           ![°C]
      COMPRESS_COEFF 1.672d-7       !C [kg/m3/Pa]
      THERMAL_EXPANSION_COEFF 0.0d0 !E [kg/m3/°C]
    END
    ENTHALPY LINEAR_TEMP 1.d3       !c oil [J/kg/°C]
    VISCOSITY CONSTANT 0.987d-3 
  END


The mathematical expression of the model is reported below:

.. math:: 
  \rho= \rho_0 + C ( p − p_0 ) − E ( T − T_0 )

where:

.. math::
  \begin{split}
  & p &=  \mbox{pressure [Pa]} \\
  &T &=  \mbox{temperature [°C]} \\
  &\rho_0 &= \mbox{reference density [kg/m3]} \\
  &p_0 &= \mbox{reference pressure [Pa]} \\
  &T_0 &=\mbox{reference temperatures [°C]}  \\
  &C &=\mbox{compressibility coefficient [kg/m3/Pa]} \\
  &E &=\mbox{thermal expansion coefficient [kg/m3/°C]} 
  \end{split}


Units cannot be specified, therefore values must be entered in the units prescribed above.

