
.. _oil_enth_lin_sec:

Oil linear enthalpy
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


The linear oil enthalpy model relates the enthalpy with temperature only, using the oil specific heat capacity. An example is given below: ::

  EOS OIL
    DENSITY CONSTANT 734.29d0 !kg/m3
    ENTHALPY LINEAR_TEMP 1.d3 !c oil [J/kg/°C]
    VISCOSITY CONSTANT 0.987d-3 !
  END


The mathematical expression of the model is represented below:

.. math::
  h_{\mbox{oil}} =c_{\mbox{oil}} T,

where:

.. math::
  \begin{split}
  &h_{\mbox{oil}} &= \mbox{oil Enthalpy [J]}\\
  &c_{\mbox{oil}} &= \mbox{oil specific heat capacity [J/kg/°C]}\\
  &T &= \mbox{temperature [°C]}
  \end{split}

Units cannot be specified, therefore the oil specific heat must be entered in the units
prescribed above.

