
.. _oil_enth_quad_sec:

Oil quadratic enthalpy
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The oil enthalpy can be modelled using the present quadratic model which accounts for
temperature dependence only. Two reference temperatures and two coefficients need to be
entered. 

See example below. ::

  EOS OIL
    DENSITY CONSTANT 734.29d0 !kg/m3
      VISCOSITY CONSTANT 0.987d-3
      ENTHALPY QUADRATIC_TEMP
      TEMP_REF_VALUES 15.5555556 15.5555556 ![°C]
      TEMP_COEFFICIENTS 1.653D3 4.001d0 ![J/kg °C] [J/(kg °C °C)]
    END
  END


The mathematical expression of this model is given below

.. math::
  h(T )=c_{t,1} (T −T_1 )+ \frac{1}{2} c_{t,2}  (T −T_2 )^2

where:

.. math::
  \begin{split}
  &h(T ) &= \mbox{oil enthalpy [J/kg]}\\
  &T_1 &= \mbox{linear reference temperature [C]}\\
  &c_{t,1} &= \mbox{linear dependency coefficient [J/(kg C)]}\\
  &T_2 &= \mbox{quadratic reference temperature [C]}\\
  &c_{t,2} &= \mbox{quadratic dependency coefficient [J/(kg C C)]}
  \end{split}

Units cannot be specified, therefore values must be entered in the units prescribed above.

