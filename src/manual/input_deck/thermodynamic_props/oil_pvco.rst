
.. _oil_pvco_sec:

:index:`PVCO`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The PVCO block is used to define the properties of a live oil for a range of different temperature and pressures. Below an example where data is given for only one temperature, and field units (i.e. different from default) are specified for pressure, :math:`R_s`, formation volume factor, compressibility and viscosibility: ::

  PVCO
    DATA_UNITS
      PRESSURE psi
      RS Mcf/bbl
      FVF bbl/bbl
      COMPRESSIBILITY 1/psi
      VISCOSIBILITY 1/psi
    END
    DATA
      TEMPERATURE 15.0
        14.7   0.001  1.062 1.04  1.36D-5 9.01D-5
        264.7  0.0905 1.15  0.975 1.36D-5 9.01D-5
        514.7  0.18   1.207 0.91  1.36D-5 9.01D-5
        1014.7 0.371  1.295 0.83  1.36D-5 9.01D-5
        2014.7 0.636  1.435 0.695 1.36D-5 9.01D-5
        2514.7 0.775  1.5   0.641 1.36D-5 9.01D-5
        3014.7 0.93   1.565 0.594 1.36D-5 9.01D-5
        4014.7 1.270  1.695 0.51  1.36D-5 9.01D-5
        5014.7 1.618  1.827 0.449 1.36D-5 9.01D-5
      END !end TEMP block
    END !endDATA
  END !end PVDO


The PVCO block may contain several pressure tables, one for each temperature. In
the pressure table, column 1 is the pressure, column 2 is the solution gas-oil ratio (Rs),
column 3 is the formation volume factor (:math:`B_o`), column 4 is the viscosity (:math:`\mu_o`), column 5 is the compressibility, :math:`C_o =-(dB_o/dP)/B_o` , column 6 the viscosibility, :math:`C_v =(d\mu_o/dP)/\mu_o`.

The pressure in column 1 must increase monotonically, and the pressure tables must be
ordered for increasing temperatures. The number of pressure points given for each
temperature must be the same, however the values and range of the pressure can vary.
Temperatures and pressures outside the given range are extrapolated. When pressure data
are available for only one temperature, the properties are considered isothermal.

The default units used by this table are:

* Bar for the pressure,
* :math:`m^3/m^3` for :math:`R_s` and :math:`B_o`
* cP (centipoise) for the viscosity
* 1/Bar for the compressibility and viscosibility
* C for temperature.

DATA_UNITS specifies units different than the default values, and include only the properties
(in any order) for which units are needed. The temperatures can currently be entered only
in degrees Celsius.

