
.. _oil_pvdo_sec:

:index:`PVDO`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The PVDO block is used to define the properties of a dead oil for a range of different temperature and pressures. Below is an example where data is given for only one temperature, and field units (i.e. different from default) are specified for pressure and formation volume factor. ::

  PVDO
    DATA_UNITS
      PRESSURE psi
      FVF bbl/bbl
    END
    DATA
      TEMPERATURE 15.0
        14.7 1.062 1.04
        264.7 1.060 1.04
        514.7 1.058 1.04
        1014.7 1.055 1.04
        2014.7 1.050 1.04
        2514.7 1.045 1.04
        3014.7 1.040 1.04
        4014.7 1.035 1.04
        5014.7 1.030 1.04
      END !end TEMP block
    END !end DATA
  END !end PVDO

The PVDO block may contain several different pressure tables, one for each temperature.

The PVDO block is used to define the properties of a dead oil for a range of different
temperature and pressures.

The PVDO block may contain several different pressure tables, one for each temperature. In
the pressure table, column 1 is the pressure, column 2 is the formation volume factor (:math:`B_o` , column 3 is the viscosity (:math:`\mu_o`).

The pressure in column 1 must increase monotonically, and the pressure tables must be
ordered for increasing temperatures. The number of pressure points given for each
temperature must be the same, however the values and range of the pressure can vary.
Temperatures and pressures outside the given range are extrapolated. When pressure data
are available for only one temperature, the properties are considered isothermal.

The default units used by this table are:

* Bar for the pressure,
* :math:`m^3/m^3` for :math:`B_o`
* :math:`cP` (centipoise) for the viscosity
* C for temperature.

DATA_UNITS specifies units different than the default values, and include only the properties
(in any order) for which units are needed. The temperatures can currently be entered only
in degree Celsius.


