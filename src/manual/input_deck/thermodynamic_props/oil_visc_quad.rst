
.. _oil_visc_quad_sec:

Oil quadratic viscosity
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


With this model the oil viscosity has a quadratic dependence with respect to the pressure and temperature. Two reference values are required for both pressure and temperature, plus corresponding coefficients. 

See an example below: ::

  EOS OIL
    DENSITY CONSTANT 734.29d0 !kg/m3
      ENTHALPY LINEAR_TEMP 1.d3 !c oil [J/kg/°C]
      VISCOSITY QUADRATIC
      REFERENCE_VALUE 2.94d-3
      PRES_REF_VALUES 1.d5 0.0
      TEMP_REF_VALUES 10.0 0.0
      PRES_COEFFICIENTS 0.0d0 0.0d0
      TEMP_COEFFICIENTS -0.01522d-3 0.0d0
    END
  END


The mathematical expression of the model is as follows:

.. math:: 

  \mu (P ,T )=\mu_0 +a_1 ( P − P_1 ) +b_1 ( T −T_1 ) + a_2 ( P− P_2 )^2 +b_2 ( T −T_2 )^2

where

.. math::
  \begin{split}
  &\mu &= \mbox{viscosity [Pa.s]} \\
  &\mu_0 &= \mbox{reference viscosity [Pa.s]} \\
  &P_1 &= \mbox{linear pressure reference [Pa]} \\
  &P_2 &= \mbox{quadratic pressure reference [Pa]} \\
  &a_1 &= \mbox{linear pressure coefficient [s]} \\
  &a_2 &= \mbox{quadratic pressure pressure coefficient [s/Pa]} \\
  &T_1 &= \mbox{linear temperature reference [°C]} \\
  &T_2 &= \mbox{quadratic temperature reference [°C]} \\
  &b_1 &= \mbox{linear temperature coefficient [Pa.s/°C]} \\
  &b_2 &= \mbox{quadratic temperature coefficient [Pa.s/(°C)^2]}
  \end{split}

Setting to zero the quadratic coefficients the quadratic model simplifies to a linear model.
Units cannot be specified, therefore values must be entered in the units prescribed above.

