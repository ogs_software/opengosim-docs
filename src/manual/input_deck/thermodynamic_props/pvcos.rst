

.. _pvcos_sec:

:index:`PVCOS`
---------------------------------------------------

This table is used with :ref:`COMP4<multigas_intro_sec>`; where it describes the degree of solution of the solvent component in the oil, and the properties of the oil with dissolved solvent. This table goes in the EOS OIL block and follows the pattern of the PVCO table. If not defined default to the PVCO table. An example is given below: ::

  PVCOS
    DATA_UNITS
      PRESSURE                Bar
      RS                      m^3/m^3
      FVF                     m^3/m^3
      VISCOSITY               cP
      COMPRESSIBILITY         1/Bar
      VISCOSIBILITY           1/Bar
    END
    DATA
      TEMPERATURE 15.0
          1.0135     0	  1.034	 0.310	2.92780E-04  0.00211302
         34.473	  20.945  1.1017 0.295	2.92780E-04  0.00211302
         68.947	  39.646  1.1478 0.274	2.92780E-04  0.00211302
         82.737	  47.679  1.1677 0.264	2.92780E-04  0.00211302
        103.421	  60.805  1.1997 0.249	2.92780E-04  0.00211302
        124.106	  75.072  1.235	 0.234	2.92780E-04  0.00211302
        137.895	  85.313  1.26	 0.224	2.92780E-04  0.00211302
        158.717	 102.020  1.301	 0.208	2.92780E-04  0.00211302
        172.369	 112.938  1.327	 0.200	2.92780E-04  0.00211302
        206.843	 140.580  1.395	 0.187	2.92780E-04  0.00211302
        241.317	 168.204  1.463	 0.175	2.92780E-04  0.00211302
        275.790	 195.829  1.5312 0.167	2.92780E-04  0.00211302
        310.264	 223.471  1.5991 0.159	2.92780E-04  0.00211302
        330.948	 240.053  1.6398 0.155	2.92780E-04  0.00211302
      END !end TEMP block
    END !endDATA
  END !end PVCOS


The PVCOS block may contain several pressure tables, one for each temperature. In the pressure table, column 1 is the pressure, column 2 is the solution solvent-oil ratio (:math:`Rss`), column 3 is the formation volume factor (:math:`Bo`), column 4 is the viscosity (:math:`μo`), column 5 is the compressibility, :math:`Co=−(dBo/dP)/Bo`, column 6 the viscosibility, :math:`Cv=(dμo/dP)/μo`.

The pressure in column 1 must increase monotonically, and the pressure tables must be ordered for increasing temperatures. The number of pressure points given for each temperature must be the same, however the values and range of the pressure can vary. Temperatures and pressures outside the given range are extrapolated. When pressure data are available for only one temperature, the properties are considered isothermal.

The default units used by this table are:

    * Bar for the pressure,
    * m3/m3 for :math:`Rss` and :math:`Bo`
    * cP (centipoise) for the viscosity
    * 1/Bar for the compressibility and viscosibility
    * C for temperature.

DATA_UNITS specifies units different than the default values, and include only the properties (in any order) for which units are needed. The temperatures can currently be entered only in degrees Celsius.



