
.. _solv_co2_db_sec:

Solvent defined as |CO2|
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Defines the solvent as real CO2. An example is given below: ::

  EOS SOLVENT
    CO2_DATABASE co2_dbase.dat
  END

The data files used must follow the specification of the real CO2 database :ref:`section<real_co2_db_sec>`.

