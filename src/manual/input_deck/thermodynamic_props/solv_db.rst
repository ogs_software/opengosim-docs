
.. _solv_db_sec:

Solvent defined by database
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Defines the solvent properties using an external database. An example is given below: ::

  EOS SOLVENT
    DATABASE solvent_dbase.dat
  END

The database in the file must have the format specified in the :ref:`EOS properties database<eos_props_db_sec>` and contains all the properties needed for the solvent (density, viscosity, enthalpy). For COMP3 and COMP4 modes that model solubility effects, the fugacity is also required.

