
.. _solv_ideal_sec:

Solvent Ideal Gas Law
^^^^^^^^^^^^^^^^^^^^^^^^^^^^


Defines the solvent as an ideal gas. See example with ideal density and enthalpy: ::

  EOS SOLVENT
    DENSITY IDEAL
    ENTHALPY IDEAL_CO2
    VISCOSITY CONSTANT 0.03 cP
  END

The type of ideal gas for the computation of enthalpy purposes can take the following values: IDEAL_METHANE, IDEAL_CO2. This value cannot be defaulted, for thermal problems or the code will return an error.

Viscosity units are optional, if not entered will default to :math:`Pa.s`

The density is modelled with gas ideal law:

.. math::
  \rho( P , T )= \frac{P}{RT},


where:

.. math::
  R = 8.31446,
  
the gas ideal constant,  units sJ/(mol K).

The Enthalpy will be computed differently based on the type of ideal gas selected above.

For IDEAL_METHANE the Enthalpy will be computed as:

.. math::
  h(T )= p_T,

where:

.. math::
  c_p = 35.69,


the constant pressure heat capacity of methane, units J/(mol K), at T = 288.15 K, P=1 bar.

For IDEAL_CO2 the Enthalpy will be computed as:

.. math::

  h(T )=c_v T + RT,

where:

.. math::
  c_v = 28.075, 
  
the constant volume heat capacity of |CO2|, units J/(mol K), at T = 15 C and P= 1 atm.
