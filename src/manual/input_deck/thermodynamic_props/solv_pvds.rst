
.. _pvds_sec:

:index:`PVDS`
^^^^^^^^^^^^^^^^^^^^^

The PVDS table is used to define the properties of a solvent for a range of different temperature and pressures. See example below, where data is given for only one temperature: ::

  PVDS
    DATA_UNITS
      PRESSURE Bar
      FVF m^3/m^3
      VISCOSITY cP
    END
    DATA
      TEMPERATURE 15.0
        1.013529   0.9357635 0.0080
        18.250422  0.0678972 0.0096
        35.487314  0.0352259 0.0112
        69.961099  0.0179498 0.0140
        138.908669 0.0090619 0.0189
        173.382454 0.0072653 0.0208
        200.00     0.0063404 0.0223
        207.856239 0.0060674 0.0228
        276.803809 0.0045534 0.0268
        345.751379 0.0036439 0.0309
        621.541659 0.0021672 0.0470
      END !end TEMP block
    END !end DATA
  END !end PVDS

The PVDS data may contain several different pressure tables, one for each temperature. In
the pressure table, column 1 is the pressure, column 2 is the solvent formation volume
factor (:math:`B_v`), column 3 is the solvent viscosity (:math:`\mu_g`).

The pressure in column 1 must increase monotonically, and the pressure tables must be
ordered for increasing temperatures. The number of pressure points given for each
temperature must be the same, however the values and range of the pressure can vary.
Temperature and pressure outside the given range are extrapolated. When pressure data
are available for only one temperature, the properties are considered isothermal.

The default units used by this table are:

* Bar for the pressure,
* :math:`m^3/m^3` for :math:`B_s`
* :math:`cP` (centipoise) for the viscosity
* C for temperature.

DATA_UNITS specifies units different than the default values, and include only the properties
(in any order) for which units are needed. The temperatures can currently be entered only
in degree Celsius.

