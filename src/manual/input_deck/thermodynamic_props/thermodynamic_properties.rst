
.. _thermodynamic_props_sec:

Thermodynamic Properties of Reservoir Fluids
---------------------------------------------------

The thermodynamic properties such as density, enthalpy and viscosity of the reservoir fluid
phases can be computed in different ways:

* by the mean of analytical models
* by external databases given in the format accepted by PFLOTRAN
* by PVT table formats commonly used in reservoir engineering

The input keyword to enter the properties of a phase is EOS, which stands for Equation of
State, followed by the name of the phase.

Water properties are required for all flow models.

Gas properties are required for Gas-Water, Multi-Gas, Black Oil, Todd-Longstaff and Solvent models.

Oil properties are required for Oil-Water, Black Oil, Todd-Longstaff and Solvent models.

The properties of the solvent component are required for the Multi-Gas Solvent models. For Todd-
Longstaff, the gas properties define the solvent properties, as the gas is effectively the
solvent for this model.

The Multi-component modelling option uses a multi component equation of state (EOS COMP) to define the properties of the non-aqueous phases.

The following EOS cards are available:

.. toctree::
   :maxdepth: 1

   eos_water
   eos_oil
   eos_gas
   eos_solv
   eos_comp

See also the following information about databases:

.. toctree::
   :maxdepth: 1

   co2_database
   eos_properties_database




