
.. _wat_BW_sec:

Water properties - Batzle and Wang model
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The model from Batzle and Wang (1992) is also available for the computation of water
density and viscosity, valid for a pressure and temperature range of :math:`5 < P < 100` MPa, and
:math:`20 < T < 350` °C. This model is considered one of the most accurate for high level of
salinity, up to 320000 mg/L. For more details, please refer to :cite:`batzle1992seismic`.


See example below: ::

  EOS WATER
    SURFACE_DENSITY 1004.0 kg/m^3
    DENSITY BATZLE_AND_WANG
    VISCOSITY BATZLE_AND_WANG
  END

Note that in the BATZLE_AND_WANG example described above, the IFC67 default model is
used for enthalpy, because no other model is specified.
