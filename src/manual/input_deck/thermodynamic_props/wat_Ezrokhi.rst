
.. _wat_Ezrokhi_sec:

Water properties - Ezrokhi model for density
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The model from Ezrokhi can be selected for the computation of brine density. For more details, please refer to :cite:`EzrokhiModel`.


See example below: ::

  EOS WATER
    SURFACE_DENSITY 1004.0 kg/m^3
    DENSITY EZROKHI
  END

Note that in the EZROKHI example described above, the :ref:`IFC67<wat_ifc67_sec>` default model is
used for enthalpy and :ref:`Batzle and Wang<wat_BW_sec>` for viscosity, because no other models are specified.
