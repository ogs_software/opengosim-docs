
.. _wat_Kestin_sec:

Water properties - Kestin model for viscosity
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The model from Kestin et al. can be selected for the computation of brine viscosity, valid for a pressure, temperature and salt concentration range of :math:`0.1 < P < 35` MPa,
:math:`20 < T < 150` °C and :math:`0 < C < 5.4` Molal. The model guarantees an accuracy of 0.5% with experimental data. For more details, please refer to :cite:`Kestin1981`.


See example below: ::

  EOS WATER
    SURFACE_DENSITY 1004.0 kg/m^3
    VISCOSITY KESTIN
  END

Note that in the KESTIN example described above, the :ref:`IFC67<wat_ifc67_sec>` default model is
used for density and enthalpy, because no other model is specified.
