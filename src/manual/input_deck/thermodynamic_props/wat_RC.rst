
.. _wat_Rowe_and_Chou_sec:

Water properties - Rowe and Chow model for density
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The model from Rowe and Chou can be selected for the computation of brine density, valid for a pressure, temperature and salt concentration range of :math:`0.1 < P < 68` MPa,
:math:`0 < T < 195` °C and :math:`0 < C < 25` Weight percent. The model guarantees an accuracy of 0.2% with experimental data. For more details, please refer to :cite:`RoweImplementation`.


See example below: ::

  EOS WATER
    SURFACE_DENSITY 1004.0 kg/m^3
    DENSITY ROWE_AND_CHOU
  END

Note that in the ROWE_AND_CHOU example described above, the :ref:`IFC67<wat_ifc67_sec>` default model is
used for enthalpy and :ref:`Batzle and Wang<wat_BW_sec>`  for viscosity, because no other models are specified.
