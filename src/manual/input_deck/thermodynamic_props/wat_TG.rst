
.. _wat_TG_sec:

Water properties – Trangenstein and Grabowsky
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This uses a model from Trangenstein to describe density dependence on temperature and
pressure, while it uses a model from Grabowsky to model the water viscosity as function of
the temperature. 

See example below: ::

  EOS WATER
    SURFACE_DENSITY 1000.0 kg/m^3
    DENSITY TRANGENSTEIN
    VISCOSITY GRABOWSKI
  END

Note that in the example above, the IFC67 default model is used for enthalpy, because no
other model is specified.

See :ref:`Trangenstein density<wat-trang>` and :ref:`Grabowsky viscosity<wat-grabow>` models in the theory manual. These density and viscosity models do not need to be applied together, and are compatible with other property model selections.


