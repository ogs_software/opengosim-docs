
.. _wat_const_sec:

Water constant value properties
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
An example is given below: ::

  EOS WATER
    SURFACE_DENSITY 1000.0 kg/m^3
    DENSITY CONSTANT 997.16d0 kg/m^3
    ENTHALPY CONSTANT 1.8890d0
    VISCOSITY CONSTANT 8.904156d-4
  END

The units are optional, if not entered they default to
density_units = kg/m^3
enthalpy_units = J/kmol
viscosity_units = Pa.s

