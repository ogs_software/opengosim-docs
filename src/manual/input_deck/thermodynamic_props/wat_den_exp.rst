
.. _wat_den_exp_sec:

Water density exponential model
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

See example below: ::

  EOS WATER
    SURFACE_DENSITY 1004.0 kg/m^3
    DENSITY EXPONENTIAL 1015.0313 34473786 4.351132E-10
    VISCOSITY CONSTANT 0.0003
  END

Note that in the example described above, the IFC67 default model is used for the
enthalpy, because no other models are specified.

The three values entered are, in order:
  
  * The reference density, :math:`\rho_{ref}`, units kg/m^3
  * The reference pressure, :math:`P_{ref}`, units Pa
  * The water compressibility, :math:`C`, units 1/Pa

These values are used in the following model. The density is computed as:

.. math::
  \rho( P ,T )=\rho_{\mbox{ref}} \exp \left[C (P−P_{\mbox{ref}} )\right]

Units cannot be entered and the values must be entered in the units reported above.


