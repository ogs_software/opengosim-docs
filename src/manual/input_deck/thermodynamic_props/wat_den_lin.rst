
.. _wat_den_lin_sec:

Water density linear model
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

See Example below: ::

  EOS WATER
    SURFACE_DENSITY 1004.0 kg/m^3
    DENSITY LINEAR 1000.0 1.0D7 4.6D-10
  END

Note that in the example above, IFC67 default models are used for viscosity and enthalpy, because no other models are specified.

The three values entered are, in order:
  
  * The reference density, :math:`\rho_{ref}`, units kg/m^3
  * The reference pressure, :math:`P_{ref}`, units Pa
  * The water compressibility, :math:`C`, units 1/Pa

These values are used in the following model. The density is computed as:


.. math::
  \rho( P)=\rho_{\mbox{ref}} [1+C (P− P_{\mbox{ref}})]

Units cannot be entered and the values must be entered in the units reported above.

