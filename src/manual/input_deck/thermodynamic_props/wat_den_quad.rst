
.. _wat_den_quad_sec:

Water density quadratic model
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

See example below: ::

  EOS WATER
    SURFACE_DENSITY 1004.0 kg/m^3
    DENSITY QUADRATIC
      REFERENCE_DENSITY 1000
      REFERENCE_PRESSURE 1.0D7
      WATER_COMPRESSIBILITY 4.6D-10
    END
  END


Note that in the example described above, the IFC67 default model is used for the enthalpy, because no other models are specified.


The density is computed as:

.. math::
  \rho(P)=ρ_{ ref }\left[1+ X_c +  \frac{X_c^2}{2} \right]

where:

.. math::
  \begin{split}
  &X_C &= C(P-P_{\mbox{ref}}) \\
  &ρ_{ref} &= \mbox{reference density [kg/m^3]} \\
  &P_{ref} &= \mbox{reference reference pressure [Pa]} \\
  &C &= \mbox{water compressibility [1/Pa] }
  \end{split}


Units cannot be entered and the values must be entered in the units reported above.


