
.. _wat_ifc67_sec:

Water properties default model - IFC67
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

If no EOS is specified for WATER, the density, viscosity, internal energy, and enthalpy of
pure |H2O| are taken from the steam tables :cite:`ifc67`. With this method the
properties are valid for a pressure and temperature range of :math:`0 < P < 1000` bars, and :math:`0 < T < 800` °C. Although very accurate, this method can make the problem more difficult to
solve, and can cause some convergence problem in some cases.

