
.. _wat_watertab_sec:

:index:`WATERTAB` table
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The WATERTAB block is used to define aqueous phase properties for a range of
temperatures and pressures.

In the example below data is provided for just one temperature: ::

  EOS WATER
    SURFACE_DENSITY 1000.0 kg/m^3
    WATERTAB
      DATA_UNITS
        PRESSURE psi
        FVF bbl/bbl
      END
      DATA
        TEMPERATURE 115.0
          14.7 1.025335 0.3
          5000 1.01     0.3
          8000 1.000991 0.3
        END !end TEMP block
      END !end DATA
    END !end WATERTAB
  END

In the example above data is provided for just one temperature, and non-default field units are used for pressure. The IFC67 default model is used for enthalpy, because no other model is specified.

The WATERTAB block may contain several different pressure tables, one for each
temperature. In the pressure table, column 1 is the pressure, column 2 is the formation
volume factor (:math:`B_w`), column 3 is the viscosity (:math:`\mu_w`).

The pressure in column 1 must increase monotonically, and the pressure tables must be
ordered by increasing temperatures. The number of pressure points given for each
temperature must be the same, however the values and range of the pressure can vary.
Temperatures and pressures outside the given range are extrapolated. When pressure data
is available for only one temperature, the properties are considered isothermal.

The default units used by this table are:

* Bar for the pressure,
* :math:`m^3/m^3` for :math:`B_w`
* :math:`cP` (centipoise) for the viscosity
* C for temperature.

DATA_UNITS specifies units different from the default values, and need include only the
properties (in any order) for which units are needed. The temperatures can currently be
entered only in degrees Celsius.

