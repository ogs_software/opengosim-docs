
.. _time_sec:

:index:`TIME`
------------------



Specifies the initial and final simulation time and the time step sizes. Some examples are given below.
An example using DATE to define initial and finale times, and a constant maximum time step size is as follows: ::

  TIME
    START_DATE 1 JAN 2005
    FINAL_DATE 31 DEC 2500
    INITIAL_TIMESTEP_SIZE 0.001 d
    MAXIMUM_TIMESTEP_SIZE 50 d
  END


An example using a relative time given in days to specify the final time, and a varying maximum time step size is as follows: ::

  TIME
    FINAL_TIME 7300 d
    INITIAL_TIMESTEP_SIZE 1.0d-3 d
    MAXIMUM_TIMESTEP_SIZE 0.5 d at 0 d
    MAXIMUM_TIMESTEP_SIZE 50 d at 20 d
  END

In this last example, the start date is defaulted to 1 JAN 2000 for output purposes.

Only the FINAL_TIME keyword is compulsory; however specifying the initial and the maximum time steps, which depend on the problem being model, is advised. Mainly when in presence of strong transient phenomena in the first part of the simulation.

The maximum allowed time step can be varied during the simulation, specifying the new maximum time step and from when it has to start. An example of such instruction is: ::

  MAXIMUM_TIMESTEP_SIZE 50 d at 20 d

This allows a maximum time step of 50 days once the simulation has advanced to 20 days since the start.

Note that DATE cannot used to specify when a new maximum time step will be imposed. Therefore the user has to count the time in days since the simulation started, paying attention at leap years.


WSTEP
^^^^^^^^^

This sets the maximum timestep after there is a change in target for any well or well group. Example: ::

  
  TIME
    START_DATE 1 JAN 2005
    FINAL_DATE 31 DEC 2500
    INITIAL_TIMESTEP_SIZE 0.001 d
    MAXIMUM_TIMESTEP_SIZE 50 d
    WSTEP 1 d
  END

In this case the maximum timestep is one day after a change in well targets.
