.. _unit-conventions:

Units and Conventions
---------------------
In most cases, the user can specify the units for the values entered in the input.
The units available for different physical quantities are listed in the table below. Note that the units are case-sensitive, and for some units different aliases are possible.

+---------------------------------------------------------------+-------------------------------------+
| Physical quantity Units                                       | Units                               |
+===============================================================+=====================================+
| <pressure_units> (absolute)                                   | | Bars = "Bar"                      |
|                                                               | | Pounds Per Square Inches = "psi"  |
|                                                               | | Pascal = "Pa"                     |
|                                                               | | Kilo Pascal = "kPA"               |
|                                                               | | Mega Pascal = "MPa"               |
+---------------------------------------------------------------+-------------------------------------+
| <temperature_units>                                           | | Degrees Celsius = "C"             |
|                                                               | | Degrees Farenheit = "F"           |
+---------------------------------------------------------------+-------------------------------------+
| <length_units>                                                | | Meters = "m"                      |
|                                                               | | Kilometers = "Km"                 |
|                                                               | | Decimeters = "dm"                 |
|                                                               | | centimeters = "cm"                |
|                                                               | | millimeters = "mm"                |
|                                                               | | feet = "ft"                       |
+---------------------------------------------------------------+-------------------------------------+
| <mass_units>                                                  | | Kilograms = "Kg"                  |
|                                                               | | Grams ="g"                        |
|                                                               | | tonne = "t", "tonne"              |
|                                                               | | Mega-tonne = "Mt", "Mtonne"       |
+---------------------------------------------------------------+-------------------------------------+
| <time_units>                                                  | | Years = "year", "y"               |
|                                                               | | Days = "day", "d"                 |
|                                                               | | Hours = "hour", "h"               |
|                                                               | | Minutes = "min"                   |
|                                                               | | Seconds = "sec", "s"              |
+---------------------------------------------------------------+-------------------------------------+
| <area_units>                                                  | | Square meters = "m^2"             |
|                                                               | | Square kilometer = "km^2"         |
|                                                               | | Square centimeters = "cm^2"       |
|                                                               | | Square decimeters = "dm^2"        |
+---------------------------------------------------------------+-------------------------------------+
| <volume_units> (used for both surface and reservoir volumes)  | | Cubic meters = "m^3"              |
|                                                               | | Barrels = "bbl"                   |
|                                                               | | Cubic feet = "cf"                 |
|                                                               | | Thousand Cubic feet ="Mcf"        |
|                                                               | | Gallons = "gallon", "gal"         |
+---------------------------------------------------------------+-------------------------------------+
| <viscosity_units>                                             | | Centi Poise = "centiPoise", "cP"  |
|                                                               | | Poise = "poise", "P"              |
|                                                               | | Pascal seconds = "Pa.s"           |
+---------------------------------------------------------------+-------------------------------------+
| <power_units>                                                 | | Watts = "W"                       |
|                                                               | | Kilowatts = "kW"                  |
|                                                               | | Megawatts = "MW"                  |
+---------------------------------------------------------------+-------------------------------------+
| <energy_units>                                                | | Joule = "J"                       |
|                                                               | | kilo Joule ="kJ"                  |
|                                                               | | Mega Joule = "MJ"                 |
+---------------------------------------------------------------+-------------------------------------+
| <molar_mass>                                                  | | Moles = "mol", "mole", "moles"    |
|                                                               | | Kilomoles = "kmol"                |
+---------------------------------------------------------------+-------------------------------------+
| <concentration_units>                                         | molarity = "M"                      |
+---------------------------------------------------------------+-------------------------------------+

The units above can be used in combination with the forward slash for physical quantities that can be described as ratio of other two.
For example for a density the user can enter kilogram over cubic meters::

  SURFACE_DENSITY 1000 kg/m^3

Note: the unit definitions can be case-dependent.
However, an error will be issued if the unit requested does not match any of the available units.

For those input cards where the user is not forced to enter the units, or cannot enter them, the following default units are assumed:

* Pressure: Pascal [Pa] (absolute)
* Temperature: Celsius [C]
* Distance: meter [m]
* Volume: meters\ :sup:`3` \ [m^3]
* Time: second [s]
* Velocity: meters/second [m/s]
* Enthalpy: kilojoules/mole [KJ/mol]
* Mass: kilograms [kg]
* Rate: mass/time [kg/s] or volume/time [m^3/s]
* Rock density: kilograms/meters\ :sup:`3` \ [kg/m^3]
* Concentration: molarity [M] if MOLAL keyword used in CHEMISTRY

Note: PFLOTRAN does not support a full set of Imperial units (like the Eclipse 'FIELD' unit set).
Although, as reported in the unit table above, some imperial units are available: 'psi' for pressure input in Psia, bbl/day and Mcf/day for well rates, etc.
Output is always in metric units.

Note: in PFLOTRAN, as default, the vertical distances are measured as elevations, that is, as positive distances upwards.
This is different from that used in many oil reservoir simulators such as Eclipse, which measure vertical distances as positive depths downwards.
In a similar way, K indices for IJK locations of cells in a regular grid count from the bottom up.
However, for most inputs described in this manual, the standard reservoir engineering convention can be used.
When this option is available, K indices and vertical distances bare the "_D" suffix to indicate top to bottom counting, and depth from a reference.
For example, the datum depth for static equilibration can be defined as::

  DATUM_D = 300 m

Note: when reading data in a GRDECL file, the ordering, depth treatment and unit conventions always follow those of Eclipse.
