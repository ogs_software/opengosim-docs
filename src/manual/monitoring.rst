
Monitoring the Simulation
===============================

There are two possible ways to monitor a simulation via a screen output

.. contents::
  :backlinks: top
  :depth: 2
  :local:

LINEREPT can be activated in :ref:`OUTPUT<output_sec>` and displays the engineering results of the study. If LINEREPT is not activate, the monitor will displays a convergence analysis, which might be helpful to investigate trouble studies that struggle to advance in time.


LINEREPT
-------------------------

The LINEREPT option provides a simple one-line-per-step output which displays the engineering results of the study. LINEREPT is an optional keyword in the the OUTPUT section. When LINEREPT is used, the output looks like:

.. figure:: images/linerpt.png
   :scale: 50% 

The columns display the oil production total and rate, the water and gas production rates,
the water and gas injection rates, the water cut, the gas-oil ratio, the average reservoir
pressure and the non-linear iterations for each step. The LI column is the average number
of linear iterations per step, and the Ch column is the number of chopped time steps since
the last reported completed step (normally zero).

Detailed Convergence Monitoring
---------------------------------------------------------------

The default form of typical simulation output to the screen or to the log file is shown below: ::


  == TOWG FLOW ===================================================================
    0 2r: 2.72E+04 2x: 0.00E+00 2u: 0.00E+00 ir: 1.95E+04 iu: 0.00E+00 iSr: 5.47E-03 rsn:   0
    1 2r: 1.82E+01 2x: 7.15E+08 2u: 1.39E+05 ir: 1.80E+01 iu: 1.55E+04 iSr: 8.10E-07 rsn:   0
    2 2r: 1.70E-05 2x: 7.15E+08 2u: 2.02E+02 ir: 5.52E-06 iu: 5.60E+01 iSr: 1.60E-12 rsn: itol_scaled_res

   Step      2 Time=  8.21918E-04 Dt=  5.47945E-04 [y] snes_conv_reason:   22
    newton =   2 [       4] linear =     9 [        17] cuts =  0 [   0]
    --> SNES Linear/Non-Linear Iterations =            9  /            2
    --> SNES Residual:   1.702288E-05  3.404576E-09  5.516551E-06
    --> max chng: dpl=   1.5593E+04 dsl=   0.0000E+00 dxmol_l=   2.8254E-03 dxmol_g=   8.6104E-07 dpg=   1.5593E+04 dt=   0.0000E+00




In the example above, each of the first three lines correspond to a non-linear iteration
taking place during a time step. The most important value to monitor is the one marked by
ir (i.e. infinity norm residual). This is the infinity norm of the residual, and the simulation will not advance to the next time step, until this value is lower than the threshold set up in
ITOL, in the :ref:`NEWTON_SOLVER block<newton_solver_flow_sec>`. 

In the line starting by Step, the first integer indicates the time step number (2 in the
example), Time indicates the simulation physical time (:math:`8.21918 \times 10^{-4}` years in the example), Dt the
current time step (:math:`5.47954 \times 10^{-4}` years in the example), snes_conv_reason, the convergence reason (22 in the example). The convergence reason gives information on why the simulator decided
that the solution is converged. Two common output are: 10, when the ITOL residual falls
below the imposed threshold, 11 when the solution changes are smaller than a certain
values imposed ITOL_UPDATE.

Finally other useful parameters are output by the simulator to analyze the numerical
solution procedure:

* newton = Dt_Newton_it [ Total_Newton_it ], where:

  * Dt_Newton_it = number of Newton iteration for the current time step (2 in the example)
  * Total_Newton_it = total number of Newton iteration (4 in the example)

* Linear = Dt_linear_it [Total_Linear_it], where:

  * Dt_linear_it = number of linear iteration for the current time step (9 in the example)
  * Total_Linear_it = total number of linear iteration (17 in the example)

* cuts = Dt_cuts [ Total_Dt_cuts] , where:

  * Dt_cuts = number of time step cuts for the current time step
  * Total_Dt_cuts = total number of time step cuts

* SNES residual: Infinity norm residual for each solution variable (Pressure, saturation and Temperature)

* dpl, dpo, dso, dt: maximum changes during the time step for the liquid and oil pressures, the oil saturation and the temperature.


