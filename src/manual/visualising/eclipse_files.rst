
.. _eclipse_files_sec:

:index:`ECLIPSE_FILE`
------------------------------------------

The ECLIPSE_FILE sub-block of OUTPUT requests that the simulation outputs Eclipse format files. See example below: ::

  OUTPUT
    MASS_BALANCE_FILE
      PERIODIC TIMESTEP 1
    END
    ECLIPSE_FILE
      PERIOD_SUM TIMESTEP 5 d
      TIMES d 30 500 1000
      WRITE_DENSITY
      WRITE_VISCOSITY
      WRITE_RELPERM
      WRITE_CAPPRES
      OUTFILE WCT
      INIT_ALL
      SUMMARY_D
        [...]
      END_SUMMARY_D
    END
    LINEREPT
  END

The default is to output unformatted unified Eclipse files that are compatible with post-processors such as ResInsight, an open-source post-processor which may be downloaded from `the ResInsight website <http://www.resinsight.org>`_. Five Eclipse files are produced – a grid file (.GRID), an init file (.INIT), a specification file (.SMSPEC), a summary file (.UNSMRY) and a restart file (.UNRST). The restart files are only used as an industry standard output format by PFLOTRAN-OGS – they cannot be used to restart an Eclipse run. 


The summary files contain a set of standard mnemonics from which line graphs may be obtained, such as FGIP and WGIR. For the full list of available variables and their names refer to :ref:`Line Graph Mnemonic<line-graph-mnemonic>`.

The init and restart files contain grid and solution properties such as permeabilities and fluid saturations.

Note that for ResInsight to load data correctly, the input name must be in capital letter (e.g. CASE.IN) so that output files will also be in capital letter (CASE.GRID, CASE.UNRST, CASE.SMSPEC,  CASE.UNSMRY,  CASE.INIT)

Below are all the keywords supported within ECLIPSE_FILE:

.. contents::
  :backlinks: top
  :depth: 2
  :local:

PERIOD_SUM
^^^^^^^^^^^^^^^^^^^^^^^^^^

PERIOD_SUM controls the frequency of the summary file (line graph data) output. This may be done by timestep (TIMESTEP), or by time period (TIME).

Below is an example asking to update the summary file at each time step: ::

  OUTPUT
    ECLIPSE_FILE
        PERIOD_SUM TIMESTEP 1
        DATES  1 NOV 2017   1 JAN 2018   1 JAN 2020   1 JAN 2021   1 JAN 2022
    END
    LINEREPT
  END

And an example asking to update the summary file every 5 days: ::

  OUTPUT
    ECLIPSE_FILE
        PERIOD_SUM TIME 5 d
        DATES  1 NOV 2017   1 JAN 2018   1 JAN 2020   1 JAN 2021   1 JAN 2022
    END
    LINEREPT
  END

DATES
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Used within the ECLIPSE_FILE instruction block to ask the simulator to output ECLIPSE restart files at given dates. See example below: ::

  OUTPUT
    ECLIPSE_FILE
        PERIOD_SUM TIMESTEP 1
        DATES  1 NOV 2017   1 JAN 2018   1 JAN 2020   1 JAN 2021   1 JAN 2022
        DATES  1 JAN 2023    1 JAN 2024   1 JAN 2025   1 JAN 2026   1 JAN 2027
    END
    LINEREPT
  END

Multiple DATES lines are accepted within the ECLIPSE_FILE block, and each line can take multiple dates. Each date is defined by the date format dd MMM YYYY.

TIMES
^^^^^^^^^^^^^^^^^^
This keyword requests output of Eclipse restart and summary data at a series of specified
times. For example: ::

  TIMES y 1 2 5 10

requests output at 1, 2, 5 and 10 years.

PERIOD_RST
^^^^^^^^^^^^^^^^^^^^^

PERIOD_RST controls the frequency of the restart file (pressure and saturations for 3D
visualisation) output. This may be done by TIMESTEP,  or by TIME period.

MULTIPLE
^^^^^^^^^^^^^^^^^^^^^^^

MULTIPLE requests ouput of multiple Eclipse restart files (with suffices like X0001) rather
than a single unified file with a .UNRST suffix.

:index:`WRITE_DENSITY`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Requests output to the eclipse file system of density.

:index:`WRITE_VISCOSITY`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Requests output to the eclipse file system of viscosity.

:index:`WRITE_RELPERM`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Requests output to the eclipse file system of relative permeabilities.

:index:`WRITE_CAPPRES`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Requests output to the eclipse file system of capillary pressures.


:index:`WRITE_MASS_RATES`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

By default the simulator output rates and totals, for the entire field and wells in surface volumes. To output to the summary files also the equivalent mass quantities, this keyword must be included within the ECLIPSE_FILE block.  Example below: ::

  OUTPUT
    ECLIPSE_FILE
      PERIOD_SUM TIMESTEP 1 d
      DATES  1 NOV 2017   1 JAN 2018   1 JAN 2020   1 JAN 2021   1 JAN 2022
      WRITE_MASS_RATES
      OUTFILE
    END
    LINEREPT
  END

This will output, for example, FGMPR (Field Gas Mass Production Rate), and other mass variables to the summary files. For a detailed list, please see :ref:`line graph mnemonics<line-graph-mnemonic>`.




FORMATTED
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The default is to output unformatted unified Eclipse files, although the FORMATTED sub-keyword may be used to request formatted files. Example: ::

  OUTPUT
    MASS_BALANCE_FILE
      PERIODIC TIMESTEP 1
    END
    ECLIPSE_FILE
      PERIOD_SUM TIMESTEP 1 d
      PERIOD_RST TIME 200 d
      FORMATTED
    END
    LINEREPT
  END

OUTFILE
^^^^^^^^^^^^^^^^^^^^^^^

Requests output of tables of fluid in place, well and completion data, and timing data, to the .out file. This will
be produced at times at which Eclipse restart files are produced.

May take an optional argument to specify additional output: ::

  OUTFILE WCT

Any combination of the characters W, C, or T may be used. Each letter specifies the following:

* W: output of well data to the out file
* C: output of completion data to the out file
* T: output of timing data to the out file

specifying output of completion and well data to the out file respectively. WC indicates both. FLIP and MATBAL are output in all cases.  


The outfile produced will take the following form:

.. figure:: ../images/outfile_output_example.png

SUMMARY_D/SUMMARY_Z
^^^^^^^^^^^^^^^^^^^^^^^

This requests that cell values be written to the .UNSMRY file, so they can be plotted against time later.

``SUMMARY_D`` and ``SUMMARY_Z`` are identical except that for ``SUMMARY_D`` the k index is downwards, and for ``SUMMARY_Z`` it is upwards.

An example usage is: ::

  OUTPUT
    [...]
    ECLIPSE_FILE
      [...]
      SUMMARY_D
        BPR   10 10 3
        BOSAT 10 10 3
        BGSAT 10 10 3
        BWSAT 10 10 3
        BSSAT 10 10 3
        BDENO 10 10 3
        BDENG 10 10 3
        BDENW 10 10 3
        BDENS 10 10 3
        BVOIL 10 10 3
        BVGAS 10 10 3
        BVWAT 10 10 3
        BVSLV 10 10 3
        BGPC  10 10 3
        BWPC  10 10 3
        BTEMP 10 10 3
        BPBUB 10 10 3
      END_SUMMARY_D
    END
    LINEREPT
  END

This requests the values of several values, all at cell 10,10,3, be written to the summary file.

The variables available are:

- ``BPR``, the cell pressure.
- Phase saturations: ``BOSAT``/``BSOIL``, ``BGSAT``/``BSGAS``, ``BWSAT``/``BSWAT``, ``BSSAT``/``BSSLV``, for oil, gas, water or solvent saturation respectively.
- Phase densites: ``BODEN``/``BDENO``/``BDOIL``, ``BGDEN``/``BDENG``/``BDGAS``, ``BWDEN``/``BDENW``/``BDWAT``, ``BSDEN``/``BDENS``/``BDSLV``, for oil, gas, water or solvent density respectively.
- Phase viscosities: ``BOVIS``/``BVOIL``, ``BGVIS``/``BVGAS``, ``BWVIS``/``BVWAT``, ``BSVIS``/``BVSLV``, for oil, gas, water or solvent viscocity respectively.
- Phase relative permeabilities: ``BKRO``, ``BKRG``, ``BKRW``, ``BKRS``, for oil, gas, water or solvent relative permeability respectively.
- Capillary pressures: ``BGPC``, ``BWPC``, for gas and water capillary pressures respectively.
- Mole fractions: ``BAMFW``, ``BAMFG``, ``BYMFW``, ``BYMFG``, water in aqueous, gas in aqueous, water in vapour, and gas in vapour respectively.
- Temperature: ``BTEMP``.
- Bubble point: ``BPBUB``.

The output from ``SUMMARY_D``/``SUMMARY_Z`` can be found in the Eclipse summary files, and also in the :ref:`mass file<mass_file_sec>`. For example, the following: ::

  SUMMARY_D
    BPR   1 1 100
  END_SUMMARY_D

adds a summary ``BPRESSUR`` summary to block ``1 1 100`` in the .UNSMRY file, and the following column to the mass file: ::

  BPressure [bar] 1_1_100

GRID
^^^^^^^^^^^^^^^^^^^^^^^

By default ECLIPSE_FILE makes PFLOTRAN-OGS write the grid in the .EGRID format. By inserting the ``GRID`` card within the ``ECLIPSE_FILE`` block, the grid will be output using the .GRID format rather than .EGRID. See example below: ::

  OUTPUT
    MASS_BALANCE_FILE
     PERIODIC TIMESTEP 1  
    END
    ECLIPSE_FILE
      TIMES y 1 2 3 4 5 6 7
      PERIOD_SUM TIMESTEP 1
      GRID
    END
    LINEREPT
  END

INIT_ALL
^^^^^^^^^^^^^^^^^^^^^^^

This keyword cuases EQLNUM, Kro and Swl values to be writen to the INIT file.
