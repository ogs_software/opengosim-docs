
.. _line-graph-mnemonic:

Line Graph Mnemonic
-------------------------------------

The variable names are mnemonics that help to identify the location and variable types. In
general, a variable name, is made of four components:

* ‘f’, ‘w’, ‘aaq’, ‘r:’ Is field, well, aquifer, region quantity
* ‘o’, ‘g’, ‘w’ or ‘s’ : Is oil, gas, water or solvent quantity
* ‘p’ or ‘i’ : Is production or injection quantity
* ‘r’ or ‘t’ : Is rate or total

These quantities are normally in surface volume terms. So rates are in :math:`sm^3/\mbox{day}` (surface cubic meters) and totals are in :math:`sm^3`. However, some quantities, such as those for the |CO2| trapping analysis, are expressed in mass terms (kg). 

In the mass case, an extra ‘m’ is included in the mnemonic. For example, fgip is the gas in place expressed in :math:`sm^3` (surface cubic meters), and fgmip is the gas mass in place expressed in kg.

The tables below list the output variables. These are in lower cases in the mass files and in capital cases in in the ECLIPSE summary files.

The sign convention for the variable in the tables follows the convention:

* Producer: positive production
* Injector: positive injection


All quantities that can be output for line graph processing are explicitly spelled out below, grouped by field, aquifer and region:

.. contents::
  :backlinks: top
  :depth: 1
  :local:


Field and Well Quantities
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

For the gas component parition (e.g. trapped |CO2|, |CO2| dissolved in brine, etc.):

+-------+---------------------------------------------------------------------+
| Field |                                                                     |       
+=======+=====================================================================+
| fgmip | Field gas mass in place                                             |            
+-------+---------------------------------------------------------------------+
| fgmgp | Field gas mass in the gas phase                                     |
+-------+---------------------------------------------------------------------+
| fgmds | Field gas mass dissolved in the aqueous phase                       |
+-------+---------------------------------------------------------------------+
| fgmtr | Maximum potential gas mass trapped in gas phase                     |
+-------+---------------------------------------------------------------------+
| fgmmo | Field gas mass in the mobile gas phase                              |
+-------+---------------------------------------------------------------------+
| fgmst | Field mass gas stranded in the gas phase (a.k.a residual trapping)  |
+-------+---------------------------------------------------------------------+
| fgmus | Field mass gas unstranded in the gas phase.                         |
+-------+---------------------------------------------------------------------+

Note that these variables are currently available only in the ECLIPSE summary file when running the GAS_WATER mode.

For Water:

+-------+-----------------------------------+-------+--------------------------------------+
| Field |                                   | Well  |                                      |       
+=======+===================================+=======+======================================+
| fwpr  | Field water production rate       | wwpr  | Well water production rate           |            
+-------+-----------------------------------+-------+--------------------------------------+
| fwmpr | Field water mass production rate  | wwmpr | Well water mass production rate      |       
+-------+-----------------------------------+-------+--------------------------------------+
| fwmpt | Field water mass production total | wwmpt | Well water mass production total     |       
+-------+-----------------------------------+-------+--------------------------------------+
| fwir  | Field water injection rate        | wwir  | Well water injection rate            |       
+-------+-----------------------------------+-------+--------------------------------------+
| fwmir | Field water mass injection rate   | wwmir | Well water mass injection rate       |       
+-------+-----------------------------------+-------+--------------------------------------+
| fwit  | Field water injection total       | wwit  | Well water injection total           |       
+-------+-----------------------------------+-------+--------------------------------------+
| fwmit | Field water mass injection total  | wwmit | Well water mass injection total      |       
+-------+-----------------------------------+-------+--------------------------------------+


For Oil:

+-------+-----------------------------------+-------+--------------------------------------+
| Field |                                   | Well  |                                      |       
+=======+===================================+=======+======================================+
| fopr  | Field oil production rate         | wopr  | Well oil production rate             |            
+-------+-----------------------------------+-------+--------------------------------------+
| fompr | Field oil mass production rate    | wompr | Well oil mass production rate        |       
+-------+-----------------------------------+-------+--------------------------------------+
| fompt | Field oil mass production total   | wompt | Well oil mass production total       |       
+-------+-----------------------------------+-------+--------------------------------------+
| foir  | Field oil injection rate          | woir  | Well oil injection rate              |       
+-------+-----------------------------------+-------+--------------------------------------+
| fomir | Field oil mass injection rate     | womir | Well oil mass injection rate         |       
+-------+-----------------------------------+-------+--------------------------------------+
| foit  | Field oil injection total         | woit  | Well oil injection total             |       
+-------+-----------------------------------+-------+--------------------------------------+
| fomit | Field oil mass injection total    | womit | Well oil mass injection total        |       
+-------+-----------------------------------+-------+--------------------------------------+

For Gas:

+-------+-----------------------------------+-------+--------------------------------------+
| Field |                                   | Well  |                                      |       
+=======+===================================+=======+======================================+
| fgpr  | Field gas production rate         | wgpr  | Well gas production rate             |            
+-------+-----------------------------------+-------+--------------------------------------+
| fgmpr | Field gas mass production rate    | wgmpr | Well gas mass production rate        |       
+-------+-----------------------------------+-------+--------------------------------------+
| fgmpt | Field gas mass production total   | wgmpt | Well gas mass production total       |       
+-------+-----------------------------------+-------+--------------------------------------+
| fgir  | Field gas injection rate          | wgir  | Well gas injection rate              |       
+-------+-----------------------------------+-------+--------------------------------------+
| fgmir | Field gas mass injection rate     | wgmir | Well gas mass injection rate         |       
+-------+-----------------------------------+-------+--------------------------------------+
| fgit  | Field gas injection total         | wgit  | Well gas injection total             |       
+-------+-----------------------------------+-------+--------------------------------------+
| fgmit | Field gas mass injection total    | wgmit | Well gas mass injection total        |       
+-------+-----------------------------------+-------+--------------------------------------+

For Solvent:

+-------+--------------------------------------+-------+--------------------------------------+
| Field |                                      | Well  |                                      |       
+=======+======================================+=======+======================================+
| fnpr  | Field solvent production rate        | wnpr  | Well solvent production rate         |            
+-------+--------------------------------------+-------+--------------------------------------+
| fsmpr | Field solvent mass production rate   | wsmpr | Well solvent mass production rate    |       
+-------+--------------------------------------+-------+--------------------------------------+
| fsmpt | Field solvent mass production total  | wsmpt | Well solvent mass production total   |       
+-------+--------------------------------------+-------+--------------------------------------+
| fnir  | Field solvent injection rate         | wnir  | Well solvent injection rate          |       
+-------+--------------------------------------+-------+--------------------------------------+
| fsmir | Field solvent mass injection rate    | wsmir | Well solvent mass injection rate     |       
+-------+--------------------------------------+-------+--------------------------------------+
| fnit  | Field solvent injection total        | wnit  | Well solvent injection total         |       
+-------+--------------------------------------+-------+--------------------------------------+
| fsmit | Field solvent mass injection total   | wsmit | Well solvent mass injection total    |       
+-------+--------------------------------------+-------+--------------------------------------+

Water cut and gas-oil ratio:

+-------+-----------------------------------+-------+--------------------------------------+
| Field |                                   | Well  |                                      |       
+=======+===================================+=======+======================================+
| fgor  | Field production gas-oil ratio    | wgor  | Well production gas-oil ratio        |            
+-------+-----------------------------------+-------+--------------------------------------+
| fwct  | Field production water cut        | wwct  | Well production water cut            |            
+-------+-----------------------------------+-------+--------------------------------------+

Pressure:

+-------+------------------------------------------------------------+-------+--------------------------------------------------+
| Field |                                                            | Well  |                                                  |       
+=======+============================================================+=======+==================================================+
| fpav  | Field pore volume weighted average pressure                | wbhp  | Well bottom hole pressure                        |            
+-------+------------------------------------------------------------+-------+--------------------------------------------------+
| fhpav | Field hydrocarbon pore volume weighted average pressure    |       |                                                  |            
+-------+------------------------------------------------------------+-------+--------------------------------------------------+

Fluid in place:

+-------+---------------------------------------------+-----------+------------------------------------------------------+
| Field |                                             | Initial   |                                                      |       
+=======+=============================================+===========+======================================================+
| foip  | Field oil in place (as surface volume)      | foip0     | Field initial oil in place (as surface volume)       |            
+-------+---------------------------------------------+-----------+------------------------------------------------------+
| fgip  | Field gas in place (as surface volume)      | fgip0     | Field initial gas in place (as surface volume)       |            
+-------+---------------------------------------------+-----------+------------------------------------------------------+
| fwip  | Field water in place (as surface volume)    | fwip0     | Field initial water in place (as surface volume)     |            
+-------+---------------------------------------------+-----------+------------------------------------------------------+
| fsip  | Field solvent in place (as surface volume)  | fsip0     | Field initial solvent in place (as surface volume)   |            
+-------+---------------------------------------------+-----------+------------------------------------------------------+

Energy:

+-------+---------------------------------------------+-----------+------------------------------------------------------+
| Field |                                             | Well      |                                                      |       
+=======+=============================================+===========+======================================================+
| feipo | Initial field energy in place               | weir      | Well energy injection rate                           |            
+-------+---------------------------------------------+-----------+------------------------------------------------------+
| feip  | Field energy in place                       | weit      | Well energy injection total                          |            
+-------+---------------------------------------------+-----------+------------------------------------------------------+
| feir  | Field energy injection rate                 | wepr      | Well energy production rate                          |            
+-------+---------------------------------------------+-----------+------------------------------------------------------+
| feit  | Field energy injection total                | wept      | Well energy production total                         |            
+-------+---------------------------------------------+-----------+------------------------------------------------------+
| fepr  | Field energy production rate                |           |                                                      |            
+-------+---------------------------------------------+-----------+------------------------------------------------------+
| fept  | Field energy production total               |           |                                                      |            
+-------+---------------------------------------------+-----------+------------------------------------------------------+

Reservoir volumes:

+-------+------------------------------------------+-------+-------------------------------------------+
| Field |                                          | Well  |                                           |
+=======+==========================================+=======+===========================================+
| fvpr  | Field reservoir volume production rate   | wvpr  | Well reservoir volume production rate     |
+-------+------------------------------------------+-------+-------------------------------------------+
| fvpt  | Field reservoir volume production total  | wvpt  | Well reservoir volume production total    |
+-------+------------------------------------------+-------+-------------------------------------------+
| fvip  | Field reservoir volume injection rate    | wvir  | Well reservoir volume injection rate      |
+-------+------------------------------------------+-------+-------------------------------------------+
| fvit  | Field reservoir volume injection total   | wvit  | Well reservoir volume injection total     |
+-------+------------------------------------------+-------+-------------------------------------------+

Note that the reservoir volume quantities are currently computed with the well BHPs rather than the field average pressure. 

COMP modes specific mnemonics:

+-------+-------------------------------------------------------------------------------------------------------------------------------------+
| Well  |                                                                                                                                     |
+=======+=====================================================================================================================================+
| wzmfo | Total molar fraction of oil produced/injected      (:ref:`COMP4<multigas_intro_sec>`)                                               |
+-------+-------------------------------------------------------------------------------------------------------------------------------------+
| wzmfg | Total molar fraction of gas produced/injected      (:ref:`COMP3<multigas_comp3_intro_sec>` and  :ref:`COMP4<multigas_intro_sec>`)   |
+-------+-------------------------------------------------------------------------------------------------------------------------------------+
| wzmfs | Total molar fraction of solvent produced/injected  (:ref:`COMP3<multigas_comp3_intro_sec>` and  :ref:`COMP4<multigas_intro_sec>`)   |
+-------+-------------------------------------------------------------------------------------------------------------------------------------+
| wzmf# | Total molar fraction of component number # produced/injected  (:ref:`COMP EOS<comp_intro_sec>`)                                     |
+-------+-------------------------------------------------------------------------------------------------------------------------------------+

Note that for :ref:`COMP3<multigas_comp3_intro_sec>` and :ref:`COMP4<multigas_intro_sec>` the injector is limited to one component. 

Aquifer Quantities
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Aquifer output :

+----------+-------------------------------------------+
| Aquifer  |                                           |
+==========+===========================================+
| aaqgpr   | Aquifer gas production rate               |
+----------+-------------------------------------------+
| aaqgpt   | Aquifer gas production total              |                              
+----------+-------------------------------------------+
| aaqwpr   | Aquifer water production rate             |
+----------+-------------------------------------------+
| aaqwpt   | Aquifer water production total            |
+----------+-------------------------------------------+
| aaqopr   | Aquifer oil production rate               |
+----------+-------------------------------------------+
| aaqopt   | Aquifer oil production total              |
+----------+-------------------------------------------+
| aaqspr   | Aquifer solvent production rate           |
+----------+-------------------------------------------+
| aaqspt   | Aquifer solvent production total          |
+----------+-------------------------------------------+
| aaqp0    | Aquifer initial pressure                  |
+----------+-------------------------------------------+
| aaqp     | Aquifer pressure                          |
+----------+-------------------------------------------+
| aaqz     | Aquifer reservoir volume production rate  | 
+----------+-------------------------------------------+
| aaqt     | Aquifer reservoir volume total            |
+----------+-------------------------------------------+

Aquifer production rates are considered positive for influx from the aquifer to the reservoir.


Region Quantities
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Region and Inter-region quantities:

+----------+-------------------------------------------+
| Region   |                                           |
+==========+===========================================+
| rwip     | Region water in place                     |
+----------+-------------------------------------------+
| rgip     | Region gas in place                       |
+----------+-------------------------------------------+
| roip     | Region oil in place                       |
+----------+-------------------------------------------+
| rsip     | Region solvent in place                   |
+----------+-------------------------------------------+
| rwfr     | Region water flux rate                    |
+----------+-------------------------------------------+
| rwft     | Region water total                        |
+----------+-------------------------------------------+
| rgfr     | Region gas flux rate                      |
+----------+-------------------------------------------+
| rgft     | Region gas total                          |
+----------+-------------------------------------------+
| rofr     | Region oil flux rate                      |
+----------+-------------------------------------------+
| roft     | Region oil total                          |
+----------+-------------------------------------------+
| rsfr     | Region solvent flux rate                  |
+----------+-------------------------------------------+
| rsft     | Region solvent total                      |
+----------+-------------------------------------------+

For the GAS_WATER mode only, the following additional variables are available:

+----------+----------------------------------------------------------------------+
| Region   |                                                                      |
+==========+======================================================================+
| rgmip    | Region gas mass in place                                             |
+----------+----------------------------------------------------------------------+
| rgmgp    | Region gas mass in the gas phase                                     |                   
+----------+----------------------------------------------------------------------+
| rgmds    | Region gas mass dissolved in aqueous phase                           |
+----------+----------------------------------------------------------------------+
| rgmmo    | Region gas mass in the mobile gas phase                              |
+----------+----------------------------------------------------------------------+
| rgmst    | Region gas mass stranded in the gas phase (a.k.a. residual trapping) |
+----------+----------------------------------------------------------------------+
| rgmus    | Region gas mass unstranded in the gas phase                          |
+----------+----------------------------------------------------------------------+

The mnemonic above is used for :ref:`FIPNUM<fip_sec>`. If additional FIP arrays are found (see :ref:`FIPXXX<fip_sec>` for example),
the letters added to FIP for the user-defined name of the array are used to identify the associated
quantities, for example: ``rwfr_xxx``, ``rgfr_xxx``, etc.

The same region mnemonic is used for region and inter-region quantities, for the latter the user must
select in the post-processor the region pair of interest.
