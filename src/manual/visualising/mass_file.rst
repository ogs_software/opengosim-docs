
.. _mass_file_sec:

:index:`MASS_BALANCE_FILE`
-------------------------------------


The MASS_BALANCE_FILE sub-block of OUTPUT requests that the simulation outputs a mass file in ASCII format. See example below: ::

  OUTPUT
    MASS_BALANCE_FILE
      PERIODIC TIMESTEP 1
    END
    ECLIPSE_FILE
      PERIOD_RST TIME 200 d
    END
    LINEREPT
  END

The mass file is named <input_deck_name>-mas.dat, and reports at each specified time, and for each fluid components: production and injection rates, total quantities injected and produced, quantities present in the entire reservoir (field). For fluid component being injected or produced, the information are given for the field but also for each well, source, sink, and boundary condition present in the model.

The file is a ASCII file, so its content can be visualised with any text editor, see example below:

.. figure:: ../images/mbal_example.png
   :scale: 50%

For the explanation of the variable names, please refer to :ref:`Line Graph Mnemonic<line-graph-mnemonic>`.

If, in the MASS_BALANCE_FILE section, the keyword WRITE_MASS_RATES is included, then the rates and totals will be written out in terms of masses as well of surface volumes. The use of WRITE_MASS_RATES is as follows: ::

  MASS_BALANCE_FILE
    PERIODIC TIMESTEP 1
    WRITE_MASS_RATES
  END



