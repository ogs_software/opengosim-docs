
.. _visualising_sec:

Visualising and Analysing Results
===================================

To analyse the results of a simulation case, several output options are available:

.. toctree::
   :maxdepth: 1

   mass_file
   eclipse_files

The mnemonic used by the ECLIPSE Summary file and the mass files is explained in:

.. toctree::
   :maxdepth: 1

   line_graph_mnemonic.rst



For advanced PFLOTRAN users the following options are also available 

 *  :ref:`SNAPSHOT_FILE<snapshot_sec>`
 *  :ref:`OBSERVATION_FILE<variable_output_sec>`



