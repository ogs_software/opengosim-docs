document.addEventListener('DOMContentLoaded', function () {
    const copyIconSvg = `
        <svg aria-hidden="true" data-prefix="far" data-icon="copy" class="svg-inline--fa fa-copy fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" width="16" height="18">
            <path fill="#777" d="M433.941 65.941l-51.882-51.882A48 48 0 0 0 348.118 0H176c-26.51 0-48 21.49-48 48v48H48c-26.51 0-48 21.49-48 48v320c0 26.51 21.49 48 48 48h224c26.51 0 48-21.49 48-48v-48h80c26.51 0 48-21.49 48-48V99.882a48 48 0 0 0-14.059-33.941zM266 464H54a6 6 0 0 1-6-6V150a6 6 0 0 1 6-6h74v224c0 26.51 21.49 48 48 48h96v42a6 6 0 0 1-6 6zm128-96H182a6 6 0 0 1-6-6V54a6 6 0 0 1 6-6h106v88c0 13.255 10.745 24 24 24h88v202a6 6 0 0 1-6 6zm6-256h-64V48h9.632c1.591 0 3.117.632 4.243 1.757l48.368 48.368a6 6 0 0 1 1.757 4.243V112z"></path>
        </svg>`;

    const successIconSvg = `
        <svg aria-hidden="true" data-prefix="far" data-icon="check" class="svg-inline--fa fa-check fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" width="16" height="18">
            <path fill="#4caf50" d="M173.898 439.404l-166.4-166.4c-9.6-9.6-9.6-25.6 0-35.2l35.2-35.2c9.6-9.6 25.6-9.6 35.2 0l96 96 241.6-241.6c9.6-9.6 25.6-9.6 35.2 0l35.2 35.2c9.6 9.6 9.6 25.6 0 35.2l-277.6 277.6c-9.6 9.6-25.6 9.6-35.2 0z"></path>
        </svg>`;

    document.querySelectorAll('div.highlight').forEach(function (codeBlock) {
        if (!codeBlock.querySelector('.copy-button')) {
            let button = document.createElement('button');
            button.className = 'copy-button';

            let tooltip = document.createElement('span');
            tooltip.className = 'tooltip';
            tooltip.textContent = 'Click to copy';  // Initial tooltip text
            button.appendChild(tooltip);  // Append the tooltip to the button

            let icon = document.createElement('div');
            icon.innerHTML = copyIconSvg;
            button.appendChild(icon);

            codeBlock.style.position = 'relative';
            button.style.position = 'absolute';
            button.style.top = '5px';
            button.style.right = '5px';

            codeBlock.appendChild(button);

            button.addEventListener('click', function () {
                let code = codeBlock.querySelector('pre').textContent;
                navigator.clipboard.writeText(code).then(() => {
                    icon.innerHTML = successIconSvg;
                    tooltip.textContent = 'Copied!';

                    setTimeout(() => {
                        icon.innerHTML = copyIconSvg;
                        tooltip.textContent = 'Click to copy';
                    }, 5000);
                });
            });
        }
    });
});
