
.. _advanced_amg_cpr_chap:

Advanced AMG/CPR Configuration
==============================

Currently the CPR_OPTIONS card serves one purpose – to configure the AMG solver used as
stage one of the two stage CPR preconditioner. This is the BOOMERAMG solver provided by
the HYPRE package. A great number of configuration options for this are available, see for
example the HYPRE technical manual if interested. The CPR_OPTIONS subcard and its
subcard CPR_HYPRE_OPTIONS provide easy access to some of the most important of
these AMG settings.

First we note that the CPR preconditioner in PFLOTRAN sets some parameters in
BOOMERAMG automatically, so that the default AMG configuration is not as described in the
HYPRE manual. The settings are as follows:

 * Coarsening type is set to PMIS

 * Interpolation type is set to EXT+I

 * Relax type (all) is set to Jacobi

 * The strong coupling threshold is set to 0.5.

This is the reason for the first CPR keyword:

 *  CPR_MANUAL_AMG_CONFIG: disables the automatic setting of AMG parameters as described above. Instead the default configuration of BOOMERAMG is used. You must use this keyword if it is desired to change any of the: coarsening type, interpolation type, smoother type, or strong threshold of the AMG part of the CPR preconditioner (see CPR_HYPRE_OPTIONS for how to do this), since any changes will be overridden otherwise.
 *  CPRAMGREPORT: causes detailed debugging information to be output from the AMG solver part of the CPR preconditioner. This is useful for confirming which AMG settings are being used. Not recommended for production runs.


CPR_HYPRE_OPTIONS
--------------------------------

The CPR_HYPRE_OPTIONS card collects keywords specifically related to the settings for the
HYPRE BOOMERAMG AMG solver part of the CPR, as follows: ::

  CPR HYPRE_OPTIONS
    BOOMERAMG_COARSEN_TYPE <amg_coarsen_type>: [Falgout, PMIS, etc.]
    BOOMERAMG_INTERPOLATION_TYPE <amg_interpolation_type>: [ext+i, classical, etc.]
    BOOMERAMG_RELAX_TYPE_ALL <amg_relax_type>: [Jacobi, symmetric-SOR/Jacobi etc.]
    BOOMERAMG_STRONG_THRESHOLD: <amg_strong_threshold>
    BOOMERAMG_AGG_NL: <amg_aggressive_coarsening_levels>
  END

Where:

* amg_coarsen_type: The coarsening method used to generate the algebraic 'coarse grid’ in the AMG solver. Possible values are [``CLJP``, ``Ruge-Steuben``, ``moddified Ruge-steuben``, ``Falgout``, ``PMIS``, ``HMIS``]. This is an exhaustive list of options available through PETSc/Hypre, note that ``PMIS`` and ``Falgout`` have been most extensively tested in the context of our CPR implementation and ``PMIS`` is recommended. Default is ``PMIS``, or ``Falgout`` if ``CPR_MANUAL_AMG_CONFIG`` is used.
* amg_interpolation_type: The interpolation method used to relate solutions on algebraically coarse grids to finer ones in the AMG solver. Possible values are [``classical``, ``direct``, ``multipass``, ``multipass-wts``, ``ext+i``, ``ext+i-cc``, ``standard``, ``standard-wts``, ``block``, ``block-wtd``, ``FF``, ``FF1``]. This is an exhaustive list of options available through PETSc/Hypre, note that ``ext+i`` and ``classical`` have been most extensively tested in the context of our CPR implementation and ext+i is recommended. Default is ``EXT+I``, or ``classical`` if ``CPR_MANUAL_AMG_CONFIG`` is used.
* amg_relax_type: The classical solver (i.e. relaxer) type used as an approximate solver on each level of the AMG solver. possible values are [``Jacobi``, ``sequential-Gauss-Seidel``, ``seqboundary-Gauss-Seidel``, ``SOR/Jacobi``, ``backward-SOR/Jacobi``, ``symmetric-SOR/Jacobi`` ``l1scaled-SOR/Jacobi``, ``Gaussian-elimination``, ``CG``, ``Chebyshev``, ``FCF-Jacobi``, ``l1scaled-Jacobi``]. This is an exhaustive list of options available through PETSc/Hypre, note that ``Jacobi`` and ``symmetric-SOR/Jacobi`` have been most extensively tested in the context of our CPR implementation and ``Jacobi`` is recommended. Default is ``Jacobi``, or ``symmetric-SOR/Jacobi`` if ``CPR_MANUAL_AMG_CONFIG`` is used.
* amg_strong_threshold: float between 0 and 1. Thresholding parameter used in the coarsening algorithm of the AMG solver. Default is 0.5, or 0.25 if ``CPR_MANUAL_AMG_CONFIG`` is used.
* amg_aggressive_coarseing_levels: integer. On how many levels to apply aggressive coarsening on the AMG solver. Default is 0.

Below is a more complex example: ::

  LINEAR_SOLVER FLOW
    ATOL 1D-8
    RTOL 1D-8
    MAXIT 50
    KSP_TYPE FGMRES
    GMRES_RESTART 50
    GMRES_MODIFIED_GS
    PC_TYPE CPR
    CPR_OPTIONS
      CPR_HYPRE_OPTIONS
        BOOMERAMG_AGG_NL 1
      END
    END
  END




