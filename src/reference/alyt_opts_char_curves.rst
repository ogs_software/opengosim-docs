.. _alyt_cc_opts_sec:

Analytical Options for CHARACTERISTIC_CURVES
=============================================================

The following options are available to define saturation curves by analytical functions in CHARACTERISTIC_CURVES:

.. contents::
  :backlinks: top
  :depth: 2
  :local:


Some of the analytic characteristic curves have an option to enable a smoother algorithm that acts on the end points improving the robustness of the numerical solution. This can results in faster convergence thanks to a modification of the original function close to the end points (and thus loss of accuracy). For more details see the :ref:`smoother section<char_curves_smoother>` in the theory manual. The SMOOTH option availability is commented in the explanation of the individual analytic function.



CAP_PRESSURE_FUNCTION_OW (or PC_OW)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. _pc_ow_constant_sec:

PC_OW CONSTANT
------------------------------
::

  CAP_PRESSURE_FUNCTION_OW (or PC_OW) CONSTANT
    CONSTANT_PRESSURE <constant_ow_cap_pres_value_value> [Pa]
  END

<constant_ow_cap_pres_value_value>: specifies the constant capillary pressure value
between oil and water. The user must enter this value of the software will return an error.

.. _pc_ow_vg_sec:

PC_OW VAN_GENUCHTEN
---------------------------------------
::

  CAP_PRESSURE_FUNCTION_OW (or PC_OW) VAN_GENUCHTEN
    ALPHA <alpha_value> [1/Pa]
    M <m_value>
    WATER_CONNATE_SATURATION <wat_connate_saturation_value>
    WATER_RESIDUAL_SATURATION <wat_residual_saturation_value>
    MAX_CAPILLARY_PRESSURE <pc_max_value> [Pa]
  END

.. math::
  P_{ cow} (S_w )=P_{ cc} \left( S_e^{−1/m} −1 \right)^{1/n},

where:

.. math::
  \begin{split}
  &n &= \frac{1}{1-m} \\
  &S_e &= \frac{S_w - S_{wcr}}{1- S_{wcr}} \\
  &P_{cc} &= \frac{1}{ \alpha} \mbox{ is the critical capillary pressure} \\
  \end{split}

and

.. math::
  \begin{split}
  &S_w &\mbox{ is the water saturation} \\
  &S_{ wcr} &\mbox{ is the water residual saturation} \\
  &S_{ wco} &\mbox{ is the water connate saturation} \\
  \end{split}

The capillary pressure is capped to a maximum pressure value :math:`P_{c,Max}`
for water saturations that tend to the water connate saturation.

:math:`P_{c,Max}`, :math:`\alpha`, :math:`m`, :math:`S_{ wcr}` and :math:`S_{ wco}` are entered by the user to define the shape and end points of the function. If not entered the software returns an error. The water residual saturation must be greater or equal to the water connate saturation.


.. _pc_ow_bc_sec:

PC_OW BROOKS_COREY
---------------------------------------
::

  CAP_PRESSURE_FUNCTION_OW (or PC_OW) BROOKS_COREY
    ALPHA <alpha_value> [1/Pa]
    LAMBDA <lambda_value>
    WATER_CONNATE_SATURATION <wat_connate_saturation_value>
    WATER_RESIDUAL_SATURATION <wat_residual_saturation_value>
    MAX_CAPILLARY_PRESSURE <pc_max_value> [Pa]
    SMOOTH: optional
  END

.. math::
  P_{ cow} (S_w )=P_{ cc} S_e^{-1/\lambda},


where:

.. math::
  \begin{split}
  &S_e &= \frac{S_w - S_{wcr}}{1- S_{wcr}} \\
  &P_{cc} &= \frac{1}{ \alpha} \mbox{ is the critical capillary pressure} \\
  \end{split}

and

.. math::
  \begin{split}
  &S_w &\mbox{ is the water saturation} \\
  &S_{ wcr} &\mbox{ is the water residual saturation} \\
  &S_{ wco} &\mbox{ is the water connate saturation} \\
  \end{split}

The capillary pressure is capped to a maximum pressure value :math:`P_{ c,Max}` for water saturations that tend to the water residual saturation.

:math:`P_{ c,Max}`, :math:`\alpha` , :math:`\lambda` , :math:`S_{ wcr}` and :math:`S_{ wco}` are entered by the user to define the shape and end points of the function. If these values are not entered the software returns an error. The water residual saturation must be greater or equal to the water connate saturation.

CAP_PRESSURE_FUNCTION_OG (or PC_OG)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. _pc_og_const_sec:

PC_OG CONSTANT
-----------------------------
::

  CAP_PRESSURE_FUNCTION_OG (or PC_OG) CONSTANT
    CONSTANT_PRESSURE <constant_og_cap_pres_value_value> [Pa]
  END

<constant_og_cap_pres_value_value>: specifies the constant capillary pressure value
between oil and gas. The user must enter this value of the software will returns an error.

.. _pc_og_vg_sec:

PC_OG VAN_GENUCHTEN_SL
---------------------------------------------
::

  CAP_PRESSURE_FUNCTION_OG (or PC_OG) VAN_GENUCHTEN_SL
    ALPHA <alpha_value> [1/Pa]
    M <m_value>
    LIQUID_RESIDUAL_SATURATION <liq_residual_saturation_value>
    MAX_CAPILLARY_PRESSURE <pc_max_value> [Pa]
  END

.. math::
  P_{ cog} (S_l )=P_{ cc} \left( S_e^{-1/m} -1 \right)^{1/n},

where:

.. math::
  \begin{split}
  &n &= \frac{1}{1-m} \\
  &S_e &= \frac{S_l - S_{lcr}}{1- S_{lcr}} \\
  &P_{cc} &= \frac{1}{ \alpha} \mbox{ is the critical capillary pressure} \\
  \end{split}

and

.. math::
  \begin{split}
  &S_l &\mbox{ is the liquid (oil + water) saturation} \\
  &S_{ lcr} &\mbox{ is the liquid (oil + water) residual saturation} \\
  \end{split}

The capillary pressure is capped to a maximum pressure value :math:`P_{c,Max}` for water saturations that tend to the water connate saturation.

:math:`P_{c,Max}`, :math:`\alpha`, :math:`m` and :math:`S_{ lcr}` are entered by the user to define the shape and end points of the function. If not entered the software returns an error.

PERMEABILITY_FUNCTION_WAT (or KRW)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. _krw_mbc_sec:

KRW MOD_BROOKS_COREY
-------------------------------------------------
::

  PERMEABILITY_FUNCTION_WAT (or KRW) MOD_BROOKS_COREY
    M <m_value>
    WATER_CONNATE_SATURATION <wat_connate_saturation_value>
    WATER_RESIDUAL_SATURATION <wat_residual_saturation_value>
    MAX_RELATIVE_PERM (or MAX_REL_PERM) <max_rel_perm> (default is 1)
    SMOOTH (can be included or not, optional)
  END

This option implements the modified Brook and Corey function described by the following
expression:

.. math:: 
  k_{ rw} (S_{ w} )=k_{ rw,Max} S^m_e,

where:

.. math::
  \begin{split}
  &S_e &= \frac{S_w - S_{wcr}}{1- S_{wcr} -S_{owcr}   } \\
  \end{split}

and

.. math::
  \begin{split}
  &S_w &\mbox{ is the water saturation} \\
  &S_{wco} &\mbox{ is the water connate saturation} \\
  &S_{wcr} &\mbox{ is the water residual saturation} \\
  &S_{owcr} &\mbox{ is the oil critical saturatoin for oil in water} \\
  &  k_{ rw,Max} &\mbox{ is the water maximum relative permeability} 
  \end{split}

For the water maximum relative permeability, the value is between 0 and 1, and defaults to 1.

:math:`S_{ wco}`, :math:`S_{ wcr}`, :math:`m` and :math:`k_{ rw,Max}` are entered by the user to define the shape and the end point of the function. :math:`S_{ wco}`, :math:`S_{ wcr}`, :math:`m` must be entered by the user or the software will
return an error. If not entered by the user, :math:`k_{ rw,Max}` defaults to 1. :math:`S_{owcr}` is taken from the definition of the relative permeability of the oil in water.



.. _krw_mvg_sec:

KRW MUALEM_VG
-------------------------------------
::

  PERMEABILITY_FUNCTION_WAT (or KRW) MUALEM_VG
    M <m_value>
    WATER_CONNATE_SATURATION <water_connate_saturation_value>
    WATER_RESIDUAL_SATURATION <water_residual_saturation_value> [-
  END

This option implements the Mualem and Van Genuchten model described by the following
expression:

.. math::
  k_{ rw} (S_{ w })=  S_e^{1/2} \left[ 1− ( 1− \left(1- S_e^{1/m} \right)^m  \right]^2,


where:

.. math::
  \begin{split}
  &S_e &= \frac{S_w - S_{wcr}}{1- S_{wcr}  } \\
  \end{split}

and

.. math::
  \begin{split}
  &S_w &\mbox{ is the water saturation} \\
  &S_{wcr} &\mbox{ is the water residual saturation} \\
  &S_{wco} &\mbox{ is the water connate saturation} 
  \end{split}

:math:`m`, :math:`S_{ wcr}` and :math:`S_{ wco}` are entered by the user to define the shape and end points of the function, if not entered the software returns an error. The watter residual saturation must be greater or equal to the water connate saturation.

.. _krw_burdine_vg_sec:

KRW BURDINE_VG
----------------------------------------
::

  PERMEABILITY_FUNCTION_WAT (or KRW) BURDINE_VG
    M <m_value>
    WATER_CONNATE_SATURATION <water_connate_saturation_value>
    WATER_RESIDUAL_SATURATION <water_residual_saturation_value>
  END

This option implements the Burdine and Van Genuchten model described by the following
expression:

.. math::
  k_{ rw} (S_{ w })=  S_e^{2} \left[ 1− ( 1− \left(1- S_e^{1/m} \right)^m  \right],

where:

.. math::
  \begin{split}
  &S_e &= \frac{S_w - S_{wcr}}{1- S_{wcr}  } \\
  \end{split}

and

.. math::
  \begin{split}
  &S_w &\mbox{ is the water saturation} \\
  &S_{wcr} &\mbox{ is the water residual saturation} \\
  &S_{wco} &\mbox{ is the water connate saturation} 
  \end{split}

:math:`m`, :math:`S_{ wcr}` and :math:`S_{ wco}` are entered by the user to define the shape and end points of the function, if not entered the software returns an error. The watter residual saturation must be greater or equal to the water connate saturation.

.. _krw_burdine_bc_sec:

KRW BURDINE_BC
-----------------------------------
::

  PERMEABILITY_FUNCTION_WAT (or KRW) BURDINE_BC
    LAMBDA <lambda_value>
    WATER_CONNATE_SATURATION <water_connate_saturation_value>
    WATER_RESIDUAL_SATURATION <water_residual_saturation_value>
    SMOOTH (can be included or not optional)
  END


.. math::
  k_{ rw} (S_{ w })=  S_e^{ 3 + \frac{2}{\lambda}  },

where:

.. math::
  \begin{split}
  &S_e &= \frac{S_w - S_{wcr}}{1- S_{wcr}  } \\
  \end{split}

and

.. math::
  \begin{split}
  &S_w &\mbox{ is the water saturation} \\
  &S_{wcr} &\mbox{ is the water residual saturation} \\
  &S_{wco} &\mbox{ is the water connate saturation} 
  \end{split}

:math:`\lambda`, :math:`S_{ wcr}` and :math:`S_{ wco}` are entered by the user to define the shape and end points of the function, if not entered the software returns an error. The watter residual saturation must be greater or equal to the water connate saturation.


PERMEABILITY_FUNCTION_GAS (or KRG)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. _krg_mbc_sec:

KRG MOD_BROOKS_COREY
--------------------------------------------
::

  PERMEABILITY_FUNCTION_GAS (or KRG) MOD_BROOKS_COREY
    M <m_value>
    GAS_CONNATE_SATURATION <gas_connate_saturation_value>
    GAS_RESIDUAL_SATURATION <gas_residual_saturation_value>
    MAX_RELATIVE_PERM (or MAX_REL_PERM) <max_rel_perm> (default is 1)
    SMOOTH (can be included or not, optional)
  END

This option implements the modified Brook and Corey function described by the following
expression:

.. math::
  k_{ rw} (S_{ g })=  k_{rg,Max} S_e^m,

where:

.. math::
  \begin{split}
  &S_e &= \frac{S_g - S_{gcr}}{1- S_{gcr} - S_{ogcr} - S_{wco}} \\
  \end{split}

and

.. math::
  \begin{split}
  &S_g &\mbox{ is the gas saturation} \\
  &S_{gcr} &\mbox{ is the gas residual saturation} \\
  &S_{ogcr} &\mbox{ is the oil critical saturation for oil in gas} \\
  &S_{wco} &\mbox{ is the water connate saturation}  \\
  &  k_{ rg,Max} &\mbox{ is the gas maximum relative permeability} 
  \end{split}

For the gas maximum relative permeability, the value is between 0 and 1, and defaults to 1.

:math:`S_{ wco}`, :math:`S_{ wgcr}`, :math:`m` and :math:`k_{ rg,Max}` are entered by the user to define the shape and the end points of the function. :math:`S_{ wco}`, :math:`S_{ gcr}` and :math:`m` must be entered by the user or the software will return an error. If not entered by the user, :math:`k_{ rg,Max}` defaults to 1.

:math:`S_{ ogcr}` is taken from the definition of the relative permeability of the oil in gas.

.. _krg_mvg_sl_sec:

KRG MUALEM_VG_SL
-----------------------
::

  PERMEABILITY_FUNCTION_GAS (or KRG) MUALEM_VG_SL
    M <m_value>
    GAS_CONNATE_SATURATION <gas_connate_saturation_value>
    GAS_RESIDUAL_SATURATION <gas_residual_saturation_value>
    LIQUID_RESIDUAL_SATURATION <liquid_residual_saturation_value>
  END

This option implements the Mualem and Van Genuchten model, where the gas relative
permeability is computed as function of the liquid (oil + water) saturation:

.. math::
  k_{ rg} (S_{ l })= S_{eg}^{1/2} \left( 1 - S_e^{1/m}   \right)^{2m},

where:

.. math::
  \begin{split}
  &S_e &= \frac{S_l - S_{lcr}}{1- S_{lcr} - S_{gcr} } \\
  &S_{eg} &= 1 - S_e \\
  \end{split}

and

.. math::
  \begin{split}
  &S_l &\mbox{ is the liquid (oil + water) saturation} \\
  &S_{lcr} &\mbox{ is the liquid (oil + water) residual saturation} \\
  &S_{gco} &\mbox{ is the gas connate saturation} \\
  &S_{gcr} &\mbox{ is the gas residual saturation}  \\
  \end{split}

:math:`m`, :math:`S_{ lcr}` , :math:`S_{ gco}` and :math:`S_{ gcr}` are entered by the user to define the shape and end points of the function, if not the software returns an error. The gas residual saturation must be greater or equal than the gas connate saturation. The gas connate saturation is typically zero.


.. _krg_burd_vg_sl_sec:

KRG BURDINE_VG_SL
-----------------------------------------
:: 

  PERMEABILITY_FUNCTION_GAS (or KRG) BURDINE_VG_SL
    M <m_value>
    GAS_CONNATE_SATURATION <gas_connate_saturation_value>
    GAS_RESIDUAL_SATURATION <gas_residual_saturation_value>
    LIQUID_RESIDUAL_SATURATION <liquid_residual_saturation_value>
  END

This option implements the Burdine and Van Genuchten model, where Krg is function of the
liquid saturation:

.. math::
  k_{ rw} (S_{ l })= S_{eg}^{2} \left( 1 - S_e^{1/m}   \right)^{m},

where:

.. math::
  \begin{split}
  &S_e &= \frac{S_l - S_{lcr}}{1- S_{lcr} - S_{gcr} } \\
  &S_{eg} &= 1 - S_e \\
  \end{split}

and

.. math::
  \begin{split}
  &S_l &\mbox{ is the liquid (oil + water) saturation} \\
  &S_{lcr} &\mbox{ is the liquid (oil + water) residual saturation} \\
  &S_{gco} &\mbox{ is the gas connate saturation} \\
  &S_{gcr} &\mbox{ is the gas residual saturation}  \\
  \end{split}

:math:`m`, :math:`S_{ lcr}` , :math:`S_{ gco}` and :math:`S_{ gcr}` are entered by the user to define the shape and end points of the function, if not the software returns an error. The gas residual saturation must be greater or equal than the gas connate saturation. The gas connate saturation is typically zero.



.. _krg_burd_bc_sl_sec:

KRG BURDINE_BC_SL
---------------------------------------------
::

  PERMEABILITY_FUNCTION_GAS (or KRG) BURDINE_BC_SL
    LAMBDA <lambda_value>
    GAS_CONNATE_SATURATION <gas_connate_saturation_value>
    GAS_RESIDUAL_SATURATION <gas_residual_saturation_value>
    LIQUID_RESIDUAL_SATURATION <liquid_residual_saturation_value>
    SMOOTH (can be included or not optional)
  END

This option implements the Burdine and Brooks and Corey model, where Krg is function of
the liquid saturation:

.. math::
  k_{ rg} (S_{ g })= S_{eg}^{2} \left( 1 - S_e^{1 + \frac{1}{\lambda}}   \right),

where:

.. math::
  \begin{split}
  &S_e &= \frac{S_l - S_{lcr}}{1- S_{lcr} - S_{gcr} } \\
  &S_{eg} &= 1 - S_e \\
  \end{split}

and

.. math::
  \begin{split}
  &S_l &\mbox{ is the liquid (oil + water) saturation} \\
  &S_{lcr} &\mbox{ is the liquid (oil + water) residual saturation} \\
  &S_{gco} &\mbox{ is the gas connate saturation} \\
  &S_{gcr} &\mbox{ is the gas residual saturation}  \\
  \end{split}


:math:`\lambda`, :math:`S_{ lcr}` , :math:`S_{ gco}` and :math:`S_{ gcr}` are entered by the user to define the shape and end points of the function, if not the software returns an error. The gas residual saturation must be greater or equal than the gas connate saturation. The gas connate saturation is typically zero.


PERMEABILITY_FUNCTION_OIL (or KRO)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

KRO ECLIPSE
-------------------------------


Requires as sub-cards a ``PERMEABILITY_FUNCTION_OW`` card and a ``PERMEABILITY_FUNCTION_OG`` card. From these arbitrary oil-water relative permeability and oil-gas relative permeability models, an oil relative permeability (``KRO``) is synthesised in using the :ref:`Eclipse Model<rpm-3ph-section>`.

See example below: ::

  PERMEABILITY_FUNCTION_OIL ECLIPSE
   PERMEABILITY_FUNCTION_OW MOD_BROOKS_COREY
    M 2.0
    OIL_RESIDUAL_SATURATION 0.0d0
   END
   PERMEABILITY_FUNCTION_OG MOD_BROOKS_COREY
    M 2.0
    OIL_RESIDUAL_SATURATION 0.0d0
   END
  END

The software will run a check to ensure that the end points of the analytical curves for :math:`K_{row}` and :math:`K_{rog}` are consistent: the oil in water and oil in gas relative permeability must have the same value for the oil saturation maximum value, if not it will return an error.

PERMEABILITY_FUNCTION_OW (or KROW)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. _krow_mbc_sec:

KROW MOD_BROOKS_COREY
---------------------------------------------
::

  PERMEABILITY_FUNCTION_OW (or KROW) MOD_BROOKS_COREY
    M <m_value>
    OIL_RESIDUAL_SATURATION <ow_residual_saturation_value>
    MAX_RELATIVE_PERM (or MAX_REL_PERM) <max_rel_perm> (default is 1)
    SMOOTH (can be included or not, optional)
  END

This option implements the modified Brooks and Corey function described by the following
expression:

.. math::
  k_{ row} ( S_{ o} )=k_{ row,Max} S^ m_e

where: 

.. math::
  \begin{split}
  &S_e &= \frac{S_o - S_{owcr}}{1- S_{wr} - S_{owcr} } \\
  \end{split}

and

.. math::
  \begin{split}
  &S_o &\mbox{ is the oil saturation} \\
  &S_{owcr} &\mbox{ is the oil critical saturation for oil in water} \\
  &S_{wcr} &\mbox{ is the water residual saturation} \\
  &k_{ row,Max} &\mbox{ is the water maximum relative permeability} \\
  \end{split}

for the water maximum relative permeability :math:`k_{ row,Max}`, the value is between 0 and 1 and defaults to 1.

:math:`S_{ owcr}` , :math:`m` and :math:`k_{ row,Max}` are entered by the user to define the shape and the end points of the function. :math:`S_{ owcr}` and :math:`m` must be entered by the user or the software will return an error. If not entered by the user, :math:`k_{ row,Max}` defaults to 1. 

:math:`S_{ wcr}` is taken from the definition of the water relative permeability. The oil connate saturation is zero (an oil wet systems cannot currently be modeled).

.. _krow_tough2_lin_sec:

KROW TOUGH2_LINEAR
-----------------------------------------
::

  PERMEABILITY_FUNCTION_OW (or KROW) TOUGH2_LINEAR_OIL
    OIL_RESIDUAL_SATURATION <oil_sat_res_value>
  /

This option implements a linear oil relative permeability function, taken from the TOUGH2-
EOS8 module. See mathematical expression below:

.. math::
  k_{ row} ( S_{ o} )=S_{ eo}

where: 

.. math::
  \begin{split}
  &S_{eo} &= \frac{S_o - S_{owcr}}{1-  S_{owcr} } \\
  \end{split}

and

.. math::
  \begin{split}
  &S_o &\mbox{ is the oil saturation} \\
  &S_{owcr} &\mbox{ is the oil residua saturation for oil in water}
  \end{split}

:math:`S_{ owcr}` is entered by the user, or the software returns an error.


PERMEABILITY_FUNCTION_HC (or KRH)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. _krh_mbc_sec:

KRH MOD_BROOKS_COREY
------------------------------------------
::

  PERMEABILITY FUNCTION_HYDROCARBON (or KRH) MOD_BROOKS_COREY
    M <m_value>
    HYDROCARBON_RESIDUAL_SATURATION <hw_residual_saturation_value>
    MAX_RELATIVE_PERM (or MAX_REL_PERM) <max_rel_perm> (default is 1)
    SMOOTH (can be included or not, optional)
  END

This is completely analogous to :ref:`KROW MOD_BROOKS_COREY<krow_mbc_sec>`, except that when used in a TODD_LONGSTAFF run, it applies to the hydrocarbon :math:`(S_h =S_o +S_s )` and not just the oil saturation. The expression used is the same as for ``KROW`` above, with :math:`S_h` instead of :math:`S_o`.



.. _krh_tough2_lin_sec:

KRH TOUGH2_LINEAR
--------------------------------------
::

  PERMEABILITY_FUNCTION_OW (or KROW) TOUGH2_LINEAR_OIL
    HYDROCARBON_RESIDUAL_SATURATION <hydrocarbon_sat_res_value>
  /

This is completely analogous to ``KROW,`` except that when used in a TODD_LONGSTAFF run,
it applies to the hydrocarbon (:math:`S_h =S_o +S_s`) and not just the oil saturation. The expression used is the same as for ``KROW`` above, with :math:`S_h` instead of :math:`S_o` .


PERMEABILITY_FUNCTION_OG (or KROG)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. _krog_mbc_sec:

KROG MOD_BROOKS_COREY
---------------------------------------------
::

  PERMEABILITY_FUNCTION_OG (or KROG) MOD_BROOKS_COREY
    M <m_value>
    OIL_RESIDUAL_SATURATION <og_residual_saturation_value>
    MAX_RELATIVE_PERM (or MAX_REL_PERM) <max_rel_perm> (default is 1)
    SMOOTH (can be included or not, optional)
  END

This option implements the modified Brook and Corey function for oil in gas:

.. math::
  k_{ rog} ( S_{ o })=k_{rog,Max} S_e^m, 

where: 

.. math::
  \begin{split}
  &S_{eo} &= \frac{S_o - S_{ogcr}}{1-  S_{ogcr} -  S_{gcr}-  S_{wco}  } \\
  \end{split}

and

.. math::
  \begin{split}
  &S_o &\mbox{ is the oil saturation} \\
  &S_{ogcr} &\mbox{ is the oil critical saturation for oil in gas} \\
  &S_{gcr} &\mbox{ is the gas residual saturation} \\
  &S_{wco} &\mbox{ is the water connate saturation} \\
  & k_{rog,Max} &\mbox{ is the maximum relative permeability of oil in gas}
  \end{split}

For the maximum relative permeability of oil in gas :math:`k_{rog,Max}`, the value is between 0 and 1.

:math:`S_{ ogcr}`, :math:`m` and :math:`k_{rowMax}` are entered by the user to define the shape and the end point of the function. :math:`S_{ ogcr}` and :math:`m` must be entered by the user or the software will return an error. If not entered by the user, :math:`k_{rog,Max}` defaults to 1.

:math:`S_{ wco}` is taken from the definition of the water relative permeability.

:math:`S_{ gcr}` is taken form the definition of the gas residual saturation. 

The oil connate saturation is zero (oil wet system cannot currently be modeled).



