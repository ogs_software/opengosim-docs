
.. _bc_ref_sec:

BOUNDARY_CONDITION
==========================

This block links a particular :ref:`FLOW_CONDITION<flow_cond_sec>` to the appropriate :ref:`REGION<region_sec>` to define an initial condition. It is set up similar to the :ref:`INITIAL_CONDITION<init_cond_sec>` block, by first creating a list of flow conditions, and then applying them to
appropriate REGIONs using this block. ::

  BOUNDARY_CONDITION <boundary_condition_name>
    FLOW_CONDITION <flow_condition_name>
    REGION <region_name>
  /

Where:

* <boundary_condition_name>: name of the boundary condition.
* <flow_condition_name>: name a flow condition defined in the input deck that will be applied to the region specified in the boundary condition.
* <region_name>: name of a region defined in the input deck where the flow condition specified in the boundary condition will be applied.

For example, this block can be used with an hydrostatic condition to set up an open boundary, see below: ::

  BOUNDARY_CONDITION west_open_boundary
    FLOW_CONDITION hydrostatic_press !hydrostatic condition
    REGION west_side
  /
