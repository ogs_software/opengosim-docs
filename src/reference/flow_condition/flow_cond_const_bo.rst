
BLACK_OIL flow condition to initialize a study with constant pressure and saturations
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

In the black oil model the solution variables are: oil pressure, oil saturation, gas
saturation/bubble point and temperature.

The flow condition can fix the initial state of the oil to be saturated or unsaturated with gas.
This state is automatically determined based on the variable used to define the flow
condition.

The initial state is defined as unsaturated oil if the flow condition fixes values for the oil
pressure, the oil saturation and the bubble point. See example below: ::

  FLOW_CONDITION initial_press
    TYPE
      OIL_PRESSURE dirichlet
      OIL_SATURATION dirichlet
      TEMPERATURE dirichlet
      BUBBLE_POINT dirichlet
    /
    OIL_PRESSURE 200.0 Bar
    OIL_SATURATION 1.0d0
    TEMPERATURE 15.0d0 C
    BUBBLE_POINT 100.0 Bar
  /

The initial state is defined as saturated oil if the flow condition fixes values for the oil
pressure, the oil saturation and gas saturation. See example below: ::

  FLOW_CONDITION initial_press
    TYPE
      OIL_PRESSURE dirichlet
      OIL_SATURATION dirichlet
      GAS_SATURATION dirichlet
      TEMPERATURE dirichlet
    /
    OIL_PRESSURE 200 Bar
    OIL_SATURATION 0.83d0
    GAS_SATURATION 0.1d0
    TEMPERATURE 15.0d0 C
  /

