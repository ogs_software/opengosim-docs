
GAS-WATER: flow condition to initialize a study with constant pressure and saturations
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

In the Gas-Water model there are three possible states in any region:

1) Undersaturated water: only the aqueous phase is present.
2) Undersaturated gas: Vapour phase only present.
3) Saturated: both phases are present.

For a region in the undersaturated water state, you must specify the pressure of the
aqueous phase, and the mole fraction concentration of the dissolved gas as in the following
example: ::

  FLOW_CONDITION initial
    TYPE
      LIQUID_PRESSURE dirichlet
      GAS_IN_LIQUID_MOLE_FRACTION dirichlet
      TEMPERATURE dirichlet
    END
    LIQUID_PRESSURE 1.20D7 Pa
    GAS_IN_LIQUID_MOLE_FRACTION 1.D-6
    TEMPERATURE 45.0d0 C
  END

Note that the mole fraction does not include the salt in the brine - this will be moles of |CO2|/ (moles of water+moles of |CO2|)

For a region in the unsaturated vapour state, you must specify the pressure of the vapour phase, and the mole fraction concentration of the water vapour as in the following example: ::

  FLOW_CONDITION initial
    TYPE
      GAS_PRESSURE dirichlet
      WATER_IN_VAPOR_MOLE_FRACTION dirichlet
      TEMPERATURE dirichlet
    END
    GAS_PRESSURE 1.20D7 Pa
    WATER_IN_VAPOR_MOLE_FRACTION 1.D-5
    TEMPERATURE 45.0d0 C
  END

For a region in the saturated state, you must specify the pressure of the aqueous phase, and the saturation of the gas phase as in the following example: ::

  FLOW_CONDITION initial
    TYPE
      LIQUID_PRESSURE dirichlet
      GAS_SATURATION dirichlet
      TEMPERATURE dirichlet
    END
    LIQUID_PRESSURE 1.20D7 Pa
    GAS_SATURATION 0.5
    TEMPERATURE 50.0 C
  END

