
SOLVENT_TL: flow condition to initialize a study with constant pressure and saturations
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

In the Solvent model the solution variables are: oil pressure, oil saturation, gas
saturation/bubble point, solvent saturation and temperature.
To initialize a study with constant pressure, saturation and temperature, four Dirichlet
conditions can be applied.

If the oil in place is saturated with reservoir gas, then the gas saturation must be used
instead of the bubble point. See example below. ::

  FLOW_CONDITION initial_press
    TYPE
      OIL_PRESSURE dirichlet
      OIL_SATURATION dirichlet
      GAS_SATURATION dirichlet
      SOLVENT_SATURATION dirichlet
      TEMPERATURE dirichlet
    /
    OIL_PRESSURE 200.0 Bar
    OIL_SATURATION 0.9999
    GAS_SATURATION 0.0001
    SOLVENT_SATURATION 0.0
    TEMPERATURE 15.0d0 C
  /


If the oil in place is unsaturated with reservoir gas, then the bubble point must be used
instead of the gas saturation. See example below. ::

  FLOW_CONDITION initial_press
  TYPE
    OIL_PRESSURE dirichlet
    OIL_SATURATION dirichlet
    SOLVENT_SATURATION dirichlet
    TEMPERATURE dirichlet
    BUBBLE_POINT dirichlet
  /
  OIL_PRESSURE 200.0 Bar
  OIL_SATURATION 1.0d0
  SOLVENT_SATURATION 0.0
  TEMPERATURE 15.0d0 C
  BUBBLE_POINT 100.0 Bar
  /
