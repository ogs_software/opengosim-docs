
TODD_LONGSTAFF flow condition to initialize a study with constant pressure and saturations
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

In the Todd-Longstaff model the solution variables are: oil pressure, oil saturation, gas
saturation, and temperature. To initialize a study with the same pressure, saturation and
temperature everywhere in the reservoir domain, four Dirichlet conditions can be applied.

See example below: ::

  FLOW_CONDITION initial_press
    TYPE
      OIL_PRESSURE dirichlet
      OIL_SATURATION dirichlet
      GAS_SATURATION dirichlet
      TEMPERATURE dirichlet
    /
    OIL_PRESSURE 200.0 Bar
    OIL_SATURATION 1.0d0
    GAS_SATURATION 0.0d0
    TEMPERATURE 15.0d0 C
  /
