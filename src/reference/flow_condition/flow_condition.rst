
.. _flow_cond_sec:

:index:`FLOW_CONDITION`
--------------------------

A flow condition can be used to define:

* Spatial distribution of pressure, temperature and saturations.
* Fluid flow injection or production rate
      
Once defined, the flow condition can be assigned to a portion or the entire reservoir. In a reservoir study, a FLOW_CONDITION that describes a spatial distribution of pressure, temperature and saturations can be used to initialise a study, see example of an hydrostatic equilibration below: ::

  FLOW_CONDITION initial_sol

    TYPE
      PRESSURE hydrostatic
     /

    PRESSURE 120.0 Bar
    DATUM_D  1200 m

    GAS_IN_LIQUID_MOLE_FRACTION 0.0

    TEMPERATURE_TABLE
      D_UNITS m
      TEMPERATURE_UNITS C !cannot be otherwise
      RTEMPVD
       1150 50
       1300 54.5
      /
    END
   
  /

The name of the FLOW_CONDITION (e.g. initial_sol), must be specified, as it will be required by the INITIAL_CONDITION coupler to associate with a REGION.
Since version OGS-1.6 a new :ref:`EQUILIBRATION<equil_sec>` option is available to set up an hydrostatic equilibration, which does not require neither INITIAL_CONDITION nor REGION.
FLOW_CONDITION can also be used to initialise a study by a space-constant distribution of pressure temperature ans saturations. For more details see:

 * :ref:`FLOW_CONDITION for constant pressure, temperature and saturation<flow_cond_const_pts_sec>`

Note that for a flow condition to be applied and initialise the study, this must be explicitly used in the :ref:`INITIAL_CONDITION<init_cond_sec>`. It is possible to define multiple flow conditions while using only one of them in the ``INITIAL_CONDITION`` card.

A condition can define a fluid flow injection or production rate using source and sink terms: this is a simplified way of modeling wells, rarely used in real reservoir studies. For more details see :ref:`FLOW_CONDITION for sources and sinks<flow_cond_srcsnk_sec>`.

Finally ``FLOW_CONDITION`` can set the pressure, temperature and saturation values applicable to a reservoir boundary (see :ref:`BOUNDARY_CONDITION<bc_ref_sec>`). However, these conditions require definition of boundary regions not supported by the GRDECL mesh format, see :ref:`Special Regions<spec_region_meth_sec>`.


Boundary conditions, ``FLOW_CONDITION`` can also be used to describe a transient set of pressure, temperature and saturation values, see :ref:`transient FLOW_CONDITION<flow_cond_td_sec>`.


.. _flow_cond_const_pts_sec:

FLOW_CONDITION for constant pressure, temperature and saturation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

``FLOW_CONDITION`` can be used to assign a set of pressure, temperature and saturations
constant in space. Such a flow condition can be applied to the reservoir with
``INITIAL_CONDITION`` to initialise a study. ::

  FLOW_CONDITION <flow_condition_name>
    TYPE
      OIL_PRESSURE dirichlet
      OIL_SATURATION dirichlet
      GAS_SATURATION dirichlet
      GAS_PRESSURE dirichlet
      LIQUID_PRESSURE dirichlet
      GAS_IN_LIQUID_MOLE_FRACTION dirichlet
      WATER_IN_VAPOR_MOLE_FRACTION dirichlet
      BUBBLE_POINT dirichlet
      SOLVENT_SATURATION dirichlet
      TEMPERATURE dirichlet
    /
    OIL_PRESSURE <oil_pressure> <pressure_unit>
    OIL_SATURATION <oil_saturation>
    GAS_SATURATION <gas_saturation>
    GAS_PRESSURE <gas_pressure> <pressure_unit>
    LIQUID_PRESSURE <liquid_pressure> <pressure_unit>
    GAS_IN_LIQUID_MOLE_FRACTION <x_gas_liq>
    WATER_IN_VAPOR_MOLE_FRACTION <x_wat_vap>
    BUBBLE_POINT <bubble_point> <pressure_unit>
    SOLVENT_SATURATION <solvent_saturation>
    TEMPERATURE <temperature> <temperature_unit>
  /

where:

* <flow_condition_name>: name to be used in :ref:`INITIAL_CONDITION<init_cond_sec>`, to associate the flow condition with a region (e.g. the entire reservoir).
* <oil_pressure>: oil pressure value.
* <oil_saturation>: oil saturation value.
* <gas_saturation>: gas saturation value.
* <gas_pressure>: gas pressure value.
* <liquid_pressure>: liquid pressure value (used by the GAS_WATER module).
* <x_gas_liq>: value of mole fraction of gas in liquid (used by the GAS_WATER module).
* <x_wat_vap>: value of mole fraction of water in vapour (used by the GAS_WATER module).
* <bubble_point>: specify the bubble point value for the whole region.
* <temperature> specify the temperature value for the whole region.

Different flow models use part the flow condition sub-blocks explained above. A few
examples for specific flow models are given below to clarify their use in practical cases.

.. toctree::
   :maxdepth: 1
   :caption: Examples

   For Black Oil<flow_cond_const_bo>
   For Simple Todd Longstaff<flow_cond_const_tl>
   For Solvent Todd Longstaff<flow_cond_const_slv_tl>
   For Gas-Water<flow_cond_const_gw>


.. _flow_cond_srcsnk_sec:

FLOW_CONDITION for sources and sinks
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

We consider here how to impose mass or volumetric rates to regions that include one or more cells. The rate can be constant or changing with time. In the case of fluid injection, the energy
entering the reservoir can be specified with one of the three following options:

(i) temperature of the fluid being injected. In this case, the pressure used in the computation of the fluid enthalpy (i.e. the energy content of the fluid being injected), is taken equal to the pressure of the grid block where the source term is included. Since this pressure is changing (usually a pressure build-up as the flow enters the grid block), the energy contents of the of the fluid being injected is also changing.
(ii) enthalpy of the fluid being injected
(iii) energy rate.

For injection, the resulting temperature of the perforated grid block will be controlled by:
the warming up of the rock, the energy content of the fluid being injected, and the heat
dispersion by advection and by diffusion towards the surrounding grid blocks.
For production, the temperature of the perforated grid block depends only on fluid flows
and temperature conditions of the surrounding grid blocks, as for any other grid block of the
reservoir. ::

  FLOW_CONDITION <flow_condition_name>
  TYPE
    RATE <rate_type> <scaling_type>
    <ENERGY_VARIABLE> <energy_var_type>
  /
  RATE <r_w> <r_o> <r_g> <r_s> <r_e> <r_w_u> <r_o_u> <r_g_u> <r_s_u> <r_e_u>
  <ENERGY_VARIABLE> <energy_var_data> <energy_var_units>
  /

Where:

* <flow_condition_name>: the name of the flow condition to be used in the :ref:`SOURCE_SINK<source_sink_sec>` coupler when defining source/sinks to be associated with this flow condition.
* <rate_type>: [mass_rate,volumetric_rate,scaled_mass_rate]

   * mass_rate: to specifies a mass rate
   * volumetric_rate: to specify a volumetric rate
   * scaled_mass_rate: to specify an overall mass rate to be distributed between all grid blocks the condition is applied to
   * scaled_volumetric_rate: to specify an overall volumentric rate to be distributed between all grid blocks the condition is applied to

* <scaling_type> [volume, perm] (for “scaled_mass_rate” and “scaled_volumetric_rate”):

  * volume: when applying the condition to multi-block regions, scales the rate of each block by the block volume. Grid blocks with larger volume will be subject to higher rates.
  * Perm: when applying the condition to multi-block regions, scales the rate of each block by the block permeability. Grid blocks with larger peremability will be subject to higher rates.

* <ENERGY_VARIABLE> is optional, and is read only for injectors. if included can be either TEMPERATURE or ENTHALPY, and the energy computed with this variable overwrites the energy rate specified in <r_energy>
* <energy_var_type>: “dirichlet” only option, . It assigns <energy_var_data> to the region associated with the flow condition.
* <r_w>: water rate, can be either a constant value or a time series in case of time-dependent data (see :ref:`transient data block<transient_data_block_sec>` ). 
* <r_o>: oil rate, can be either a constant value or a time series in case of time-dependent data (see :ref:`transient data block<transient_data_block_sec>`).
* <r_g>: gas rate, can be either a constant value or a time series in case of time-dependent data (see :ref:`transient data block<transient_data_block_sec>`).
* <r_s>: solvent rate, can be either a constant value or a time series in case of time- dependent data (see :ref:`transient data block<transient_data_block_sec>`).
* <r_e>: energy rate, can be either a constant value or a time series in case of time- dependent data (see :ref:`transient data block<transient_data_block_sec>`).
* <r_w_u>: specifies the water rate units, must be entered or the software returns an error
* <r_o_u>: specifies the oil rate units, must be entered or the software returns an error
* <r_g_u>: specifies the gas rate units, must be entered or the software returns an error
* <r_s_u>: specifies the solvent rate units, must be entered or the software returns an error
* <r_e_u>: specifies the energy rate units, must be entered or the software returns an error
* <energy_var_data>: specifies the value of temperature or the enthalpy of the fluid being injected. This can be either a constant value or a time series in case of time-dependent data (see :ref:`transient data block<transient_data_block_sec>`).
* <energy_var_units> specify either the temperature or the enthalpy units. If not entered is defaulted to “C” or “J”. For time series is defined in the :ref:`transient data block<transient_data_block_sec>`.


Positive rates are considered to be injections, and negative rates extractions.
The option for the rate units are:

* mass rates: “kg/s”, “kg/day”, “kg/year”
* volumetric rates: “m^3/s”, “m^3/day”, “m^3/year”

The option for the energy rate units are: “MW”, “KW”

Based on the flow model option selected different component can be injected or produced.
Few examples are shown below to help understanding the application of the source/sink
conditions for each flow model.

Source/sink condition TOIL_IMS example: ::

  FLOW_CONDITION wat_injection
    TYPE
      RATE mass_rate
      TEMPERATURE dirichlet !other option is “ENTHALPY”
    /
    !Wat(kg/s), oil(kg/s), Energy rate (MW)
    RATE 0.1d0 0.d0 0.0d0 kg/s kg/s MW
    TEMPERATURE 50 C
  /

Source/sink condition BLACK_OIL example: ::

  FLOW_CONDITION gas_injector_cond
    TYPE
      RATE volumetric_rate
      TEMPERATURE dirichlet
    /
    RATE 0.0000000 0.000000 1000 0 m^3/day m^3/day m^3/day MW
    TEMPERATURE 15.0 C
  /

Source/sink condition TODD_LONGSTAFF example: ::

  FLOW_CONDITION gas_injector_cond
    TYPE
      RATE volumetric_rate
      TEMPERATURE dirichlet
    /
    RATE 0.0000000 0.000000 100 0 m^3/day m^3/day m^3/day MW
    TEMPERATURE 15.0 C
  /

Source/sink condition – SOLVENT_TL example: ::

  FLOW_CONDITION gas_injector_cond
    TYPE
      RATE volumetric_rate
      TEMPERATURE dirichlet
    /
    ! Inject gas and solvent
    RATE 0.0 0.0 500.0 500.0 0.0 m^3/day m^3/day m^3/day m^3/day kW
    TEMPERATURE 15.0 C
  /






.. _flow_cond_td_sec:

FLOW_CONDITION for time-dependent pressure, temperature and saturation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

FLOW_CONDITION can be used to assign a set of time-dependent pressure, temperature
and saturations constant in space. Such a flow condition, can be applied to the reservoir
with BOUNDARY_CONDITION to emulate a transient well or boundary conditions. ::

  FLOW_CONDITION <flow_condition_name>
    TYPE
      OIL_PRESSURE dirichlet
      OIL_SATURATION dirichlet
      GAS_SATURATION dirichlet
      BUBBLE_POINT dirichlet
      SOLVENT_SATURATION dirichlet
      TEMPERATURE dirichlet
    /
    OIL_PRESSURE <oil_pressure_data>
    OIL_SATURATION <oil_saturation_data>
    GAS_SATURATION <gas_saturation_data>
    BUBBLE_POINT <bubble_point_data>
    SOLVENT_SATURATION <solvent_saturation_data>
    TEMPERATURE <temp_data>
    INTERPOLATION <interpolation_type>
  /

where:

* <flow_condition_name>: name to be used in the :ref:`BOUNDARY_CONDITION<bc_ref_sec>` coupler blocks, to associate the flow condition with a surface region.
* <oil_pressure_data>: specify the pressure values for the whole region or at the datum. This can be either be a constant value or a list in case of a time-dependent data (see :ref:`transient data block<transient_data_block_sec>`)
* <oil_saturation_data>: specify the oil saturation values for the whole region. This can be either a constant value or a list in case of time-dependent data (see :ref:`transient data block<transient_data_block_sec>`).
* <gas_saturation_data>: specify the gas saturation values for the whole region. This can be either a constant value or a list in case of time-dependent data (see :ref:`transient data block<transient_data_block_sec>`).
* <bubble_point_data>: specify the bubble point values for the whole region. This can be either a constant value or a list in case of time-dependent data (see :ref:`transient data block<transient_data_block_sec>`).
* <temp_data> specify the temperature values for the whole region. This can be either a constant value or a list in case of time-dependent data (see :ref:`transient data block<transient_data_block_sec>`)
* <interpolation_type>: specifies the type of interpolation when a time-series for transient data are specified. It can be either “step” (the default), or “linear”. See :ref:`transient data block<transient_data_block_sec>` for mode details.


.. _transient_data_block_sec:

Transient data block to define time-dependent values within a flow condition
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

When the value to be associated with a variable (e.g. pressure, saturation, temperature) is
time dependent, a time-series must be specified to define variable changes with time. This
can be done directly in the in input deck using the ``LIST`` sub-block, when the data to be
provided in the input deck is not too large. For a time series requiring n entries, ``LIST`` can be
used as follows: ::

  <VAR_NAME> LIST
    TIME_UNITS <time_units>
    DATA_UNITS <var_data_units>
    <t_1> <var_value_t_1>
    .....................
    <t_i> <var_value_t_i>
    ......................
    <t_n> <var_value_t_n>
  END

During the simulation, the value of the variable at time t is updated using the value
specified at the previous time if ``INTERPOLATION``, in :ref:`FLOW_CONDITION<flow_cond_sec>` is set to “step”, i.e.:

.. math::
  \mbox{for } t_i < t < t_{(i+1)} => \mbox{var_t} = \mbox{var_value_t_i}

Or with a linear interpolation between the two values corresponding to t_i and t_(i+1), if
INTERPOLATION is set to “linear”.

See example below: ::

  FLOW_CONDITION source
    TYPE
      RATE mass_rate
      TEMPERATURE dirichlet
    END
    RATE LIST
      TIME_UNITS d
      DATA_UNITS kg/s
      0.     1.0   0.0
      10.    1.0   0.0
      10.001 0.    0.0
      1d22   0.0   0.0
    /
    TEMPERATURE 80.0 C
  END

In the example above, ``RATE LIST`` is used to impose a transient rate. ``TIME_UNITS`` and
``DATA_UNITS`` specify the units of the data entered. Each transient rate entry, under ``RATE LIST`` specifies: the time for which the rate entries are given, the water rate, the oil rate, the
energy rate. In this specific case, the water rate is equal to 1.0 kg/s between 0 and 10 days, it then changes to 0 for the remainder of the simulation.

When the amount of data required to describe the time series becomes too large, this can
be provided by an external file, using a ``FILE`` keyword followed by the file name, as described in the example below: ::

  FLOW_CONDITION source
    TYPE
      RATE mass_rate
      TEMPERATURE dirichlet
    END
    RATE FILE injection_rate.dat
    TEMPERATURE 80.0 C
  END

where the file injection_rate.dat contains the time series, with the same format used by ``LIST``, see example below: ::

  DATA_UNITS kg/s
  0.   0.3 0.0
  1.   0.5 0.0
  2.   0.8 0.0
  3.   1.0 0.0
  4.   1.1 0.0
  5.   1.2 0.0
  6.   0.0 0.0
  1d22 0.0 0.0






