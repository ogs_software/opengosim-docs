
.. _init_cond_sec:

:index:`INITIAL_CONDITION`
-------------------------------


This block links a particular ``FLOW_CONDITION`` to the appropriate REGION to define an initial condition. It is set up by first creating a list of flow conditions, and then applying them to appropriate REGIONs.

For example, this block can be used with an hydrostatic condition to set up an equilibrated initialisation, see below. ::

  INITIAL_CONDITION Equilibration
    FLOW_CONDITION hydrostatic_pressure
    REGION all
  /

