
.. _linear_solver_card_sec:

:index:`LINEAR_SOLVER`
------------------------

Specifies linear solver parameters associated with the simulation. A typical general purpose example is as follows: ::

  LINEAR_SOLVER FLOW
    KSP_TYPE FGMRES
    PC_TYPE CPR
  END

An unlikely example demonstrating all possible options is: ::

  LINEAR_SOLVER  FLOW
    KSP_TYPE FGMRES
    PC_TYPE CPR
    ATOL 1.d-6
    RTOL 1.d-6
    DTOL 1.d-6
    MAXIT 50
    GMRES_RESTART 200
    GMRES_MODIFIED_GS
    CPR_OPTIONS
      CPR_MANUAL_AMG_CONFIG
      CPR_AMG_REPORT
    END
  END



The meanings of the keywords above are given in the following sections.

The preconditioner CPR is the implementation of the compressed pressure residual
preconditioner that OGS has developed. This is a two stage preconditioner which uses AMG
(algebraic multigrid) as the first stage; currently block Jacobi is the only available second
stage. CPR is a very effective modern preconditioner and should provide a significant
increase in efficiency and decrease in linear iterations in almost all nontrivial situations.

In addition to the options described above there is a sub-card CPR_OPTIONS which collects
options specifically for the CPR preconditioner. These are listed in :ref:`Advanced AMG/CPR configuration section<advanced_amg_cpr_chap>`.


LINEAR_SOLVER Type
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


The word after ``LINEAR_SOLVER`` at the start of this card can be either FLOW or TRANSPORT. For TOIL_IMS, GAS_WATER, BLACK_OIL,TODD_LONGSTAFF and SOLVENT_TL, this must be FLOW.


.. _ksp_type_sec:

KSP_TYPE
^^^^^^^^^^^^^^^^^^

Specifies solver type, where options include: DIRECT, ITERATIVE, GMRES, BCGS, IBCGS. DIRECT uses LU and ITERATIVE employs Bi-CGStab (BCGS) and block Jacobi preconditioning with ILU[0] in each block.


.. _pc_type_sec:

PC_TYPE
^^^^^^^^^^^^^^^^^^^^
Specifies preconditioner type, where options include: NONE, ILU, LU, BJACOBI, ADDITIVE_SCHWARZ or ASM, HYPRE, or CPR. CPR is the compressed pressure residual preconditioner which OGS has implemented for PFLOTRAN. This is the AMG linear solver option, more on the CPR and its options are discussed above.

ATOL
^^^^^^^^^^^^

absolute tolerance. Absolute size of 2-norm of residual.


RTOL
^^^^^^^^^^^^^
Relative tolerance. Relative decrease in size of 2-norm of residual.

DTOL
^^^^^^^^^^
Divergence tolerance. Relative increase in residual norm.

.. _maxit_ls_sec:

MAXIT
^^^^^^^^^^^^^^^^^

Maximum number of linear solver iterations before reporting failed convergence.

.. _gm_rst_sec:

GMRES_RESTART
^^^^^^^^^^^^^^^^^^^^
How many linear iterations to allow before restarting the Krylov subspace, if using GMRES or FGMRES as the linear solver.

.. _mod_gs_sec:

GMRES_MODIFIED_GS
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Use modified instead of classical Gram-Schmidt orthogonalization in the construction of the Krylov subspace if using GMRES or FGMRES as the linear solver.

