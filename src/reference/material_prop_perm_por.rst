
.. _mat_prop_perm_poro_sec:

MATERIAL_PROPERTY with permeability and porosity
========================================================

Note that if a mesh format different than GRDECL is used, the porosity and permeability
values will be taken from MATERIAL_PROPERTY. Therefore, in addition to the data input
described in :ref:`MATERIAL_PROPERTY<material_prop_sec>`, the user must enter the PERMEABILITY and
POROSITY block: ::

  MATERIAL_PROPERTY <material_name>
    ID <material_id>
    POROSITY [<constant_porosity>, DATASET <porosity_dataset>]
    <PERMEABILITY BLOCK>
    CHARACTERISTIC_CURVES <ch_curve_name>
    ROCK_DENSITY <rock_density>
    SOIL_COMPRESSIBILITY <soil_compress_coeff>
    SOIL_COMPRESSIBILITY_FUNCTION <compress_function_type>
    SOIL_REFERENCE_PRESSURE [<ref_press> or INITIAL_PRESSURE]
    SPECIFIC_HEAT <spec_heat> or HEAT_CAPACITY <heat_cap>
    THERMAL_CONDUCTIVITY_DRY <th_cond_dry>
    THERMAL_CONDUCTIVITY_WET <th_cond_wet>
  END


PERMEABILITY
----------------------------

Note that permeabilities are specified in m2 , rather than milliDarcies, as used by Eclipse.

<PERMEABILITY BLOCK> depends on the type of permeability field to be used, as follows:

<PERMEABILITY BLOCK> for Isotropic and homogeneous permeability fields: ::

  PERMEABILITY
    ISOTROPIC: Turn off anisotropy.
    PERM_ISO <iso_perm_value>: specifies a constant isotropic permeability value [m2]
  END

<PERMEABILITY BLOCK> for anisotropy and homogeneous permeability fields: ::

  PERMEABILITY
    PERM_X <perm_x_value>: specifies a constant permeability value in x-direction [m2]
    PERM_Y <perm_y_value>: specifies a constant permeability value in y-direction [m2]
    PERM_Z <perm_z_value>: specifies a constant permeability value in z-direction [m2]
  END

<PERMEABILITY BLOCK> for anisotropic and heterogeneous permeability fields:

In this case the permeabilities are taken from a ‘dataset’, a block of data which is normally loaded from a file: ::

  PERMEABILITY
    ANISOTROPIC or ISOTROPIC: specifies the type of permeability field
    DATASET <dataset_name>: dataset where the permeability field is defined [m2]
  END

In case of ANISOTROPIC permeability, three data sets must be present in the input deck, named <dataset_name>X, <dataset_name>Y and <dataset_name>Z respectively.


POROSITY
------------------------

This can be defined in two alternative ways:

*  <constant_porosity>: assigning a constant value for all cells belonging to the material being defined.
*  DATASET <porosity_dataset>: assigning porosity values cell-by-cell, where the values are provided in a DATASET defined in the input deck.

The example below illustrates a heterogeneous anisotropic case. Note that in addition to
this information, the required datasets must have been loaded - see :ref:`DATASET<dataset_poro_sec>` for details of how to do this: ::

  MATERIAL_PROPERTY formation
    ID 1
    POROSITY DATASET Porosity
    ROCK_DENSITY 2.350d3
    SPECIFIC_HEAT 1.0d3
    THERMAL_CONDUCTIVITY_DRY 1.541d0
    THERMAL_CONDUCTIVITY_WET 1.541d0
    CHARACTERISTIC_CURVES ch1
    PERMEABILITY
      ANISOTROPIC
      DATASET perm
    /
  /

.. _dataset_poro_sec:

DATASET
-------------------------

Specifies a data set to be associated with parameter sets in the model. ::

  DATASET <dataset_name>
    FILENAME <dataset_file_name>: specifies the external file name of the dataset
    HDF5_DATASET_NAME <hdf5_dataset_name>: specifies the dataset name
  END

See example below for a dataset defining the permeability in the x direction ::

  DATASET permX
    FILENAME perm_poro.h5
    HDF5_DATASET_NAME PermeabilityX
  END

Note such datasets must follow the conventions of an `HDF5 file <https://www.hdfgroup.org/>`_. Such datasets normally need to be prepared by external programs which link in the HDF5 libraries.




