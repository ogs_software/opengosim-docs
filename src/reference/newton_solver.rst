
.. _newton_solver_flow_sec:

:index:`NEWTON_SOLVER FLOW`
------------------------------

Specifies nonlinear solver parameters associated with the simulation. An example is as follows: ::

  NEWTON_SOLVER FLOW
    ATOL 1D-8
    RTOL 1D-8
    STOL 1D-30
  END

An example showing all possible options is: ::

  NEWTON_SOLVER FLOW
    NO_INFINITY_NORM
    NO_FORCE_ITERATION
    NO_PRINT_CONVERGENCE
    PRINT_DETAILED_CONVERGENCE
    ATOL 1D-8
    RTOL 1D-8
    STOL 1D-30
    ITOL 1D-6
    ITOL_S 1D-6
    ITOL_UPDATE 1D-6
    MAXIT 10
    MAXF 50
    MATRIX_TYPE AIJ
    PRECONDITIONER_MATRIX_TYPE AIJ
  END

The variables in the keywords above have the following meanings:






NEWTON_SOLVER Type
^^^^^^^^^^^^^^^^^^^


Can be FLOW or TRANSPORT. For TOIL_IMS, GAS_WATER, BLACK_OIL, TODD_LONGSTAFF and SOLVENT_TL, this must be FLOW.

NO_INFINITY_NORM
^^^^^^^^^^^^^^^^^^^^^^

If included, toggles off calculation of infinity norm on residual and update vectors. The default is to calculate infinity norm.


NO_FORCE_ITERATION
^^^^^^^^^^^^^^^^^^^^^^

If included toggles off the forcing of at least 1 linear iteration. The default is to force at least 1 linear iteration.

NO_PRINT_CONVERGENCE
^^^^^^^^^^^^^^^^^^^^^^^^

If included, prints convergence information.

PRINT_DETAILED_CONVERGENCE
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
If included, printing of detailed convergence information.

ATOL
^^^^^^^^^^^^^
Absolute tolerance. Absolute size of 2-norm of residual.

RTOL
^^^^^^^^^^^^

Relative tolerance. Relative decrease in size of 2-norm of residual.

.. _stol_ns_sec:

STOL
^^^^^^^^^^^^^^^^

Relative update tolerance. Relative decrease in size of 2-norm of solution

Note: STOL needs to be treated with great caution, as it simply accepts the step when then
solution updates fall below the specified criteria, and this may allow badly unconverged
steps to be accepted. It is recommended that STOL be set very low (e.g. 1.0D-30, as in the
examples above) so that this cannot happen

.. _itol_ns_sec:

ITOL
^^^^^^^^^^^^^
Infinity tolerance. Size of infinity norm of residual.

.. _itol_s_ns_sec:

ITOL_S
^^^^^^^^^^^^^
Infinity tolerance. Size of infinity norm of scaled residual.


ITOL_UPDATE
^^^^^^^^^^^^^^^^^^^^^^

Infinity tolerance. Size of infinity norm of update

.. _maxit_ns_sec:

MAXIT
^^^^^^^^^^^^^

Maximum number of Newton iterations before reporting failed convergence.

MAXF
^^^^^^^^^^^^^

Maximum number of function evaluations before reporting failed convergence.

MATRIX_TYPE 
^^^^^^^^^^^^^^^^^

Format of main solver matrix. PETSc Mat (i.e. AIJ, BAIJ, or HYPRESTRUCT).

PRECONDITIONER_MATRIX_TYPE
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Format of preconditioning matrix. Default is same as solver.
