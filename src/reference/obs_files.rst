
.. _variable_output_sec:

:index:`OBSERVATION_FILE`
--------------------------------------------------------------
 
The OBSERVATION_FILE sub-block of OUTPUT requests that the simulation outputs observations of variables on limited portions of the reservoir domain. See example below: ::

  OUTPUT
    FORMAT HDF5
    MASS_BALANCE_FILE
      PERIODIC TIMESTEP 1
    END
    OBSERVATION_FILE
       PERIODIC TIMESTEP 5
        VARIABLES
           TEMPERATURE
           GAS_SATURATION
           GAS_PRESSURE
        END
    END
  END

OBSERVATION_FILES output :ref:`observation point<observation_sec>` variables into ASCII files named <input_deck_name>-obs-proc_numb.tec, where proc_numb is the processor number the observation points belong to. Due to the domain decomposition, different observation points can fall within different sub-domains. The simulator outputs one observation file for each sub-domain, with all variable information related to the observation points falling within that sub-domain. Within each observation file, the variable values of all observation points are listed one time step for each line, and within each line the variables requested are grouped and printed by observation point. 


See below an example of an an observation file for a single observation point for which temperature, gas saturation and gas pressure are printed:


.. figure:: ../manual/images/obs_file_example.png
   :scale: 75%

The options supported for OBSERVATION_FILE are:

.. contents::
  :backlinks: top
  :depth: 2
  :local:

TIMES
^^^^^^^^^^^^^^^^^^

The TIMES sub-block of OBSERVATION_FILE takes one argument that specifies a time unit, followed by a generic number N of arguments that are the times for which the observation point variables are output. See example: ::

  OBSERVATION_FILE
     TIMES d 5 30 100 500 1095
  END


PERIODIC TIMESTEP
^^^^^^^^^^^^^^^^^^^

The PERIODIC TIMESTEP sub-block of OBSERVATION_FILE takes one argument, which is the number of time steps between two consecutive simulation states, for which observation point variables are output. See example: ::

  OBSERVATION_FILE
    PERIODIC TIMESTEP  1
  END


PERIODIC TIME
^^^^^^^^^^^^^^^^^^^^

The PERIODIC TIME sub-block of OBSERVATION_FILE takes two arguments: the first is the time between two consecutive simulation states for which observation point variables are output, the second is the time unit, see example below: ::

  OBSERVATION_FILE
    PERIODIC TIME  5 d
  END

VARIABLE
^^^^^^^^^^^^^^^^^^^^^^
The VARIABLE sub-block of OBSERVATION_FILE is a multiple-line instruction block, starting with the keyword VARIABLE and ending by END or ‘/’.


The variables that can be output are model-dependent. Below a the list of variables available for TOWG and TOIL_IMS grouped by fluid phase and material properties, using the syntax expected within by the VARIABLE block:

 * OIL_PRESSURE, OIL_SATURATION, OIL_DENSITY, OIL_MOBILITY, OIL_ENERGY
 * GAS_PRESSURE, GAS_SATURATION, GAS_DENSITY, GAS_MOBILITY, GAS_ENERGY
 * LIQUID_PRESSURE, LIQUID_SATURATION, LIQUID_DENSITY, LIQUID_MOBILITY, LIQUID_ENERGY
 * TEMPERATURE, POROSITY, PERMEABILITY_X, PERMEABILITY_Y, PERMEABILITY_Z

Note that the GAS variables are available only for the TOWG flow model, and that LIQUID refers to water. Each variable to print must be included in a single line.

If no VARIABLES are specified, the flow model default set of variables will be printed.


