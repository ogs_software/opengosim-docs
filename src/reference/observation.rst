
.. _observation_sec:

:index:`OBSERVATION`
---------------------------

Sets up an observation point(s) within the problem domain. ::

  OBSERVATION
    REGION <region_name>
    VELOCITY
    AT_CELL_CENTER
    AT_COORDINATE
  END

Where:

* <region_name>: the name of a region specified in the input deck, (i.e. a point or an array of points within the reservoir domain) where the observation is placed.
* VELOCITY: if included prints the cell centered velocities for the cells being observed
* AT_CELL_CENTER: if included specifies that the observation data is sampled at center of cell intersected by observation (default).
* AT_COORDINATE: if included the observation data is interpolated (linearly) from neighboring cell centers to coordinate

See example below: ::

  OBSERVATION
    REGION inj1
  /
