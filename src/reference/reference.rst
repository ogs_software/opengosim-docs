

Reference Pages
============================

In these pages we provide details on some specialist and less used functionalities in |pftogs|.


.. toctree::
   :maxdepth: 1

   timestepper
   newton_solver
   linear_solver
   flow_condition/flow_condition
   initial_condition
   boundary_condition
   source_sink
   region
   strata
   observation
   obs_files
   snapshot
   spec_keywords
   spec_grids
   advanced_amg_cpr
   material_prop_perm_por
   special_region_methods
   alyt_opts_char_curves
   solution_monitor
   trademarks
