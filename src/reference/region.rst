
.. _region_sec:

:index:`REGION`
-----------------------

Defines a region (e.g. a volume portion, a surface, a set of points) within the reservoir
domain. Regions are used to specify the problem initialisation, assign material properties,
define boundary conditions, and observations in the reservoir domain.

Each region is defined using the REGION keyword, followed by its name, and being a
multiple-line keyword must end by ``END`` or ``/``. Different methods are available to define
the space portion within the reservoir (volume, surface or points), depending on the type of
grids.

For the reservoir simulation cases that use GRDECL, the only supported method is the
volume region by lower and upper coordinates. Volume regions can be defined once the
upper and lower coordinates bounds are known. In practical terms this method is used to
either define a region that includes the entire domain needed to intialised the study, or a
portion of the domain needed to apply an OBSERVATION. ::

  REGION <region_name>: specifies the region name
    COORDINATES
      <x_min, y_min, z_min>: specify the minimum values for the x, y and z coordinates
      <x_max, y_max, z_max>: specify the maximum values for the x, y and z coordinates
    END
  END

Note that <z_min>, <z_max> uses the default PFLOTRAN convention instead of the
GRDECL convention. That is <z_min> is an elevation with respect to the reference depth
instead of a relative depth. In general, once the depth is known, the elevation can be
worked out swapping the sign: <z_min> = - <depth_min>; <z_max> = - <depth_max>

Below is an example of a region that includes the entire domain, where lower and upper
corners are assigned very large positive and negative values to include all grid blocks: ::

  REGION all
    COORDINATES
      -1.d20 -1.d20 -1.d20
      1.d20 1.d20 1.d20
    /
  /

Below is another example where a region is used to define a portion of the domain where to
apply an OBSERVATION. In this case the reference depth is zero, and <z_min>, <z_max>
are both negative to define depths: ::

  REGION reservoir_portion_one
    COORDINATES
      1500.d0 1000.d0 -50.d0
      2000.0 2000.0 -120.0
    /
  /

Other methods that do not support GRDECL are available to advanced PFLOTRAN users, see
:ref:`Special REGION definition method<spec_region_meth_sec>`.

