
.. _snapshot_sec:

:index:`SNAPSHOT_FILE`
--------------------------------------------------------------

The SNAPSHOT_FILE sub-block of OUTPUT requests that the simulation outputs HDF5 format files. See example below: ::

  OUTPUT
    FORMAT HDF5
    MASS_BALANCE_FILE
      PERIODIC TIMESTEP 1
    END
    SNAPSHOT_FILE
      TIMES d 30 500 1000
      VARIABLES
        TEMPERATURE
        GAS_SATURATION
        LIQUID_SATURATION
        GAS_PRESSURE
        LIQUID_PRESSURE
      END
    END
    PRINT_PRIMAL_GRID
  END

The instructions above output 3D snapshots of the variables on the entire reservoir domain at given times (30 500  and 1000 days), for visualisation in `Paraview <http://www.paraview.org/>`_.

The following options can be used to tune the SNAPSHOT_FILE output:

.. contents::
  :backlinks: top
  :depth: 2
  :local:


FORMAT
^^^^^^^^^^^^^^^^^^^^^^^^^

The FORMAT sub-block of OUTPUT specifies the format of the SNAPSHOT_FILE, and can take the following values:

  * HDF5 to include all printed times in the same file.
  * HDF5 MULTIPLE_FILES to include each printed time in a different file.
      
If FORMAT is not entered, no snapshots files are saved.

Note that the PRINT_PRIMAL_GRID sub-block of OUTPUT is required when using GRDECL grids and the HDF5 output format.

Note that the HDF5 format uses an external library for high performance parallel input/output operations (https://www.hdfgroup.org/). In the version downloaded with PETSc, a issue has been encountered with this library, which makes the software write an empty grid file (-domain.h5), when running pflotran in parallel (np >1). The problem has a work around: generate the -domain.h5 file containing the grid geometry information running pflotran sequentially (np = 1), save the file in a different folder to avoid it gets overwritten by successive parallel runs, run the simulation in parallel, copy back the -domain.h5 file generated earlier to visualise the results.


TIMES
^^^^^^^^^^^^^^^^^

The TIMES sub-block of SNAPSHOT_FILE takes one argument that specifies a time unit, followed by a generic number N of arguments that are the times for which simulation states must be saved. See example: ::

 SNAPSHOT_FILE
   TIMES d 5 30 100 500 1095
 END


PERIODIC TIMESTEP
^^^^^^^^^^^^^^^^^^^^^^^^^

The PERIODIC TIMESTEP sub-block of SNAPSHOT_FILE takes one argument, which is the number of time steps between two consecutive simulation states to be saved. See example: ::

  SNAPSHOT_FILE
    PERIODIC TIMESTEP  10
  END


PERIODIC TIME
^^^^^^^^^^^^^^^^^

The PERIODIC TIME sub-block of SNAPSHOT_FILE takes two arguments: the first is the time between two consecutive simulation states to be saved in the restart files, the second is the time unit, see example below: ::

  SNAPSHOT_FILE
    PERIODIC TIME  20 d 
  END


VARIABLES
^^^^^^^^^^^^^^^^^

The VARIABLE sub-block of SNAPSHOT_FILE is a multiple-line instruction block, starting with the keyword VARIABLE and ending by END or ‘/’.

The variables that can be output are model-dependent. Below a the list of variables available for TOWG and TOIL_IMS grouped by fluid phase and material properties, using the syntax expected within by the VARIABLE block:

 * OIL_PRESSURE, OIL_SATURATION, OIL_DENSITY, OIL_MOBILITY, OIL_ENERGY
 * GAS_PRESSURE, GAS_SATURATION, GAS_DENSITY, GAS_MOBILITY, GAS_ENERGY
 * LIQUID_PRESSURE, LIQUID_SATURATION, LIQUID_DENSITY, LIQUID_MOBILITY, LIQUID_ENERGY
 * TEMPERATURE, POROSITY, PERMEABILITY_X, PERMEABILITY_Y, PERMEABILITY_Z

Note that the GAS variables are available only for the TOWG flow model, and that LIQUID refers to water. Each variable to print must be included in a single line.

If no VARIABLES are specified, the flow model default set of variables will be printed.


