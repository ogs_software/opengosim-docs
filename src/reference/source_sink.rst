
.. _source_sink_sec:

SOURCE_SINK
===========================

Specifies coupling between flow condition and a REGION for a source/sink term. It can be used
also to define a well. ::

  SOURCE_SINK <source_sink_name>
    FLOW_CONDITION <flow_condition_name>
    REGION <region_name>
  END

Where:

* <source_sink_name>: name of the source_sink coupler.
* <flow_condition_name>: name a flow condition defined in the input deck that will be applied to the region specified in the source sink coupler.
* <region_name>: name of a region defined in the input deck where the flow condition specified in the source sink coupler will be applied.

See example below: ::

  SOURCE_SINK wat_injection
    FLOW_CONDITION wat_injector_condition
    REGION well_1
  END
