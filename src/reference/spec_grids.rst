
Other Specialist Grid formats defined in an External File
=========================================================================

Other formats are available to PFLOTRAN expert users, however they do not currently
support the WELL_DATA wells. Two more formats are reported in this section in two sections:

* :ref:`Structured and Cylindrical Grids<cylin_grid_sec>`
* :ref:`Unstructured Explicit Meshes Defined by External Files<unstruc_exp_grid_sec>`

If using this type of mesh it is possible to emulate wells to some extent using:

* for injectors: PFLOTRAN :ref:`SOURCE_SINK<source_sink_sec>` and :ref:`FLOW_CONDITION for source and sinks<flow_cond_srcsnk_sec>`
* for producers: :ref:`BOUNDARY_CONDITION<bc_ref_sec>` and :ref:`FLOW_CONDITION for constant pressure, temperature and saturation<flow_cond_const_pts_sec>`, and :ref:`FLOW_CONDITION for time-dependent pressure temperature and saturation<flow_cond_td_sec>`.


.. _cylin_grid_sec:

Structured and Cylindrical Grids
---------------------------------------------------

PFLOTRAN generates the grid internally when the structured or cylindrical grid types are
selected, requiring additional input parameters:

* number of cells in the x, y and z direction with NXYZ
* domain size, either specifying the domain lower and upper limits with BOUNDS or the discretisation steps in the x, y and z directions using DXYZ
* (optionally) the domain origin using ORIGIN, which is defaulted to (0,0,0) if not specified.

It is important to observe that in PFLOTRAN the z values stands for elevation rather than
depths as for the case of many commercial reservoir simulators. Therefore, once a datum
or a reference elevation is fixed, z indicates the distance from that reference moving
upwards.

Note, however, that property values with these structured grids are rather limited – for
example porosity values cannot be zero and all the cells in the grid will be present in the
simulation. Less restricted regular grids may be entered using GRDECL keywords such as
DX, DY and DZ (See Manual). WELL_DATA wells are not supported as they need a more
complex internal structure than a PFLOTRAN structured grid (to accommodate multiply
completed wells) and so cannot be used with these simpler grids.

The sub-blocks are required to provide the additional parameters requested by structured
and cylindrical grids are described below.

NXYZ <NX, NY, NZ>:

* <NX>: number of grid blocks in the x direction
* <NY>: number of grid blocks in the y direction
* <NZ>: number of grid blocks in the z direction
 
An example is: ::
  NXYZ 10 20 5

This specifies a grid with Nx=10, Ny=20 and Nz=5.

Different methods are available to define the domain sizes, these are described one by one in the next paragraphs:

* :ref:`Specify the domain size using BOUNDS<spec_domain_bounds_sec>`
* :ref:`Specify the domain size using DXYZ for uniform grids<spec_domain_dxyz_sec>`
* :ref:`Specify the domain size using DXYZ for non-uniform grids – option 1<spec_domain_dxyz_forNUG_O1_sec>`
* :ref:`Specify the domain size using DXYZ for non-uniform grids – option 2<spec_domain_dxyz_forNUG_O2_sec>`

For the use of cylindrical grids, the user is advised to read also the section :ref:`Cylindrical Grid Note<cylindrical_note_sec>`.

.. _spec_domain_bounds_sec:

Specify the domain size using BOUNDS
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

::

  BOUNDS
    <x_min, y_min, z_min>: specify the minimum values for the x, y and z coordinates
    <x_max, y_max, z_max>: specify the maximum values for the x, y and z coordinates
  END

An example of a mesh definition using BOUNDS is given below: ::

  GRID
    TYPE structured
    ORIGIN 0.d0 0.d0 0.d0
    NXYZ 40 40 24
    BOUNDS
      0.d0 0.0 0.0
      2000.d0 2000.d0 120.d0
    /
  END



.. _spec_domain_dxyz_sec:

Specify the domain size using DXYZ for uniform grids
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

::

  DXYZ
    <dx>: specifies a constant space discretisation step in the x direction
    <dy>: specifies a constant space discretisation step in the y direction
    <dz>: specifies a constant space discretisation step in the z direction
  END

See example below: ::

  GRID
    TYPE structured
    NXYZ 10 5 8
    DXYZ
      50.0
      20.0
      2.0
    END
  END

In the example above the domain sizes are Lx = 500m, Ly = 100m, Lz = 16 m.

.. _spec_domain_dxyz_forNUG_O1_sec:

Specify the domain size using DXYZ for non-uniform grids – option 1
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The above form of DXYZ is quite restrictive, as all the DX values, for example, are the same.
An alternative is a list of values using the ``@`` character as follows ::

  DXYZ
    < nx_1 @dx 1 ,..., nx_i @dx i ,...., nx Nx @dx Nx >: specify groups of nx i cells characterised by dx_i
    < ny_1 @dy 1 ,..., ny_j @dy j ,...., ny Ny @dy Ny >: specify groups of ny j cells characterised by dy_j
    < nz_1 @dz 1 ,..., nz_k @dz k ,...., nz Nz @dz Nz >: specify groups of nz k cells characterised by dz_k
  END

where the following constrains apply to the parameters above:

.. math::
  \begin{split}
  &nx_i ≤ N_x &\mbox{and} \sum{nx_i} = N_x \\
  &ny_j ≤ N_y &\mbox{and} \sum{ny_j} = N_y \\
  &nz_k ≤ N_z &\mbox{and} \sum{nz_k} = N_z
  \end{split}

See an example below: ::

    GRID
      TYPE structured
      NXYZ 10 5 8
      DXYZ
        1@50.0 2@75.0 4@100.0 2@75.0 1@50.0
        1@20.0 3@40.0 1@20.0
      2.0
      END
    END

In the example above the domain sizes are Lx = 800m, Ly = 160m, Lz = 16m.

.. _spec_domain_dxyz_forNUG_O2_sec:

Specify the domain size using DXYZ for non-uniform grids – option 2
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The second option is to specify all the cell size values in each direction: ::

  DXYZ
    < dx_1 ,..., dx_i ,...., dx_Nx >: specify N_x discretization steps in the x direction
    < dy_1 ,..., dy j ,...., dy_Ny >: specify N_y discretization steps in the y direction
    < dz_1 ,..., dz k ,...., dz_Nz >: specify N_z discretization steps in the z direction
  END

where the following constrains apply to the parameters above:

.. math::
  \begin{split}
  i &≤ NX  \\
  j &≤ NY  \\
  k &≤ NZ
  \end{split}

See example below ::

  GRID
    TYPE structured
    NXYZ 24 10 40
    DXYZ
      0.30 0.50 1.0 3.0 5.0 10.0 15.0 30.0 60.0 100.0 120.0 150.0 180.0 200.0 200.0 \
      200.0 200.0 180.0 150.0 100.0 80.0 60.0 30.0 10.0
      20.0
      1.0
    END
  END

In the example above the domain sizes are Lx = 2084.8m, Ly = 200m, Lz = 40 m
Note that to avoid writing a line which is too long, the backwards slash ``\`` is used to go to
the next line. Lines in a input deck should not exceed 120 characters.

.. _cylindrical_note_sec:

Cylindrical Grid Note
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

For cylindrical coordinates, the X dimension corresponds to the radius of the cylinder while
the Z dimension represents the height. It is assumed that the Y dimension is variable with
NY = 1, and no Y grid spacing is specified. PFLOTRAN will calculate the distance in the Y
direction automatically based on the 360 degree cylindrical coordinate system.

See example below: ::

  GRID TYPE structured cylindrical
    NXYZ 100 1 10
    BOUNDS
      0.d0 0.
      100.d0 10.d0
    /
  END


.. _unstruc_exp_grid_sec:

Unstructured explicit grids
------------------------------------------------

Unstructured explicit grids are introduced by the “unstructured_explicit” type: ::

  GRID
    TYPE unstructured_explicit <unstructured_grid_file_name>
  END

The unstructured grid file contains four data sections: cells, connections between cells,
element, and vertex ids. The first two sections are compulsory for the fluid flow simulation,
while the last two are only used for the description of the reservoir geometry for post-
processing visualisation purposes. ::

  CELLS <number_of_cells>
  <id_1> <x_1> <y_1> <z_1> <volume_1>
  .........................
  <id_i> <x_i> <y_i> <z_i> <volume_i>
  .................................
  <id_n> <x_n> <y_n> <z_n> <volume_n>
  CONNECTIONS <number_of_connection>
  <id_up_1> <id_dn_1> <fc_1_x> <fc_1_y> <fc_1_z> <fc_1_area>
  ....................
  <id_up_i> <id_dn_i> <fc_i_x> <fc_i_y> <fc_i_z> <fc_i_area>
  ....................
  <id_up_m> <id_dn_m> <fc_m_x> <fc_m_y> <fc_m_z> <area_m>
  ELEMENT <number_of_elements>
  <ele_type_1> <ele_1_v1>, <ele_1_v2>,...., <ele_1_vk>
  ............
  <ele_type_i> <ele_i_v1>, <ele_i_v2>,...., <ele_i_vk>
  ...............
  <ele_type_n> <ele_n_v1>, <ele_n_v2>,...., <ele_n_vk>
  VERTICES <number_of_vertices>
  <v1_x> <v1_y> <v1_z>
  ...........................
  <vi_x> <vi_y> <vi_z>
  ...........................
  <v_nv_x> <v1_nv_y> <v1_nv_z>

Where:

* <number_of_cells> = total number of grid blocks
* <id_i> = id of the cell i
* <x_i> <y_i> <z_i> = cartesian coordinate in meters of the centroids of cell i
* <volume_i> = volume of the cell in m^3
* <number_of_connection> total number of internal connections between cells
* <id_up_i> = id of the left cell connected by the connection i
* <id_dn_i> = id of the right cell connected by the connection i
* <fc_i_x> <fc_i_y> <fc_i_z> = cartesian coordinates in meters of the centre of connection i
* <fc_i_area> = surface in m2 of the connection i
* <number_of_elements>= total number of elements, in most reservoir application this is equal to <number_of_cells>. It differs if a voronoi type of grid is used.
* <ele_type_i> = this the element type of element i, which for the three-dimensional reservoir of interest can be [H, W, P, T ]. This four initials stands for Hexahedron, Wedge, Pyramid, Tetrahedron.
* <ele_i_v1>, <ele_i_v2>,...., <ele_i_vk> = vertices ids that form the element. These ids are the location in the vertex list defined in the fourth section, i.e. VERTICES. The number of vertices for each element vary with the type as reported below:

  * H has 8 vertices
  * W has 6 vertices
  * P has 5 vertices
  * T has 4 vertices

* <number_of_vertices> number of total vertices needed to define the elements. The number must be specified only if <number_of_elements> = <number_of_cells>
* <vi_x> <vi_y> <vi_z> = cartesian coordinate in meters of the vertex i

The mesh file above does not have any spaces between different sections.

An example is given below: ::

  GRID
    TYPE unstructured_explicit unstructured_grid_file_name.dat
  END

Contents of unstructured_grid_file_name.dat for a 2 x 2 x 2= 8 cell grid: ::

  CELLS 8
  1 0.5 0.5 0.5 1.
  2 1.5 0.5 0.5 1.
  3 0.5 1.5 0.5 1.
  4 1.5 1.5 0.5 1.
  5 0.5 0.5 1.5 1.
  6 1.5 0.5 1.5 1.
  7 0.5 1.5 1.5 1.
  8 1.5 1.5 1.5 1.
  CONNECTIONS 12
  1 2 1. 0.5 0.5 1.
  3 4 1. 1.5 0.5 1.
  5 6 1. 0.5 1.5 1.
  7 8 1. 1.5 1.5 1.
  1 3 0.5 1. 0.5 1.
  2 4 1.5 1. 0.5 1.
  5 7 0.5 1. 1.5 1.
  6 8 1.5 1. 1.5 1.
  1 5 0.5 0.5 1. 1.
  2 6 1.5 0.5 1. 1.
  3 7 0.5 1.5 1. 1.
  4 8 1.5 1.5 1. 1.



UPWIND_FRACTION_METHOD
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

For explicit unstructured grids, an additional keyword UPWIND_FRACTION_METHOD allows
advanced users some control over the upwinding methodology.

* [FACE_CENTER_PROJECTION]: for upwinding computations, the upwinding fraction value is computed as the projection of the vector from the center of the upwind cell to the centroid of the joining face, onto the vector between the two cell centers, divided by the absolute distance. This is the default method and works well unless the mesh used happens to be very distorted in which nonphysical values may occur.
* [CELL_VOLUME]: for upwinding computations, the upwinding fraction value is computed as the ratio of the volume of the upwind cell to the volume of the downwind cell.
* [ABSOLUTE_DISTANCE]: for upwinding computations, the upwinding fraction value is computed as the ratio of the absolute distance of the vector from the center of the upwind cell to the joining face centroid, to the sum of the absolute distance of the vectors from the centers of each of the two cells to the face centroid.
