
Specialist and rarely-used keywords
======================================

GRAVITY
--------------------------
This grid section keyword allows to the user to change the gravity vector from its default
value. However if the user attempts to initialize the problem using hydrostatic equilibration,
and a non-default gravity value, the code will return an error. 

* GRAVITY <gx gy gz>: Specifies gravity vector components (default: 0. 0. -9.8068)

See example below, where the gravity effects are neglected ::

  GRID
    TYPE structured
    ORIGIN 0.d0 0.d0 0.d0
    GRAVITY 0.d0 0.d0 0.d0
    NXYZ 500 1 100
    BOUNDS
      0.d0 0.0 0.0
      250.d0 1.d0 50.d0
    /
  END

GEOMETRIC_PENALTY
-------------------------------

This is only valid if mode is TOIL_IMS. If present, the following heuristic is executed in the
nonlinear solver: if an update from the nonlinear solver would change a saturation value in
a cell by more than 20% of its original value, then the update of that saturation value is set
to change the value by 20%. Like most heuristics, it is not useful in every situation or
model.

.. _max_pres_change_sec:

MAX_PRESSURE_CHANGE
-------------------------------
PFLOTRAN will aim to have no more than this value as the change in pressure each
timestep, adapting the timestep length accordingly. In effect after a timestep where the
pressure change exceeded this value, the timestep length will decrease, after a timestep
where the pressure change was less than this value, the timestep length may increase if
permitted by other factors. The default value is 5.0d5 Pa. 
