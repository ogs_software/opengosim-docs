
:index:`STRATA`
-----------------


Couples a material defined in the input deck with a region in the problem domain.  In most of the reservoir simulation cases using GRDECL, the entire domain will simply be associated to the only material defined in the input deck. See example below: ::

  STRATA
    REGION All
    MATERIAL formation
  END

For simulation cases that use grid formats different than GRDECL, e.g. structured, unstructured_explicit, material defined on a cell-by-cell basis are also supported. See example below: ::

  STRATA
    MATERIAL FILE mat_ids.h5
  END

The external file contains IDs of the material defined in the input deck. A material ID equal to zero corresponds to inactive grid blocks.


