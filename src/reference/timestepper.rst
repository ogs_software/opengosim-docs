
.. _timestepper_sec:

:index:`TIMESTEPPER`
------------------------

Specifies time step acceleration, maximum number of time steps, etc. An example is given below: ::

  TIMESTEPPER FLOW
    TS_ACCELERATION 8
    MAX_STEPS 10000
    MAX_TS_CUTS 5
    NUM_STEPS_AFTER_CUT 5
    DT_FACTOR <tfact_1,.., tfact_i, ..., tfact_n>
  END





The keywords above have the following meanings:

TIMESTEPPER Type
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The word after ``TIMESTEPPER`` at the start of this card can be either FLOW or TRANSPORT. For TOIL_IMS, GAS_WATER, BLACK_OIL, TODD_LONGSTAFF and SOLVENT_TL, this must be FLOW.


.. _ts_accel_sec:


TS_ACCELERATION
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Takes as input an integer, indexing time step acceleration ramp. This is an advanced feature, setup by RESERVOIR_DEFAULTS.


MAX_STEPS
^^^^^^^^^^^^^^^^^^^^^^^^^^

Takes as input an integer, representing the maximum time step after which the simulation will be terminated.

MAX_TS_CUTS
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Takes as input and integer, representing the maximum number of consecutive time step cuts before the simulation is terminated with plot of the current solution printed to a XXX_cut_to_failure.tec file for debugging.


DT_FACTOR
^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Takes as input a sequence of integers. These become the array of floating point numbers of tfac array. Advanced feature. Almost never used outsiede of debuggging.


