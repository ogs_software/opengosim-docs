Release notes
=============

.. _release-notes-intro:

.. contents::
  :backlinks: top
  :depth: 2
  :local:
  
PFLOTRAN-OGS-1.8
+++++++++++++++++++++++

Introduction
~~~~~~~~~~~~~~~~~~~~~~~

The PFLOTRAN-OGS development continues to focus on carbon dioxide capture and storage (CCS), and compared to the previous version, the latest OGS-1.8 release adds the following new elements:

    * Dynamic modelling of temperature for COMP3 and COMP4, to model thermal effects in depleted hydrocarbon fields.
    * Well history to reproduce reservoir initial condition before |CO2| storage, see :ref:`HYSTORY_DATA<history_data_sec>` and :ref:`ESCHED<esched_sec>`.
    * New correlations for the brine properties: Kestin :cite:`Kestin1981` for the viscosity, Rowe and Chou :cite:`RoweImplementation` and Ezrokhi :cite:`EzrokhiModel` for the density.
    * Support for TRANX/Y/Z  and NNC in the GRDECL grids.
    * Transmissibility computation using a local diagonal tensor (LDT), to reproduce the exact same transmissibility computation of industry standard reservoir simulators.

Back-Compatibility
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In COMP3 and COMP4 AMAGAT is now default, and it is no longer possible to interpolate directly the component molar densities in the component molar fractions to obtain the mixture molar density.

PFLOTRAN-OGS-1.7
+++++++++++++++++++++++

Introduction
~~~~~~~~~~~~~~~~~~~~~~~

PFLOTRAN-OGS-1.7 continues to add capabilities to support reservoir engineering required for carbon dioxide capture and storage (CCS). Compared to the previous OGS-1.6 version, the main new elements are:

    * A new method to compute total gas immobilised by re-wetting for |CO2| storage in saline aquifers, see :ref:`STRAND<strand-sec>`.
    * Option to use the Amagat’s law for |CO2|/Reservoir gas mixing for the COMP3/COMP4 solution modes.
    * Miscible option to model |CO2| injection into depleted oil fields, for pressure above the minimum miscible pressure.
    * Composition-dependent saturation function for the COMP3 solution mode, see :ref:`COMP_DEP_SFNS<comp-dep-sfns-sec>`.
    * Possibility to output mass rate to the summary files.
    * :ref:`Directional relative permeability<dir_relperms_sec>` functions, through use of the :ref:`KRNUM<krnumxyz_sec>` keyword. 
    * :ref:`Fetkovich aquifer<fet_sec>`.
    * Multiple :ref:`FIPNUM<fip_sec>` arrays and region-to-region flows.
    * Basic support for Eclipse schedule file  (:ref:`ESCHED<esched_sec>`).
    * :ref:`Leverett J-function<levj_sec>` support for the :ref:`GAS_WATER<gw_intro_sec>` solution mode.

Back-Compatibility
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

There are no back-compatibility issues for this release.

PFLOTRAN-OGS-1.6
+++++++++++++++++++++++

Introduction
~~~~~~~~~~~~~~~~~~~~~~~

The OGS 1.6 version of PFLOTRAN is the sixth in a series of releases designed to support reservoir engineering required for carbon dioxide capture and storage (CCS). Compared to the previous OGS 1.5 version of PFLOTRAN, the main new elements are:

    * New component equation of state mode (:ref:`COMP<comp_intro_sec>`) and multigas modes :ref:`COMP3<multigas_comp3_intro_sec>` and :ref:`COMP4<multigas_intro_sec>`, for simulation of |CO2| storage into depleted hydrocarbon fields.
    * New local mesh refinement options (:ref:`CARFIN<carfin_sec>`) and the ability to complete wells within local refinements (:ref:`CIJKL_D<cijkl_d_sec>`)
    * Changes to the treatment of surface density in brine-type water PVT models like WATERTAB. See :ref:`Density and viscosity dependence on salinity<den_salinity_eos_water_sec>`.
    * Input arrays of the type \*NUM (e.g. SATNUM, IMBNUM) are now written to the INIT output file as integer*4, unlike 1.5 where they were written as real*4
    * New :ref:`EQUILIBRATION<equil_sec>` card that simplifies the input to initialise a study.
    * Support of DATES in :ref:`ECLIPSE_FILE<eclipse_files_sec>` to request report times in by a calendar format (DD-MMM-YYYY).
    * Support for .EGRID grid, in input and output, see :ref:`GDFILE<gdfile_sec>` and :ref:`ECLIPSE_FILE<eclipse_files_sec>`.
    * Improvement of :ref:`MATERIAL_PROPERTY<material_prop_sec>`, where is now possible to skip thermal property for isothermal run, and specifying units.
    * Many bug fixes, and convergence improvement for the GAS_WATER mode.
    * Compatibility with Ubuntu 20.04.

Back-Compatibility
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    * Input arrays of the type \*NUM (e.g. SATNUM, IMBNUM) are now written as integer*4, unlike 1.5 where they were written as real*4


PFLOTRAN-OGS-1.5 
+++++++++++++++++++++++

Introduction
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The OGS 1.5 version of PFLOTRAN is the fifth in a series of releases designed to support reservoir engineering required for carbon dioxide capture and storage (CCS).
Compared to the previous OGS 1.4 version of PFLOTRAN, the main new elements are:

    - SALT_TABLE (SALTVD) to define salinity concentrations that vary with depth.
    - Salinity-dependent brine viscosity (Batzle and Wang :cite:`batzle1992seismic`) in the GAS_WATER flow mode.
    - Field Gas Mass partition in the summary file to analyse the CO2 trapping mechanism (Structural, Solution and Residual trapping).
    - User-defined pressure-dependent miscibility function for the Solvent Todd-Longstaff model, via PMISC and MISC tables.
    - Introduction of grid block reporting (BPR, BTEMP, etc) in the summary file.
    - User-defined vertical resolution for hydrostatic equilibration computation.
    - New keywords :ref:`ABOUT<about-sec>` and :ref:`PREAMBLE<preamble-sec>` to control the amount of output to the screen and the .out file. Please see the corresponding sections for more information.
    - The negative direction transmissibility multiplier grid keywords :ref:`MULTX-<multx_m_grdecl_sec>`, :ref:`MULTY-<multy_m_grdecl_sec>` and :ref:`MULTZ-<multz_m_grdecl_sec>` are now supported. These will be modified by the X-, Y- and Z- face selections in the :ref:`FAULTS<faults_grdecl_sec>` keyword.


Back-Compatibility
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In OGS-1.5, the FMIS card used to model the miscibility function required by the Solvent Todd-Longstaff model has been replaced by MISC. If the user enters FMIS an error is reported suggesting to use of MISC.


PFLOTRAN-OGS-1.4
+++++++++++++++++++++++

Introduction 
~~~~~~~~~~~~~~~~~~~

The OGS 1.4 version of PFLOTRAN is the fourth in a series of releases designed to support reservoir engineering required for carbon dioxide capture and storage (CCS).

Compared to the previous OGS 1.3 version of PFLOTRAN, the main new elements are:

- Carter-Tracy aquifer model
- Fixed-temperature thermal boundaries
- An hysteresis model for relative permeability and capillary pressure
- Introduction of well group control
- Enhanced reporting: FLIP, mass balance, CO2 solution and residual trapping
- The well model now support PI multiplier and variable drilling direction by completion
- New default convergence criteria based on dimensionless residual, included in RESERVOIR_DEFAULT
- NOGASSOL option to turn off the solution of gas in the aqueous phase


Back-compatibility
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Units are now required for all well values such as reference depths.

RESERVOIR_DEFAULTS has been extended to set the STOL and ITOL non-linear convergence criteria and the maximum number of non-linear iterations.

