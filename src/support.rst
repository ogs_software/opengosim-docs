
Support
======================

PFLOTRAN-OGS is an open source reservoir simulator package dedicated to |co2| storage. The software is supported and maintained by `OpenGoSim <https://opengosim.com/>`_. To report any issue, please email support at `opengosim.com <https://opengosim.com/contact.php>`_.
