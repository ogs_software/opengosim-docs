
.. _dir_relperms_sec:

Directional Relative Permeability
================================================================

Multi-phase flows in reservoir simulators are controlled by relative permeabilities, which define how the
phase mobility varies as a function of saturation. Although PFLOTRAN-OGS contains a number of
analytical forms for the directional relative permeabilities, the most common way of inputting the relative
permeabilities is using saturation tables.

The directional relative permeability option allows different relative permeability functions to be used for
different directions of flow. So, for example, gas flow in the vertical :math:`z-` direction, between strata, could be given a different critical gas saturation to reflect the trapping of gas under impermeable layers or layers
with widely different capillary pressures. This effect might not be so marked in the :math:`x-` and :math:`y-` directions,
where flow is generally within strata.

Using a separate saturation function for a given flow direction is similar to using a pseudo relative permeability function. A common use of a pseudo is to limit the effect of numerical dispersion or saturation fronts.

In the PFLOTRAN-OGS implementation the directional relative permeabilities are applied by direction of
flow, but are still regarded as reversable – in other words the same set of relative permeabilities will be
applied to flow in the positive and negative :math:`x-` directions. Note also that :math:`x-` direction here refers to the local
x-coordinate within the grid (really the :math:`I-`, :math:`J-` and :math:`K-` directions) that the true :math:`xyz` coordinates of, say, a local
UTM coordinate system.

Note also that, although the sets of saturation functions used by keywords such as ``KRNUMX`` contain
both relative permeability and capillary pressure information, directional capillary pressures are not
applied – the phase pressures in each grid block that drive the Darcy flow between blocks use the values
in the tables specified by the SATNUM and IMBNUM arrays.

