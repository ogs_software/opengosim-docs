.. _fluid_component_properties:

Fluid component Properties
================================================================

In this section some of the fluid and material property options available within PFLOTRAN are mentioned.

.. _wat-trang:

Water: Trangenstein Density Modeling
-----------------------------------------------

.. math::
  \rho = \frac{A_0 + A_1 T + A_2 T^2 + A_3 T^3 + A_4 T^4 + A_5 T^5  }{1+ A_6 T^6} e^{ c_{pw}  (P_p − A_7 ) },

where:

.. math::
  \begin{split}
  &A_0 =  999.83952   \\
  &A_1 =  16.955176    \\
  &A_2 = –7.987\cdot10^{−3}    \\
  &A_3 = –46.170461\cdot10^{−6}   \\
  &A_4 =  105.56302\cdot10^{−9}    \\
  &A_5 = –280.54353\cdot10^{−12}    \\
  &A_6 =  16.87985\cdot10^{−3}    \\
  &A_7 = −10.2    \\
  &c_{pw} = 0.0003947693 \mbox{ MPa} 
  \end{split}


with the temperature :math:`T` given in degree Celsius and :math:`\rho` in :math:`\mbox{kg/m}^3`.

.. _wat-grabow:

Water: Grabowsky viscosity model
--------------------------------------

.. math::
  \mu_w = \frac{A_w}{ −1+ B_w T +C_w T^2_F},

where:

.. math::
  \begin{split}
  &A_w =2.1850 \\
  &B_w =0.04012 \\
  &C_w =5.1547\cdot 10^{−6}
  \end{split}

and the temperature :math:`T` given in degrees Fahrenheit and :math:`\mu` in :math:`cP`.
