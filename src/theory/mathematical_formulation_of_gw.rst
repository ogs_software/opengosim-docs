.. _formulation-of-gw:

############################################
The Mathematical Formulation of GAS_WATER
############################################

Introduction
==========================


The molar balance for a phase :math:`\alpha` is given by a sum over various contributions from phases :math:`\beta`:

.. math::
  \frac{\partial}{\partial t} \phi  \sum_{\beta}{\left(  x^{\alpha}_{\beta} s_\beta\eta_\beta\right)} + \nabla\cdot \sum_{\beta}{\left( x^{\alpha}_{\beta} \mathbf{q_\beta}\eta_\beta +  \phi s_{\beta} D_{\beta} \eta_{\alpha} \nabla  x^{\alpha}_{\beta}   \right)} = Q_i
  :label: molar_balance_gw

Where:

 * :math:`\phi` is the rock porosity;
 * :math:`x^{\alpha}_{\beta}` is the mole fraction of component :math:`\alpha` (water or gas) in phase :math:`\beta` (aqueous or vapour);
 * :math:`s_\beta` is the saturation of phase :math:`\beta`;
 * :math:`\eta_\beta` is the molar density of phase :math:`\beta`;
 * :math:`\mathbf{q_\beta}` is the Darcy velocity of phase :math:`\beta` (see below);
 * :math:`D_\beta` is the diffusion coefficient of phase :math:`\beta`;
 * :math:`Q_i` is a source term.

The Darcy velocity :math:`\mathbf{q_\beta}` for a phase :math:`\beta` is given by:

.. math::
  \mathbf{q_\beta} = \frac{K k_\beta}{\mu_\beta} \nabla(P_\beta -  \rho_\beta g z)
  :label: darcy_gw

Where:

 * :math:`K` is the saturated media permeability;
 * :math:`k_\beta` is the relative permeability of phase :math:`\beta`;
 * :math:`\mu_\beta` is the viscosity of phase :math:`\beta`;
 * :math:`P_\beta` is the pressure of phase :math:`\beta`;
 * :math:`\rho_\beta` is the mass density of phase :math:`\beta`;
 * :math:`g` is the gravitational constant;
 * :math:`z` is the height coordinate.

The energy balance is given by:

.. math::
  \frac{\partial}{\partial t} \left[ \phi \sum_{\beta}{s_\beta\eta_\beta U_\beta} + (1-\phi)\rho_r c_r T  \right] + \nabla \cdot  \sum_{\beta}{\left[ q_\beta\eta_\beta H_\beta + \kappa \nabla T \right]} = Q_e
  :label: eng_balance_gw

Where:

 * :math:`U_\beta` is the internal energy of phase :math:`\beta`;
 * :math:`\rho_r` is the rock density;
 * :math:`c_r` is the rock heat capacity;
 * :math:`\kappa` is the rock thermal conductivity.
 * :math:`Q_e` is a source term.

The gas-water model is a two-phase mode with aqueous and gaseous phases. The conserved components are a gas (usually CO2) and water. Both components can exist in both phases, so that there may be dissolved gas in the reservoir aqueous phase, and vapourised water in the reservoir gas phase.

The model assumes full instant equilibrium between the components in a cell, so that the gas-water system can exist in three possible states:

    - Saturated – both aqueous and gas phases are present, and both are saturated.

    - Undersaturated water – there is no free gas in the cell, the aqueous phase is undersaturated.

    - Undersaturated gas – there is no free water in the cell, the vapour phase is undersaturated.


Gas-water Characterization Data
=========================================

The gas-water mode supports standard correlations for the gas and water phase properties, in particular the Span and Wagner correlation for CO2 properties :cite:`span1996new` and the Duan, Sun and the Sun, Duan, Hu, Li, Mao correlations for the properties of the CO2-H2O and CO2-H2O-NaCl systems :cite:`duan2003improved`, :cite:`duan2008densities`.


Variable Switching
=========================================
In PFLOTRAN the state of a cell is defined by a number of primary variables, from which other secondary variables are derived. In the gas-water mode two of the three primary variables required are always a pressure and a temperature. The third variable varies by state:

    - In a saturated cell, the saturation of the aqueous phase is a primary variable. The state is changed to unsaturated water if this saturation exceeds unity, and the state is changed to unsaturated gas if this saturation falls below zero.
    - In an unsaturated water cell the mole fraction concentration of the gas component in the aqueous phase is a primary variable. The state is changed to saturated, if this concentration exceeds an estimated maxmium concentration computed using Henry’s law.
    - In an unsaturated gas cell the mole fraction concentration of the water component in the vapour phase is a primary variable. The state is changed to two phase, if this concentration exceeds an estimated dew point.
