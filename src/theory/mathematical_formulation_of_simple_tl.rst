.. _formulation-of-simple_todd_longstaff:

The Mathematical Formulation of the Simple Todd-Longstaff Model
================================================================

| This is a simple version of the Todd-Longstaff model, suitable to reservoirs which are under-saturated (no free gas). In this treatment the injected solvent is represented by the gas phase. Although the gas can be miscible with the oil, it does not dissolve in the oil phase as in the black-oil-based treatment of section 3. This is commonly known as a 3-phase Todd-Longstaff method.

| The 3-phase miscible option in Pflotran treats a system of oil, gas and water, where the gas is the solvent phase of 4-phase model. In the miscible limit (the parameter omega, :math`\omega`, being set to one), it models a miscible displacement – the hydrocarbon phases move together and their mobilities are simply proportional to their saturations, so the phases behave like components in a single miscible phase.

| The simple Todd-Longstaff model therefore has three phases, oil, gas and water, the gas being associated with the solvent, which is typically CO2. 

| The 3-phase Todd-Longstaff model in considerably simpler than the 4-phase model. As the vapour phase is always assumed to be a simple miscible solvent, there are no issues with end-point scaling, and the vapour-oil relative permeability curve is always a simple straight line, and the FMIS keyword is not required. 

| The parameter :math:`\omega` is still required, however. In the miscible limit, ω=1, the oil and gas phase are assumed miscible and intertwined, flowing with a single mixed viscosity and density, emulating a single miscible phase, whilst in the :math:`\omega=0`, the fluids flow with their own viscosities and densities.
