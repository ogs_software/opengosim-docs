.. _formulation-of-solvent_todd_longstaff:

The Mathematical Formulation of the Solvent Todd-Longstaff Model
================================================================

.. _tl4p-intro:

Introduction
----------------------------------

The solvent Todd-Longstaff model is a generalization of the :ref:`black oil model<formulation-of-black-oil>`. It is a four-phase, four-component model. The oil, water and gas phases are treated in the same way as in the black oil model, and there is an additional solvent phase. The solvent is typically CO2, and is regarded as miscible with the gas and miscible with the oil. However, the oil and the gas are not generally miscible with each other (although gas is usually soluble in oil). In this note ‘hydrocarbon’ is oil+gas+solvent and ‘vapour’ is gas+solvent. The solvent Todd-Longstaff model is often known as a ‘four-phase’ Todd-Longstaff model; Pflotran also supports the simpler ‘three-phase’ Todd-Longstaff model and that is described in the next chapter.

The Pflotran Todd Longstaff options are based on :cite:`todd1972development`,  and in particular the four-component model described in the appendix of that paper.

The Todd Longstaff model is phenomenological and is a way of using a standard model with four phases to emulate a miscible or partially miscible displacement. The degree of miscibility is parametrised by a variable :math:`\omega`, and in the miscible limit of :math:`\omega \rightarrow 1` the simulated system still formally has four distinct phases, but the phase properties are modified in such a way that they behave as if some phases were flowing together as a single phase. In some respects, this is rather crude compared to a full equation of state flash calculation to determine the number of true phases. However, it allows partial miscibility to be modelled. The user may control the degree of miscibility in the relative permeability and the density/viscosity treatments using the FMIS and OMEGA keywords respectively.

Some special points about the Pflotran four-phase model:
    - In the miscible limit, the oil relative permeability data, :math:`K_{row}(S_o+S_g+S_s)` is used as the hydrocarbon relative permeability. The gas relative permeability data, :math:`K_{rg}(S_g+S_s)` is always used as the vapour relative permeability.

    - The degree of miscibility is a user-defined function of the fractional saturation of the solvent in the vapour (not the mass or molar fraction).

    - A three-point end point scaling method is used for the miscible/immiscible relative permeability interpolation.

    - Simple saturations rather than mobile saturations are used to obtain fluid viscosities and densities.

    - Pflotran Ignores the effect of the solvent on the oil-gas equilibrium point – in terms of the oil-gas system solvent is regarded as a separate phase.

The Relative Permeability Model
----------------------------------

The model has miscible (M) and immiscible (I) limits.

The Miscible Limit
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In the miscible limit the relative permeabilities are proportional to the phase saturations.
These are not really true phase saturations in the miscible case, but hydrocarbon
component volume fractions.

.. math::
  K_{ro}^M = (S_o/S_h)K_{rh}

.. math::
  K_{rg}^M = (S_g/S_h)K_{rh}

.. math::
  K_{rs}^M = (S_s/S_h)K_{rh}

where :math:`K_{rh}` is the hydrocarbon relative permeability, defined by :math:`K_{rh}(S_h)=K_{ow}(S_h=1-S_w)`, where :math:`K_{row}` is the user-supplied oil-water relative permeability table.


The Immiscible Limit
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
When the vapour phase composition approaches pure gas the normal immiscible model is
obtained:

.. math::
  K_{ro}^I=K_{ro}(S_o,S_v,S_w)
.. math::
  K_{rv}^I=K_{rg}(S_v)

In this limit, in which the vapour phase is no longer miscible with the oil, the gas and
solvent phases are still assumed miscible within the vapour:

.. math::
  K_{rg}^I=(S_g/S_v)K_{rv}^I
.. math::
  K_{rs}^I=(S_s/S_v)K_{rv}^I

This is reasonable, as miscibility would always be expected between two gases. :math:`K_{rv}` is defined by :math:`K_{rg}(S_v=S_g+S_s)`, :math:`K_{rg}` the user-supplied gas relative permeability table.


.. _tl4p-interpolation:

Interpolation between the miscible and immiscible pictures
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In the Todd Longstaff paper :cite:`todd1972development` it is suggested that the interpolation between the I and
the M pictures of relative permeability be a simple switch based on the fractional solvent
saturation in the hydrocarbon, and a value of 1% is suggested as the criterion. Such a sharp
theta-function switch is likely to cause severe non-linear convergence problems in an
implicit simulator. In addition, the :math:`S_s /S_h` is not really a measure of the composition of the
vapour phase. A better interpolant, which reflects the miscible nature of pure solvent
injection, and the immiscible nature of pure gas injection, is:

.. math::
  f^M = f_m^s  (f_s ) \cdot f_m^p(P)

where :math:`f_s=S_s/S_v` is the solvent saturation fraction in the vapour phase, and :math:`f_m^s(f_s)` and :math:`f_m^p(P)` are user defined functions supplied by the :ref:`MISC<MISC-sec>` and :ref:`PMISC<PMISC-sec>` tables. If  MISC is not provided by the user, :math:`f_m^s` defaults to a linear function with null value for zero solvent saturation, and unity when the vapour is fully saturated with solvent (:math:`f_s=1` ). If the user does not enter the PMISC table, the pressure-dependent miscibility function is defaulted to 1 (:math:`f_m^p=1` ). :math:`f_m^p` is used to describe the miscibility changes with pressure.

A simple interpolation between the miscible and immiscible limit will yield:

.. math::
  K_{rp} =f_M K^ M_{rp} +(1-f_M ).K^I_{rp}
  :label: misc-immisc-interp


End point effects when mixing relative permeability curves
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In practice the interpolation function in :eq:`misc-immisc-interp` above, which simply adds two weighted relative permeability curves, is not ideal, as it causes a discontinuous jump in the residual
saturations between :math:`f_M =0` and :math:`f_M =\varepsilon`, where :math:`\varepsilon` is just above :math:`0`, so that a tiny amount of solvent can set the residual saturation to zero and offer full recovery. A more realistic model is one in which the oil and vapour residual saturations are interpolated in :math:`f_M`:

.. math::
  S_{ocr} (f_s )=(1-f_M ).S_{ocr}^I

.. math::
  S_{vcr} (f_s )=(1-f_M ).S_{vcr}^I

Where the vapour functions are the user-supplied gas functions, and :math:`S_{ocr}^I` and :math:`S _{vcr}^I`  are the immiscible, ‘rock curve’, critical values.
Both the miscible and immiscible relative permeability contributions are rescaled to the new
residual saturations. The normalised miscible and immiscible contributions are then added
together:

.. math::
  K_{rp} =f_M {K^M_{rp}}^* +(1-f_M ) {K^I_{rp}}^*
  :label: misc-immisc-interp-2

where :math:`{K_{rp}}^*` are the normalised curves, and :math:`p=(o,g,s)`.
The formulation adopted is a minimal three-point rescaling of the M and I contributions. This
is slightly different from the Eclipse approach in which a four-point scaling is used, which
can cause problems when used within the reduced saturation range of the hydrocarbon
relative permeability look-up.
The reason for the concern with endpoints is that ultimate recovery should scale smoothly
with miscibility – otherwise if there is any miscible fraction the ultimate recovery jumps to
100%.

The Viscosity Mixing Model
----------------------------------
The Todd-Longstaff model interpolates between two pictures of the phase viscosities in the
omega parameter, ω. For ω=0, each phase takes its normal viscosity value. For ω=1,
miscible phases take a common mixed viscosity, which is a power-law combination of the
individual phase viscosities. This reflects the idea that, in an adverse-mobility displacement
process, extensive fingering will occur, which tangles up the phases and causes them to flow together with a single effective viscosity. In practice, intermediate ω values such as 0.5
are proposed in :cite:`todd1972development`.
In the 4-phase Todd Longstaff model it is assumed that gas and oil are not miscible, but that
gas is miscible with solvent (but not oil), oil is miscible with solvent (but not gas), and that
solvent is miscible with both oil and gas, so the phase effective viscosities, μ pe , following :cite:`todd1972development`, are:

.. math::
  \mu_{ oe} = μ_o^{(1-\omega)} μ_{om}^\omega

.. math::
  \mu_{ ge} = μ_g^{(1-\omega)} μ_{gm}^\omega

.. math::
  \mu_{ se} = μ_s^{(1-\omega)} μ_{sm}^\omega

where the mixed viscosities are:

.. math::
  \mu_{ om}^\omega = \frac{\mu_o \mu_s}{ \left( \frac{S_o}{S_{os}}\mu_s^{1/4} + \frac{S_s}{S_{os}}\mu_o^{1/4}   \right)^4            }
  :label: visc-mix-1

.. math::
  \mu_{ gm}^\omega = \frac{\mu_g \mu_s}{ \left( \frac{S_g}{S_{gs}}\mu_s^{1/4} + \frac{S_s}{S_{gs}}\mu_o^{1/4}   \right)^4            }
  :label: visc-mix-2

.. math::
  \mu_{ sm}^\omega = \frac{ \mu_o \mu_o \mu_s}{ \left( \frac{S_o}{S_{h}}\mu_g^{1/4} mu_s^{1/4}+ \frac{S_g}{S_{h}}\mu_o^{1/4} \mu_s^{1/4} + \frac{S_s}{S_{h}}\mu_o^{1/4} \mu_g^{1/4}  \right)^4            }
  :label: visc-mix-3

where :math:`S_{os} =S_{o} +S_{s}`, :math:`S_{gs} =S_{ g} +S_{s}`, and :math:`S_{h} = S_{o} + S_{g} +S_{s}`. 

| Note: Eclipse, :cite:`schlumberger2010eclipse`, uses mobile saturations :math:`S_p’=S_p -S_{pcr}`, rather than actual saturations in the expressions (3.3), (3.4) and (3.5). However, in the Pflotran implementation we will follow Todd and Longstaff, :cite:`todd1972development`, model in using actual saturations.

The Density Mixing Model
--------------------------------------------
Todd and Longstaff propose that the effective density of the phases, which appears in the
Darcy flow expressions, is also obtained as a mixture of the actual phase densities. Again,
this reflects the fact that the phases are viewed as intertwined and flowing as one. In the
completely miscible case with ω=1 both the viscosities and densities of the phases must
match, so that they flow as if they were components of a single phase.
In the interpolation formulae proposed by Todd and Longstaff the effective densities are
functions of the effective viscosities obtained in the previous section. For the simple case of
a mixing between two phases, say oil and solvent, we have an effective oil viscosity of μ oe .
The approach used is based on the 1/4 th power fluidity mixing rule (equation (4) in Todd &
Longstaff), in which a mixed viscosity μ m is related to viscosities for phases a and b, (μ a and
μ b ), through saturation volume fractions as:
 
.. math::
  \phi_m = \frac{S_a }{S_a +S_b} \phi_a + \frac{S_b}{S_a +S_b}  \phi_b = f_a \phi_a + f_b \phi_b
  :label: den1

where f a is the saturation fraction S a /(S a +S b ), for phases a and b, and φ=(1/μ) 1/4 .
The argument used by Todd and Longstaff is that if a mixed viscosity is known (e.g. the
effective ω-dependent effective viscosities obtained above), equation 3.6 can be used to
obtain a fractional saturation. So, considering oil-solvent mixing, we have:

.. math::
  f_o \cdot (\phi_s - \phi_o )=(\phi_s – \phi_{om} )

.. math::
  f_o = \frac{\phi_s - \phi_{om}}{\phi_s - \phi_o}   = \frac{  \frac{\phi_s}{\phi_o} - \frac{\phi_{om}}{\phi_o}  }{ \frac{\phi_s}{\phi_o}  - 1}  =  \frac{M^{1/4} – \left(  \frac{\mu_o}{\mu_{om}} \right)^{1/4}}{M^{1/4} - 1}

with :math:`M=\frac{\phi_s}{\phi_o}`; which is the result (8b) in the Todd-Longstaff paper. Once we have the fraction :math:`f_o` the mixed oil density is simply obtained as:

.. math::
  \rho_{oe} =f_o \rho_o + (1-f_o )\rho_s

This analysis can be applied to the oil-solvent and the gas-solvent cases. However, the
solvent is miscible with the oil and the gas, and so a more complex mixing rule will be
required. To handle the solvent, an oil-gas viscosity in the absence of solvent is defined by
the mixing rule:

.. math::
  \rho_{og} = g_o \phi_o + g\phi_g ,

with:

.. math::
  g_o =  \frac{S_o}{S_o +S_g}  ; g_g = \frac{S_g}{S_o +S_g }  .

Then the solvent mixing rule becomes one between the solvent and the oil-gas mixture,
and the solvent mixing fraction can be obtained:

.. math::
  \phi_{sm}= f_{og}\phi_{og} + f_s \phi_s,
  
with 

.. math::
  f_{og} = \frac{S_o +S_g}{S_o +S_g +S_s}   =1-f_s

The above viscosity-based treatment will fail if the mobility ratio, M, is unity, as the
denominators in the expressions for the phase fractions become zero. In this case a backup
interpolation is used, in which the viscosity is simply interpolated in :math:`\omega` between the original phase densities and the mixed density.

Treatment of Dissolved Gas
---------------------------------------------------
The original Todd-Longstaff model did not mention dissolved gas. If the solvent is regarded
purely in the vapour phase, then the partial pressure of reservoir gas will be reduced, and
gas will might come out of solution. However, the solvent will generally also partition into
the oil (e.g. CO2 is soluble in oil at less than miscible concentrations). If the solvent is
miscible with oil, it will be miscible with saturated or undersaturated oil, and the Todd-
Longstaff approach is for the solvent to be treated as a separate volume fraction of the
hydrocarbon. So, in the absence of a better-justified treatment of solvent solubility, the
solvent will not affect the oil-gas phase equilibrium point.

Treatment of Capillary Pressures
---------------------------------------------------
Oil-solvent capillary pressures are not regarded as present in the three-phase Todd-Longstaff model, as the oil and solvent are always miscible. In the four-phase Todd-Longstaff model there can be an oil-gas capillary pressure – indeed it may be required to define the initial state transition zones. The natural solution is to make the vapour-oil capillary pressure a function of the immiscible interpolation coefficient :math:`f^I = f^M -1`:


.. math::
  P_{cov} = f^I P_{cog} (S_v )



