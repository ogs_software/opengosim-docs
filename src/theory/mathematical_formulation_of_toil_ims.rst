.. _formulation-of-toil-ims:

The Mathematical Formulation of TOIL_IMS
========================================

The mathematical model of the TOIL_IMS module implemented in PFLOTRAN describes the oil/water non-isothermal immiscible fluid flows by a system of three equations (3 degrees of freedom, DOFs): a molar balance equation :eq:`molar_balance_toil` for each phase, which can be easily recast as mass balance, an energy equation :eq:`eng_balance_toil` which assumes that the two phases and the rock are in thermal equilibrium and neglects the kinetic and potential energy.
Fluxes are modeled using a multi-phase Darcy's formula :eq:`darcy_toil`.
See below the equation system

.. math::
  \frac{\partial}{\partial t} \phi \left(s_\alpha\eta_\alpha\right) + \nabla\cdot\left(\mathbf{q_\alpha}\eta_\alpha\right) = Q_i
  :label: molar_balance_toil

.. math::
  \frac{\partial}{\partial t} \left[ \phi \sum_{\alpha}{s_\alpha\eta_\alpha U_\alpha} + (1-\phi)\rho_r c_r T  \right] + \nabla \cdot  \sum_{\alpha}{\left[ q_\alpha\eta_\alpha H_\alpha + \kappa \nabla T \right]} = Q_e
  :label: eng_balance_toil

.. math::
  q_\alpha = \frac{K k_\alpha}{\mu_\alpha} \nabla(P_\alpha - \rho_\alpha g z)
  :label: darcy_toil
  

Where:
 * :math:`\eta_\alpha` is the molar density of phase :math:`\alpha`;
 * :math:`H_{\alpha}` is the enthalphy of phase :math:`\alpha`;
 * :math:`U_{\alpha}` is the internal energy of phase :math:`\alpha`;
 * :math:`s_{\alpha}` is the phase saturation of phase :math:`\alpha`;
 * :math:`K` is the saturated media permeability;
 * :math:`k_\alpha` is the relative permeability of phase :math:`\alpha`;
 * :math:`\mu_\alpha` is the viscosity of phase :math:`\alpha`;
 * :math:`P_\alpha` is the pressure of phase :math:`\alpha`;
 * :math:`\rho_\alpha` is the mass density of phase :math:`\alpha`;
 * :math:`g` is the gravitational constant;
 * :math:`z` is the height coordinate.

The system of equations is discretised in space with a finite volume scheme that uses a two-point flux formula, and in time with a first order fully implicit scheme. The equations are linearised using the Newton-Raphson approach. An adaptive time stepping scheme is adopted. The resulting system features three equations in three unknowns (primary variables) that are chosen as follows: Oil Pressure, Oil Saturation and Temperature.

