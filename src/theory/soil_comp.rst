.. _soil_comp_model:

Rock Compressibility Model
================================================================

Three models are available in |pftogs| to compute the rock compressibility, which reflects the rock porosity changes due to pressure build ups and relaxations. These are applied to the pore volume of the rock. The available models are Leijnse, BRAGFLO and a quadratic model. 

| These formulations are described below:


The Leijnse rock compressibility model
----------------------------------------------------

.. math::
  \phi_{\mbox{eff}} = 1−(1−\phi_0 ) \exp\left[  −C_{\mbox{rock}} ( p− p_{\mbox{ref}} ) \right]
  :label: leijnse
  


where:

.. math::
  \begin{split}
  &p &= \mbox{pressure [Pa]} \\
  &p_{\mbox{ref}} &= \mbox{reference pressure [Pa]} \\
  &\phi_{\mbox{eff}} &= \mbox{effective porosity [-]} \\
  &\phi_0 &= \mbox{initial (unperturbed) porosity [-]} \\
  &C_{\mbox{rock}} &= \mbox{rock compressibility [1/Pa]} \\
  \end{split}



The BRAGFLO rock compressibility model
-----------------------------------------------

.. math::
  \phi_{\mbox{eff}} = \phi_0 \exp\left[C_{\mbox{rock}} ( p− p_{\mbox{ref}} ) \right]
  :label: bragflo

.. math::
  \begin{split}
  &p &= \mbox{pressure [Pa]} \\
  &p_{\mbox{ref}} &= \mbox{reference pressure [Pa]} \\
  &\phi_{\mbox{eff}} &= \mbox{effective porosity [-]} \\
  &\phi_0 &= \mbox{initial (unperturbed) porosity [-]} \\
  &C_{\mbox{rock}} &= \mbox{rock compressibility [1/Pa]} \\
  \end{split}


.. _quad_soil_comp_model_sec:


The Quadratic rock compressibility model
--------------------------------------------------------

.. math::
  \Phi = \Phi( P_{\mbox{ref}} ) \left( 1 + X + \frac{X^2}{2} \right),
  :label: soil_quadratic

where:

.. math::
  \begin{split}
  &X &= C_{\mbox{rock}} ( P−P_{\mbox{ref}} )  \\
  &\Phi &= \mbox{porosity}  \\
  &P &= \mbox{pressure [Pa]}  \\
  &P_{\mbox{ref}} &= \mbox{reference pressure [Pa]}  \\
  &C_{\mbox{rock}} &= \mbox{rock compressibility [1/Pa]}  \\
  \end{split}













