

Theory Pages
============================

In these pages we provide detailed theory for the various simulation capacities in |pftogs|.

.. toctree::
   :maxdepth: 1

   mathematical_formulation_of_gw
   mathematical_formulation_of_toil_ims
   mathematical_formulation_of_black_oil
   mathematical_formulation_of_stl
   mathematical_formulation_of_simple_tl
   miscible_treatment
   solving_the_flow_equations
   fluid_properties
   soil_comp
   char_curves_smoother
   well_model
   dir_relperms
   residual_and_stranded

