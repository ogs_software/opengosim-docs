
Reservoir simulator benchmarks
===============================================

For the user interested to check the code against typical reservoir simulator benchmarks, the input files of three cases are provided: SPE1, SPE3, SPE5 and SPE10. 

The input files will run as provided and follow the specifications given in the literature. No detailed instructions are given: the user can consult the key reference of each benchmark for its problem description, and this wiki for the explanation of the input instructions.


SPE1 
-----------------

This SPE1 benchmark is taken from :cite:`odeh1981comparison`, and it models the second variable bubble point case

Input files:

 * :download:`SPE1.in <tutorial_downloads/reservoir_simulator_benchmarks/SPE1/SPE1.in>`
 * :download:`spe1.grdecl <tutorial_downloads/reservoir_simulator_benchmarks/SPE1/spe1.grdecl>`

SPE3
-----------------

This SPE3 benchmark is taken from :cite:`spe_3`, and it models 9 hydrocarbon component  version of the problem from Arco.

Input files:

 * :download:`SPE3_ARCO_9C.in <tutorial_downloads/reservoir_simulator_benchmarks/SPE3/SPE3_ARCO_9C.in>`
 * :download:`spe3.grdecl <tutorial_downloads/reservoir_simulator_benchmarks/SPE3/spe3.grdecl>`


SPE5
-----------------

This SPE5 benchmark is taken from :cite:`killough1987fifth`, and it models the 4-component Todd-Longstaff version of the problem.

Input files:

 * :download:`SPE5.in <tutorial_downloads/reservoir_simulator_benchmarks/SPE5/SPE5.in>`
 * :download:`spe5.grdecl <tutorial_downloads/reservoir_simulator_benchmarks/SPE5/spe5.grdecl>`



SPE10
-----------------

This SPE10 benchmark is taken from :cite:`christie2001tenth`, and it models the full-size version of the problem with no upscaling.

Input files:

 * :download:`SPE10.in <tutorial_downloads/reservoir_simulator_benchmarks/SPE10/SPE10.in>`
 * :download:`spe10.grdecl <tutorial_downloads/reservoir_simulator_benchmarks/SPE10/spe10.grdecl>`
 * :download:`spe10_perm.data <tutorial_downloads/reservoir_simulator_benchmarks/SPE10/spe10_perm.data>`
 * :download:`spe10_phi.data <tutorial_downloads/reservoir_simulator_benchmarks/SPE10/spe10_phi.data>`

