

Tutorials
==========================================

This is a set of tutorials dedicated to |CO2| and Hydrogen Storage problems. The first three help the user to familiarise with the :ref:`GAS_WATER model<gw_intro_sec>`, which is dedicated to |CO2| storage in saline aquifers. The fourth tutorial initiate the user to the modelling of |CO2| Storage in depleted gas fields, and uses the Multi-Gas model in its :ref:`COMP3<multigas_comp3_intro_sec>` mode implementation. The fifth tutorial focuses on Hydrogen storage in a depleted reservoir and uses the Multi-Gas model in its :ref:`COMP3<multigas_comp3_intro_sec>` mode implementation.
The user should carry out first Tutorial 1, it can then move to either Tutorial 2 or 3 independently. Before running Tutorial 4 or 5, the user is advised to at least run Tutorials 1 and 3. However, covering all tutorials is recommended for a quick overview of the main capabilities available.

.. toctree::
   :maxdepth: 1

   tut1
   tut2
   tut3
   tut4
   tut5


Datasets for other benchmark cases are provided in the following section:

.. toctree::
   :maxdepth: 1

   benchmarks


For instructions on how to run PFLOTRAN-OGS, follow the link below:

.. toctree::
   :maxdepth: 1

   running
